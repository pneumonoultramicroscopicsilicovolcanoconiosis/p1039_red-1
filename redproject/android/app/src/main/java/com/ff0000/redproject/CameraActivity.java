package com.ff0000.redproject;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.provider.MediaStore;
import android.util.Log;
import android.view.SurfaceView;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvException;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.Policy;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class CameraActivity extends AppCompatActivity implements CameraBridgeViewBase.CvCameraViewListener2 {

    public static final int PADDING = 20;
    public static final int CAMERA_MODE_BOOK = 0;
    public static final int CAMERA_MODE_SCRAP = 1;
    public static final int PICK_FROM_ALBUM = 1;

    private static final String TAG = "opencv";
    private CameraBridgeViewBase mOpenCvCameraView;
    private Mat matOriginal;
    private Mat matInput;
    private Mat matResult;
    CameraBridgeViewBase.CvCameraViewFrame cvFrame;
    private Rect boundingRect;
    private boolean boolProcess = false;

    private String token;
    private File tempFile;

    private ArrayList<String> files = new ArrayList<>();

    int cameraMode = CAMERA_MODE_BOOK;

    int w;
    int h;
    float wRatio = 1.0f;

    public native void minus(long firstImage, long secondImage, int alpha);

    static {
        System.loadLibrary("opencv_java4");
        System.loadLibrary("native-lib");
    }

    private ImageButton btnCamera;
    private ImageButton btnGallery;
    private Button btnMode;
    private TextView txtTitle;

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS: {
                    mOpenCvCameraView.enableView();
                }
                break;
                default: {
                    super.onManagerConnected(status);
                }
                break;
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_camera);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //퍼미션 상태 확인
            if (!hasPermissions(PERMISSIONS)) {
                //퍼미션 허가 안되어있다면 사용자에게 요청
                requestPermissions(PERMISSIONS, PERMISSIONS_REQUEST_CODE);
            }
        }

        btnCamera = findViewById(R.id.btnCamera);
        btnGallery = findViewById(R.id.btnGallery);
        btnMode = findViewById(R.id.btnMode);
        txtTitle = findViewById(R.id.txtTitle);
        mOpenCvCameraView = findViewById(R.id.activity_surface_view);
        mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
        mOpenCvCameraView.setCvCameraViewListener(this);
        mOpenCvCameraView.setCameraIndex(0); // front-camera(1),  back-camera(0)
        mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);

        Intent mainIntent = getIntent();
        token = mainIntent.getStringExtra("token");
        cameraMode = mainIntent.getIntExtra("cameraMode", 0);

        if(cameraMode == CAMERA_MODE_SCRAP) {
            cameraMode = CAMERA_MODE_SCRAP;
            btnMode.setText("스크랩");
            txtTitle.setText("스크랩 촬영");
        } else if(cameraMode == CAMERA_MODE_BOOK) {
            cameraMode = CAMERA_MODE_BOOK;
            btnMode.setText("책");
            txtTitle.setText("책 촬영");
        }

        btnMode.setOnClickListener(v -> {
            changeButtonMode();
        });

        btnCamera.setOnClickListener(v -> {
            if(cameraMode == CAMERA_MODE_BOOK) takeBook();
            else if(cameraMode == CAMERA_MODE_SCRAP) takeScrap();
        });

        btnGallery.setOnClickListener(v -> {
            openGallery();
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_FROM_ALBUM) {

            Uri photoUri = data.getData();

            Cursor cursor = null;

            try {

                /*
                 *  Uri 스키마를
                 *  content:/// 에서 file:/// 로  변경한다.
                 */
                String[] proj = { MediaStore.Images.Media.DATA };

                assert photoUri != null;
                cursor = getContentResolver().query(photoUri, proj, null, null, null);

                assert cursor != null;
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);

                cursor.moveToFirst();

                tempFile = new File(cursor.getString(column_index));

            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }

            BitmapFactory.Options options = new BitmapFactory.Options();
            Bitmap bitmap = BitmapFactory.decodeFile(tempFile.getAbsolutePath(), options);
            Bitmap bmp32 = bitmap.copy(Bitmap.Config.ARGB_8888, true);
            Utils.bitmapToMat(bmp32, matOriginal);

            imageProcessing(null);

            if(cameraMode == CAMERA_MODE_BOOK) {
                takeBook();
            } else {
                takeScrap();
            }

        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "onResume :: Internal OpenCV library not found.");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_2_0, this, mLoaderCallback);
        } else {
            Log.d(TAG, "onResum :: OpenCV library found inside package. Using it!");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    public void onCameraViewStarted(int width, int height) {
    }

    @Override
    public void onCameraViewStopped() {

    }

    @Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {

        if(cameraMode == CAMERA_MODE_SCRAP) {
            matInput = inputFrame.rgba();
            matOriginal = matInput.clone();
            return matInput;
        }

        if(!boolProcess) {
            boolProcess = true;
            cvFrame = inputFrame;
            new ImageAsync().execute();
        } else {
            matOriginal = inputFrame.rgba();
            matInput = matOriginal.clone();
            Log.d("TakeBook", "matOriginal = " + matOriginal + " / matInput = " + matInput);
            if(boundingRect != null) {
                Imgproc.rectangle(matInput, boundingRect, new Scalar(0, 0, 255), 3);
            }
        }

        return matInput;

        /*matInput = inputFrame.rgba();
        Mat mRgbaT = matInput.t();
        Core.flip(matInput.t(), mRgbaT, 1);
        Imgproc.resize(mRgbaT, mRgbaT, matInput.size());

        return matInput;*/
    }

    private void changeButtonMode() {
        if(cameraMode == CAMERA_MODE_BOOK) {
            cameraMode = CAMERA_MODE_SCRAP;
            btnMode.setText("스크랩");
            txtTitle.setText("스크랩 촬영");
        } else if(cameraMode == CAMERA_MODE_SCRAP) {
            cameraMode = CAMERA_MODE_BOOK;
            btnMode.setText("책찍");
            txtTitle.setText("책 촬영");
        }
    }

    private void takeBook() {

        Mat mat = matOriginal;
        Bitmap bitmap = null;

        try {
            bitmap = Bitmap.createBitmap(mat.cols(), mat.rows(), Bitmap.Config.ARGB_8888);
            Utils.matToBitmap(mat, bitmap);
            //bitmap = Bitmap.createBitmap(bitmap, boundingRect.x, boundingRect.y, boundingRect.width, boundingRect.height);
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, false);
        } catch (CvException e) {
            Log.d(TAG, e.getMessage());
        }

        FileOutputStream out = null;
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp;

        File sd = new File(Environment.getExternalStorageDirectory() + "/takebook");
        boolean success = true;

        if(!sd.exists()) {
            success = sd.mkdir();
        }

        if(success) {
            File dst = new File(sd, imageFileName + ".jpg");

            try {
                out = new FileOutputStream(dst);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            } catch (Exception e) {
                Log.d(TAG, e.getMessage());
            } finally {
                try {
                    if(out != null) {
                        files.add(imageFileName);

                        out.close();

                        Intent intent = new Intent(CameraActivity.this, PreviewActivity.class);
                        intent.putExtra("files", files);
                        intent.putExtra("token", token);
                        intent.putExtra("boundingRectX", h - boundingRect.y - boundingRect.height);
                        intent.putExtra("boundingRectY", boundingRect.x);
                        intent.putExtra("boundingRectWidth", boundingRect.width);
                        intent.putExtra("boundingRectHeight", boundingRect.height);
                        intent.putExtra("ratio", wRatio);
                        startActivity(intent);
                        finish();
                        Log.d(TAG, "OK!!");
                    }
                } catch (IOException e) {
                    Log.d(TAG, e.getMessage() + "ERROR");
                }
            }
        }
    }

    private void takeScrap() {
        Mat mat = matOriginal;
        Bitmap bitmap = null;

        try {
            bitmap = Bitmap.createBitmap(mat.cols(), mat.rows(), Bitmap.Config.ARGB_8888);
            Utils.matToBitmap(mat, bitmap);
            //bitmap = Bitmap.createBitmap(bitmap, boundingRect.x, boundingRect.y, boundingRect.width, boundingRect.height);
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, false);
        } catch (CvException e) {
            Log.d(TAG, e.getMessage());
        }

        FileOutputStream out = null;
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp;

        File sd = new File(Environment.getExternalStorageDirectory() + "/takebook");
        boolean success = true;

        if(!sd.exists()) {
            success = sd.mkdir();
        }

        if(success) {
            File dst = new File(sd, imageFileName + ".jpg");

            try {
                out = new FileOutputStream(dst);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            } catch (Exception e) {
                Log.d(TAG, e.getMessage());
            } finally {
                try {
                    if(out != null) {
                        files.add(imageFileName);
                        out.close();

                        String path = Environment.getExternalStorageDirectory() + "/takebook/" + imageFileName + ".jpg";
                        HashMap<String, String> map = new HashMap<>();
                        map.put("dir", path);
                        map.put("mode", "1");

                        FlutterResult flutterResult = FlutterResult.getInstance();
                        flutterResult.getResult().success(map);

                        finish();
                        Log.d(TAG, "OK!!");
                    }
                } catch (IOException e) {
                    Log.d(TAG, e.getMessage() + "ERROR");
                }
            }
        }

    }



    private void openGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(intent, PICK_FROM_ALBUM);
    }

    private void imageProcessing(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        if(inputFrame != null) {
            matInput = inputFrame.rgba();
        } else {
            matInput = matOriginal.clone();
        }

        if (matResult != null) matResult.release();
        matResult = new Mat(matInput.rows(), matInput.cols(), matInput.type());
        Mat medianResult = new Mat(matInput.rows(), matInput.cols(), matInput.type());
        Mat matKernel = new Mat();

        w = (int) matInput.size().width;
        h = (int) matInput.size().height;
        wRatio = w / 720f;
        float hRatio = h / 480f;
        int alpha = 1;
        //Imgproc.Canny(matInput, matResult, 50, 150);

        Imgproc.cvtColor(matInput, matResult, Imgproc.COLOR_RGBA2BGR);

        Imgproc.resize(matResult, matResult, new Size(720, 480));
        Imgproc.medianBlur(matResult, medianResult, 5);
        Imgproc.GaussianBlur(medianResult, matResult, new Size(1, 47), 0);
        Imgproc.GaussianBlur(matResult, matResult, new Size(71, 1), 0);

        Imgproc.GaussianBlur(medianResult, medianResult, new Size(11, 15), 0);

        //Imgproc.Canny(matInput, matResult, 50, 150);


        minusImage(matResult, medianResult, alpha);

        Imgproc.cvtColor(matResult, matResult, Imgproc.COLOR_BGR2GRAY);

        Imgproc.adaptiveThreshold(matResult, matResult, 1,
                Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C,
                Imgproc.THRESH_BINARY_INV,
                31,
                5);
        matKernel = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(3, 4));

        Imgproc.erode(matResult, matResult, matKernel);
        Imgproc.erode(matResult, matResult, matKernel);


        for(int i = 0; i < 10; i++) {
            Imgproc.dilate(matResult, matResult, matKernel);
        }

        for(int i = 0; i < 8; i++) {
            Imgproc.erode(matResult, matResult, matKernel);
        }

        List<MatOfPoint> contours = new ArrayList<>();
        Mat matHicy = new Mat();

        Imgproc.findContours(matResult, contours, matHicy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_NONE);

        int max = -1;
        MatOfPoint ret = null;

        for(MatOfPoint contour : contours) {
            int len = contour.toArray().length;

            if(max < len) {
                max = len;
                ret = contour;
            }
        }

        if(ret != null || boundingRect != null) {

            try {
                boundingRect = Imgproc.boundingRect(ret);
                Log.v("BoundingRect", "[Origin size] w = " + w + ", h = " + h);
                Log.v("BoundingRect", "Ratio = " + wRatio);
                Log.v("BoundingRect", "[OpenCV Size] w = " + boundingRect.width + ", h = " + boundingRect.height + ", x = " + boundingRect.x + ", y = " + boundingRect.y);

                boundingRect.width = (int) (boundingRect.width * wRatio + (2 * PADDING));
                boundingRect.height = (int) (boundingRect.height * wRatio + (2 * PADDING));
                boundingRect.x = (int) (boundingRect.x * wRatio - PADDING);
                boundingRect.y = (int) (boundingRect.y * wRatio - PADDING);

                if (boundingRect.width > w) {
                    boundingRect.width = w;
                }

                if (boundingRect.height > h) {
                    boundingRect.height = h;
                }

                if (boundingRect.x < 0) {
                    boundingRect.x = 0;
                }

                if (boundingRect.y < 0) {
                    boundingRect.y = 0;
                }

                Log.v("BoundingRect", "[Camera Size] w = " + boundingRect.width + ", h = " + boundingRect.height + ", x = " + boundingRect.x + ", y = " + boundingRect.y);


                //Log.v("BoundingRect", "[Camera Size] w = " + boundingRect.width + ", h = " + boundingRect.height + ", x = " + boundingRect.x + ", y = " + boundingRect.y);
            } catch (Exception e) {

            }
        }

    }

    private void minusImage(Mat matFirst, Mat matSecond, int alpha) {
        minus(matFirst.nativeObj, matSecond.nativeObj, alpha);
    }

    class ImageAsync extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            imageProcessing(cvFrame);
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            boolProcess = false;

            if(boundingRect != null) {
                Imgproc.rectangle(matInput, boundingRect, new Scalar(0, 0, 255));
                Imgproc.resize(matResult, matResult, new Size(w, h));
            }
        }
    }

    //여기서부턴 퍼미션 관련 메소드
    static final int PERMISSIONS_REQUEST_CODE = 1000;
    String[] PERMISSIONS = {"android.permission.CAMERA"};

    private boolean hasPermissions(String[] permissions) {
        int result;

        //스트링 배열에 있는 퍼미션들의 허가 상태 여부 확인
        for (String perms : permissions) {

            result = ContextCompat.checkSelfPermission(this, perms);

            if (result == PackageManager.PERMISSION_DENIED) {
                //허가 안된 퍼미션 발견
                return false;
            }
        }

        //모든 퍼미션이 허가되었음
        return true;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {

            case PERMISSIONS_REQUEST_CODE:
                if (grantResults.length > 0) {
                    boolean cameraPermissionAccepted = grantResults[0]
                            == PackageManager.PERMISSION_GRANTED;

                    if (!cameraPermissionAccepted)
                        showDialogForPermission("앱을 실행하려면 퍼미션을 허가하셔야합니다.");
                }
                break;
        }
    }


    @TargetApi(Build.VERSION_CODES.M)
    private void showDialogForPermission(String msg) {

        AlertDialog.Builder builder = new AlertDialog.Builder(CameraActivity.this);
        builder.setTitle("알림");
        builder.setMessage(msg);
        builder.setCancelable(false);
        builder.setPositiveButton("예", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                requestPermissions(PERMISSIONS, PERMISSIONS_REQUEST_CODE);
            }
        });
        builder.setNegativeButton("아니오", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                finish();
            }
        });
        builder.create().show();
    }
}
