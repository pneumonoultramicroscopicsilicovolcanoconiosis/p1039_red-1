package com.ff0000.redproject;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageButton;

import com.isseiaoki.simplecropview.CropImageView;
import com.isseiaoki.simplecropview.callback.CropCallback;
import com.isseiaoki.simplecropview.callback.LoadCallback;
import com.isseiaoki.simplecropview.callback.SaveCallback;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

import io.flutter.plugin.common.MethodChannel;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class PreviewActivity extends AppCompatActivity implements Serializable {

    public static final String FOLDER = "/takebook/";
    public static final String FILE_CROPPED = "_cropped";

    CropImageView imgPreview;
    ImageButton btnSend;
    Button btnCrop;

    ArrayList<String> files = new ArrayList<>();
    private String token;
    private String folderPath;
    private Uri loadUrl;
    private Uri saveUrl;
    private float boundingRectX;
    private float boundingRectY;
    private float boundingRectWidth;
    private float boundingRectHeight;
    private float boundingRectX2;
    private float boundingRectY2;
    private float ratio;
    private boolean isCrop = false;
    private boolean isSend = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview);

        imgPreview = findViewById(R.id.imagePreview);
        btnSend = findViewById(R.id.btnSend);
        btnCrop = findViewById(R.id.btnCrop);

        Intent intent = getIntent();
        files = (ArrayList<String>) intent.getSerializableExtra("files");
        token = intent.getStringExtra("token");
        ratio = intent.getFloatExtra("ratio", 1.0f);
        boundingRectX = (intent.getIntExtra("boundingRectX", 0));
        boundingRectY = (intent.getIntExtra("boundingRectY", 0));
        boundingRectWidth = (intent.getIntExtra("boundingRectWidth", 100));
        boundingRectHeight = (intent.getIntExtra("boundingRectHeight", 100));

        boundingRectX2 = boundingRectX + boundingRectHeight;
        boundingRectY2 = boundingRectY + boundingRectWidth;

        Log.d("Preview token", token);

        for (String s : files) {
            Log.e("file", s);
        }

        folderPath = Environment.getExternalStorageDirectory() + FOLDER;

        loadUrl = Uri.parse("file:///" + folderPath + files.get(0) + ".jpg");
        saveUrl = Uri.parse("file:///" + folderPath + files.get(0) + FILE_CROPPED + ".jpg");

        //imgPreview.setInitialFrameScale(0.5f);
        imgPreview.load(loadUrl).initialFrameRect(new RectF(boundingRectX, boundingRectY, boundingRectX2, boundingRectY2)).execute(new LoadCallback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(Throwable e) {

            }
        });

        btnSend.setOnClickListener(v -> {
            if(!isSend) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("사진 전송 중").setMessage("사진을 서버에 전송하고 있습니다.\n서버 전송 후 사진을 분석하는데 다소 시간이 걸릴 수 있습니다.");
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
                //sendData(folderPath, files.get(0));
                Log.d("Preview", "Clicked Send Button");
                imgPreview.crop(loadUrl).execute(mCropCallback);
            }
        });

        btnCrop.setOnClickListener(v -> {
            if(isCrop) {
                isCrop = false;
                btnCrop.setText("직접 자르기");
            } else {
                isCrop = true;
                btnCrop.setText("직접 안 자르기");
            }
        });
    }

    private void deleteImage(String d) {
        File dir = new File(d);
        File[] files = dir.listFiles();

        if(dir.exists()) {
            for(File file : files) {
                file.delete();
            }
        }
    }

    private final CropCallback mCropCallback = new CropCallback() {
        @Override
        public void onSuccess(Bitmap cropped) {

            Log.d("Preview", "CropCallback");

            File sd = new File(Environment.getExternalStorageDirectory() + "/takebook");
            boolean success = true;

            if(!sd.exists()) {
                success = sd.mkdir();
                Log.d("Preview", "Make Directory");
            }

            if(success) imgPreview.save(cropped).execute(saveUrl, mSaveCallback);
        }

        @Override
        public void onError(Throwable e) {

        }
    };

    private final SaveCallback mSaveCallback = new SaveCallback() {
        @Override
        public void onSuccess(Uri uri) {

            Log.d("Preview", "SaveCallback");

            sendData(folderPath, files.get(0) + FILE_CROPPED);
        }

        @Override
        public void onError(Throwable e) {

        }
    };

    private void sendData(String folderPath, String fileName) {

        Log.d("Preview", "sendData");

        final MediaType MEDIA_TYPE_JPG = MediaType.parse("image/*");

        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("book_image", fileName, RequestBody.create(MEDIA_TYPE_JPG, new File(folderPath + fileName + ".jpg")))
                .build();

        Log.d("OkHttpToken", token);

        Request request = new Request.Builder()
                .url("https://api.takebook.org/Account/AnalyzeBookImage")
                .post(requestBody)
                .addHeader("Authorization", token)
                .build();

        OkHttpClient client = new OkHttpClient();

        Log.d("OkHttpToken", request.toString());

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                Log.d("OkHttp", "onFailure: " + e.getMessage());
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                Log.d("OkHttp", "onResponse: " + response.body().string());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        // Call the desired channel message here.
                        Log.e("Thread", "Handler");
                        FlutterResult flutterResult = FlutterResult.getInstance();
                        MethodChannel.Result result = flutterResult.getResult();
                        result.success("");
                        deleteImage(folderPath);
                        finish();
                    }
                });
            }
        });
    }
}
