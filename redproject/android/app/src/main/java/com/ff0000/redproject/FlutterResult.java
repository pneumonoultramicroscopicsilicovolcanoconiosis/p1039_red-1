package com.ff0000.redproject;

import io.flutter.plugin.common.MethodChannel;

public class FlutterResult {

    private static FlutterResult instance = null;
    private MethodChannel.Result result = null;

    private FlutterResult() {
    }

    public static FlutterResult getInstance() {
        if(instance == null) {
            instance = new FlutterResult();
        }

        return instance;
    }

    public void setResult(MethodChannel.Result result) {
        this.result = result;
    }

    public MethodChannel.Result getResult() {
        return result;
    }
}
