package com.ff0000.redproject;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.util.Log;

import io.flutter.app.FlutterActivity;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugins.GeneratedPluginRegistrant;

public class MainActivity extends FlutterActivity {
    private String CHANNEL = "CameraActivity";
    private int cameraMode = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GeneratedPluginRegistrant.registerWith(this);
        Log.i("HomeActivity", "oncreate");

        new MethodChannel(getFlutterView(), CHANNEL).setMethodCallHandler((methodCall, result) -> {
            if(methodCall.method.equals("startCameraActivity")) {
                FlutterResult flutterResult = FlutterResult.getInstance();
                flutterResult.setResult(result);
                String token = methodCall.argument("token").toString();
                cameraMode = methodCall.argument("cameraMode");
                startCameraActivity(token);
            }
        });
    }

    private String startCameraActivity(String token) {
        Intent intent = new Intent(MainActivity.this, CameraActivity.class);
        intent.putExtra("token", token);
        intent.putExtra("cameraMode", cameraMode);
        startActivity(intent);

        return "Success CameraActivity";
    }
}
