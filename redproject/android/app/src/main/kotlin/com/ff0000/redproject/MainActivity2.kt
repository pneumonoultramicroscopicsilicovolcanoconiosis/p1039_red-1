package com.ff0000.redproject

import android.content.Intent
import android.os.Bundle
import android.util.Log

import io.flutter.app.FlutterActivity
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugins.GeneratedPluginRegistrant

class MainActivity2: FlutterActivity() {
  private val CHANNEL = "CameraActivity"

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    GeneratedPluginRegistrant.registerWith(this)
    Log.i("HomeActivity", "oncreate")
    MethodChannel(flutterView, CHANNEL).setMethodCallHandler { call, result ->
      if (call.method == "startCameraActivity") {
        var token:String = call.argument<String>("token").toString()
        Log.d("Kotlin", token);
        result.success(startCameraActivity(token))
      }
    }
  }

  private fun startCameraActivity(token: String) {
    val intent = Intent(this, CameraActivity::class.java)
    intent.putExtra("token", token)
    //startActivityForResult(intent, 200)
    startActivity(intent)
  }
}
