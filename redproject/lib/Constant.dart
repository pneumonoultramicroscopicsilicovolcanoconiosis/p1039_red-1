final String HOME_SCREEN="/HOME_SCREEN";
final String CAMERA_SCREEN="/CAMERA_SCREEN";
final String PREVIEW_SCREEN="/PREVIEW_SCREEN";

final int BACKGROUND_COLOR = 0xFFEDEDED;
final int PRIMARY_COLOR = 0xFF7F0506;
final int ACCENT_COLOR = 0xFF2D3433;
final int TITLEBAR_COLOR = 0xFFFFFFFF;
final int CANVAS_COLOR = 0xFFFFFFFF;

final int COLOR_PINK = 0xFFe379a9;
final int COLOR_YELLOW = 0xFFfac13a;

final Set<String> SUCCESS_CODES = {
  'RS000',
  'RS001',
};

final Set<String> ERROR_CODES = {
  'EC001',
  'EC002',
  'EC003',
  'EC004',
  'EC005',
  'ES000',
  'ES001',
  'ES002',
  'ES003',
  'ES004',
  'ES010',
  'ES011',
  'ES012',
  'ES013',
  'EP000',
  'EP001',
};