import 'package:redproject/pack.dart';
import 'package:image_picker/image_picker.dart';
import 'package:redproject/DTO/Follower.dart';
import 'package:redproject/FollowerScreen.dart';
import 'dart:developer' as developer;
import 'package:async/async.dart';
import 'package:http/http.dart' as http;

class UserInfoScreen extends StatefulWidget {
  @override
  _UserInfoScreenState createState() => _UserInfoScreenState();
}

class _UserInfoScreenState extends State<UserInfoScreen> with SingleTickerProviderStateMixin {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final nameController = TextEditingController();
  final stateController = TextEditingController();
  TabController _tabController;
  UserManager userManager;
  VoidCallback onChanged;

  int pageNumber = 0;
  List<String> leftTab = ['assets/img_tab_left_enabled.png', 'assets/img_tab_left_disabled.png'];
  List<String> rightTab = ['assets/img_tab_right_disabled.png', 'assets/img_tab_right_enabled.png'];

  List<Follower> followers = List();
  List<Follower> followees = List();
  File _image;

  bool isEditing = false;
  bool isEditingImage = false;

  String userState = '';

  @override
  void initState() {
    _tabController = new TabController(length: 2, vsync: this);
    _tabController.animation.addListener(_tabAnimation);
    userManager = UserManager();

    getFollowerList();
    getFolloweeList();
    userState = userManager.stateMessage;

    onChanged = () {

      setState(() {
        pageNumber = this._tabController.index;
        print(_tabController.offset);

      });
    };
    _tabController.addListener(onChanged);
  }

  _tabAnimation() {
    double value = _tabController.animation.value;

    setState(() {
      if(value < 0.5) {
        pageNumber = 0;
      } else {
        pageNumber = 1;
      }
    });
  }

  isEdit() {
    setState(() {
      if(!isEditing) {
        isEditing = true;
        nameController.text = userManager.name;
        stateController.text = userManager.stateMessage;
      } else {
        isEditing = false;
        updateState();
        updateImage();
      }
    });
  }

  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);

    if(image == null) return;

    setState(() {
      _image = image;
       isEditingImage = true;
    });
  }

  Future updateState() async {
    var url = 'https://api.takebook.org/Account/UserInfo';

    TokenManager tokenManager = TokenManager();
    String token = await tokenManager.getToken();

    HttpClient httpClient = HttpClient();
    HttpClientRequest request = await httpClient.putUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    request.headers.add('Authorization', '$token');
    request.add(utf8.encode(json.encode({'state_message' : '${stateController.text}'})));

    developer.log(stateController.text, name: 'HttpClient');
    developer.log(request.toString(), name: 'HttpClient');

    HttpClientResponse response = await request.close();
    String statusCode = response.statusCode.toString();
    String result = await response.transform(utf8.decoder).join();

    httpClient.close();

    await userManager.reloadData();
    setState(() {
      userState = stateController.text;
    });
  }

  Future updateImage() async {

    if(_image != null) {
      // open a bytestream
      var stream = new http.ByteStream(
          DelegatingStream.typed(_image.openRead()));
      // get file length

      var length = await _image.length();

      // string to uri
      var uri = Uri.parse("https://api.takebook.org/Account/UserProfile");

      // create multipart request
      var request = new http.MultipartRequest("PUT", uri);

      // multipart that takes file
      var multipartFile = new http.MultipartFile(
          'profile_image', stream, length,
          filename: _image.path
              .split('/')
              .last);

      TokenManager tokenManager = TokenManager();
      String token = await tokenManager.getToken();

      // add file to multipart
      request.files.add(multipartFile);
      request.headers['Authorization'] = '$token';

      print(_image.path
          .split('/')
          .last);

      // send
      var response = await request.send();
      print(response.statusCode);

      // listen for response
      response.stream.transform(utf8.decoder).listen((value) {
        developer.log(value, name: 'UpdateImage');

        Map<String, dynamic> map = json.decode(value);
        String resultCode = map['Result_code'];
      });

      _image = null;
    }

    setState(() {
      userManager.reloadData();
    });

    Log.d('UserInfoScreen - updateImage', 'Complete Update Image');
  }

  Future getFollowerList() async {
    var url = 'https://api.takebook.org/Account/Follower';

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('userToken');

    var response = await http.get(url, headers: {'Authorization': '$token'});
    developer.log(response.body, name: 'Follower List');
    Log.d('UserInfoScreen', response.body);

    ResultManager resultManager = ResultManager(response);
    if(!resultManager.isSuccess()) {
      // 실패
      String resultCode = resultManager.getResultCode()[0];
      MessageSnack.show(_scaffoldKey, '팔로워 목록을 불러오는데 실패했습니다.');
      developer.log(resultManager.getResultCode()[1], name: 'TakeBook - ERROR');
      return;
    }

    Map<String, dynamic> map = resultManager.getResult();
    var item = map['item'] as List;

    setState(() {
      followers = item.map<Follower>((json) => Follower.fromJson(json)).toList();
    });
  }

  Future getFolloweeList() async {
    var url = 'https://api.takebook.org/Account/Followee';

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('userToken');

    var response = await http.get(url, headers: {'Authorization': '$token'});
    developer.log(response.body, name: 'Follower List');
    Log.d('UserInfoScreen', response.body);

    ResultManager resultManager = ResultManager(response);
    if(!resultManager.isSuccess()) {
      // 실패
      String resultCode = resultManager.getResultCode()[0];
      MessageSnack.show(_scaffoldKey, '팔로워 목록을 불러오는데 실패했습니다.');
      developer.log(resultManager.getResultCode()[1], name: 'TakeBook - ERROR');
      return;
    }

    Map<String, dynamic> map = resultManager.getResult();
    var item = map['item'] as List;

    setState(() {
      followees = item.map<Follower>((json) => Follower.fromJson(json)).toList();
    });
  }

  Future changeFollow(List<Follower> list, int index) {
    Log.d('UserInfoScreen - changeFollow', 'isFollow = ${list[index].isFollow}');

    if(list[index].isFollow == 0) {
      doFollow(list[index].userID);
    } else {
      cancelFollow(list[index].userID);
    }
  }

  Future doFollow(String ID) async {
    String token = await TokenManager().getToken();

    var response = await http.post(
        'https://api.takebook.org/Account/FollowUser',
        headers: {'Authorization': '$token'},
        body: {
          'followee_id' : '$ID',
        }
    );

    Log.d('UserInfoScreen - doFollow', 'followee_id = $ID');
    Log.d('UserInfoScreen - doFollow', '$token');

    ResultManager resultManager = ResultManager(response);
    String resultCode = resultManager.getResultCode()[0];
    String resultMessage = resultManager.getResultCode()[1];
    String msg = '$resultCode :: $resultMessage';

    Log.d('UserInfoScreen - doFollow', '${resultManager.getFullMessage()}');

    if(!resultManager.isSuccess()) {
      // 실패
      MessageSnack.show(_scaffoldKey, '구독하는데 오류가 발생했습니다.');
      developer.log(resultManager.getResultCode()[1], name: 'TakeBook - ERROR');
      return;
    }

    followers.clear();
    followees.clear();
    getFollowerList();
    getFolloweeList();
  }

  Future cancelFollow(String ID) async {
    String token = await TokenManager().getToken();
    var url = 'https://api.takebook.org/Account/FollowUser';

    Log.d('UserInfoScreen - cancelFollow', 'followee_id = $ID');
    Log.d('UserInfoScreen - cancelFollow', '$token');

    HttpClient httpClient = HttpClient();
    HttpClientRequest request = await httpClient.deleteUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    request.headers.add('Authorization', '$token');
    request.add(utf8.encode(json.encode({'followee_id' : '$ID'})));

    HttpClientResponse response = await request.close();

    int statusCode = int.parse(response.statusCode.toString());

    if(statusCode != 200) {
      return;
    }

    String result = await response.transform(utf8.decoder).join();
    Map<String, dynamic> map = json.decode(result);
    String resultCode = map['Result_Code'];
    String resultMessage = map['Message'];
    String msg = '$resultCode :::: $resultMessage';

    Log.d('UserInfoScreen - cancelFollow', '$msg');

    if(ERROR_CODES.contains(resultCode)) {
      // 실패

      MessageSnack.show(_scaffoldKey, '구독을 취소하는데 오류가 발생했습니다.');
      return;
    }

    followers.clear();
    followees.clear();
    getFollowerList();
    getFolloweeList();
  }

  Widget editButton() {
    if(isEditing) {
      return GestureDetector(
        onTap: (){
          isEdit();
        },
        child: Container(
          color: Colors.transparent,
          height: 50,
          child: Row(
            children: <Widget>[
              Container(
                width: 16,
                height: 16,
                margin: EdgeInsets.only(right: 8),
                child: Image.asset('assets/icon_done.png'),
              ),
              Text('완료', style: TextStyle(fontSize: 14)),
            ],
          ),
        ),
      );
    } else {
      return GestureDetector(
        onTap: (){
          isEdit();
        },
        child: Container(
          color: Colors.transparent,
          height: 50,
          child: Row(
            children: <Widget>[
              Container(
                width: 16,
                height: 16,
                margin: EdgeInsets.only(right: 8),
                child: Image.asset('assets/icon_edit.png'),
              ),
              Text('프로필 수정', style: TextStyle(fontSize: 14)),
            ],
          ),
        )
      );
    }
  }

  Widget titleBar() {
    return Container(
      margin: EdgeInsets.fromLTRB(0, MediaQuery.of(context).padding.top, 0, 0),
      height: 50,
      child: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment.centerLeft,
            child: GestureDetector(
              onTap: (){
                if(!isEditing) {
                  Navigator.of(context).pop();
                } else {
                  setState(() {
                    isEditing = false;
                    _image = null;
                  });
                }
              },
              child: Container(
                width: 18,
                height: 18,
                margin: EdgeInsets.only(left: 12),
                child: Image.asset('assets/icon_back.png'),
              ),
            ),
          ),
          Align(
            alignment: Alignment.centerRight,
            child: Padding(
              padding: EdgeInsets.fromLTRB(0, 0, 12, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  editButton()
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget editProfile() {
    if(!isEditing) {
      return Container(
        color: Colors.white,
        height: 130,
        padding: EdgeInsets.all(16),
        child: Stack(
          children: <Widget>[
            Align(
              alignment: Alignment.centerLeft,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(userManager.name, style: TextStyle(fontSize: 28),),
                  Text(userState)
                ],
              ),
            ),
            Align(
              alignment: Alignment.centerRight,
              child: ProfileImage(
                userManager.profileURL,
                width: 80,
                height: 80,
              ),
            ),
          ],
        ),
      );
    } else {
      return Container(
        color: Colors.white,
        height: 130,
        padding: EdgeInsets.all(16),
        child: Stack(
          children: <Widget>[
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                width: 200,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(userManager.name, style: TextStyle(fontSize: 28)),
                    TextField(
                      controller: stateController,
                    )
                  ],
                ),
              ),
            ),
            Align(
              alignment: Alignment.centerRight,
              child: GestureDetector(
                onTap: () => { getImage() },
                child: Container(
                  width: 80,
                  height: 80,
                  child: Stack(
                    children: <Widget>[
                      Container(
                        width: 80,
                        height: 80,
                        child: CircleAvatar(
                          minRadius: 40,
                          backgroundImage: profileImage(),
                        ),
                      ),

                      Align(
                        alignment: Alignment.bottomRight,
                        child: Image.asset('assets/icon_edit_image.png', width: 30),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      );
    }
  }

  ImageProvider profileImage() {

    if(_image != null) {
      return FileImage(_image);
    }

    if(isEditingImage && _image != null) {
      return FileImage(_image);
    }

    if(userManager.profileURL == null) {
      return AssetImage('assets/icon_profile_norm.png');
    } else {
      return NetworkImage(userManager.profileURL);
    }
  }

  ImageProvider followerImage(String url) {
    if(url == null) {
      return AssetImage('assets/icon_profile_norm.png');
    } else {
      return NetworkImage(url);
    }
  }

  Widget followButton(int follow) {
    if(follow == 0) {
      return Container(
        width: 90,
        height: 25,
        decoration: BoxDecoration(
          color: Color(COLOR_YELLOW),
          borderRadius: BorderRadius.circular(8),
        ),
        child: Align(
          alignment: Alignment.center,
          child: Text('구독', style: TextStyle(fontSize: 12),),
        )
      );
    } else {
      return Container(
        width: 90,
        height: 25,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(8),
            border: Border.all(
                color: Colors.grey,
                width: 1
            )
        ),
        child: Align(
          alignment: Alignment.center,
          child: Text('구독 취소', style: TextStyle(fontSize: 12),),
        )
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: Column(
        children: <Widget>[
          titleBar(),
          editProfile(),
          Container(
            width: MediaQuery.of(context).size.width,
            child: Container(
              height: 50,
                child: Row(
                  children: <Widget>[
                    Flexible(
                      flex: 1,
                      child: GestureDetector(
                        onTap: (){
                          _tabController.animateTo(0);
                        },
                        child: Container(
                            width: MediaQuery.of(context).size.width / 2,
                            child: Stack(
                              children: <Widget>[
                                Image.asset(
                                  leftTab[pageNumber % 2],
                                  fit: BoxFit.fill,
                                ),
                                Align(
                                  alignment: Alignment.center,
                                  child: Container(
                                    padding: EdgeInsets.only(bottom: 12),
                                    child: Text('구독자'),
                                  ),
                                ),
                              ],
                            )
                        ),
                      )
                    ),
                    Flexible(
                        flex: 1,
                        child: GestureDetector(
                          onTap: (){
                            _tabController.animateTo(1);
                          },
                          child: Stack(
                            children: <Widget>[
                              Image.asset(
                                rightTab[pageNumber % 2],
                                fit: BoxFit.fill,
                              ),
                              Align(
                                alignment: Alignment.center,
                                child: Container(
                                  padding: EdgeInsets.only(bottom: 12),
                                  child: Text('구독중'),
                                ),
                              ),
                            ],
                          ),
                        )
                    )
                  ],
                )
            )
        ),
          Expanded(
              child: Container(
                child: Stack(
                  children: <Widget>[
                    TabBarView(
                      controller: _tabController,
                      children: <Widget>[
                        firstPage(followers),
                        firstPage(followees)
                      ],
                    ),
                  ],
                ),
              )
          )
        ],
      ),
    );
  }

  Widget firstPage(List<Follower> list) {
    return Container(
        child: ListView.builder(
            scrollDirection: Axis.vertical,
            itemCount: list.length,
            itemBuilder: (BuildContext context, int index) => GestureDetector(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context) => FollowerScreen(list[index].userID)));
              },
              child: Stack(
                children: <Widget>[
                  Container(
                    color: Colors.white,
                    height: 100,
                    padding: EdgeInsets.all(8),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          width: 50,
                          height: 50,
                          child: CircleAvatar(
                            minRadius: 30,
                            backgroundImage: followerImage(list[index].profileURL),
                          ),
                        ),
                        Container(
                          width: 8,
                        ),
                        Expanded(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(list[index].name, style: TextStyle(fontSize: 14),),
                              Text(list[index].userID, style: TextStyle(fontSize: 12))
                            ],
                          ),
                        ),
                        GestureDetector(
                          onTap: (){
                            changeFollow(list, index);
                          },
                          child: followButton(list[index].isFollow),
                        ),
                      ],
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      height: 1,
                      color: Colors.black12,
                    ),
                  ),
                ],
              )
            )
        )
    );
  }

  Widget secondPage() {
    return Container(
        color: Colors.blue
    );
  }
}
