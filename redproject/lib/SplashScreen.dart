import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:redproject/Constant.dart';
import 'package:redproject/LoginScreen.dart';

class SplashScreen extends StatefulWidget {
  @override
  SplashScreenState createState() => new SplashScreenState();
}

class SplashScreenState extends State<SplashScreen> with SingleTickerProviderStateMixin {
  var _visible = true;

  startTime() async {
    var _duration = new Duration(seconds: 2);
    return new Timer(_duration, navigationPage);
  }

  void navigationPage() {
    Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (BuildContext context) => LoginScreen()));
  }

  @override
  void initState() {
    super.initState();

    startTime();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                      colors: [Color(COLOR_YELLOW), Color(COLOR_PINK)]
                  )
              ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text('책을 만나는 순간', style: TextStyle(color: Colors.white, fontSize: 20),),
              Container(
                width: 200,
                height: 200,
                margin: EdgeInsets.all(12),
                child: Image.asset('assets/logo_camera_white.png'),
              ),
              Container(
                width: 120,
                margin: EdgeInsets.all(12),
                child: Image.asset('assets/icon_title_splash.png', color: Colors.white,),
              ),
              Container(
                height: 100,
              ),
            ],
          )
        ],
      )
    );
  }
}
