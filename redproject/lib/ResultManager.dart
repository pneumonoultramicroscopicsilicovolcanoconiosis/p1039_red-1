import 'dart:convert';
import 'package:redproject/Constant.dart';
import 'dart:developer' as developer;
import 'package:redproject/Log.dart';
import 'dart:io';

class ResultManager{

  var response;
  HttpClientResponse deleteResponse;
  Map<String, dynamic> map = Map();
  int statusCode;
  String resultCode;
  String message;

  Set<String> successCode = Set();
  Set<String> errorCode = Set();

  ResultManager(this.response) {

    statusCode = response.statusCode;

    if(statusCode == 200) {
      map = json.decode(utf8.decode(response.bodyBytes));
      resultCode = map['Result_Code'];
      message = map['Message'];
    }
  }

  ResultManager.delete(this.deleteResponse) {
    Log.d('ResultManager.delete', 'call delete');

    statusCode = int.parse(deleteResponse.statusCode.toString());

    if(statusCode == 200) {
      _decodeResponse(deleteResponse);
    }
  }

  Future _decodeResponse(HttpClientResponse res) async {
    String result = await res.transform(utf8.decoder).join();
    map = json.decode(result);
    resultCode = map['Result_Code'];
    message = map['Message'];
  }

  bool isSuccess() {
    if(SUCCESS_CODES.contains(resultCode)) {
      return true;
    } else if(ERROR_CODES.contains(resultCode)) {
      return false;
    } else {
      return false;
    }
  }

  /// Response 메시지의 Result_Code와 Message를 List로 반환
  ///
  /// index 0 = Result_Code
  /// index 1 = Message
  List<String> getResultCode() {
    return [resultCode, message];
  }

  /// Response 메시지의 Response 객체 반환
  Map<String, dynamic> getResult() {
    return map['Response'];
  }

  String getFullMessage({String text}) {
    String msg;

    if(text == null) {
      msg = '';
    } else {
      msg = text;
    }

    if(msg != '') {
      msg = '$text\n';
    }

    return '$msg$resultCode :::: $message';
  }

}