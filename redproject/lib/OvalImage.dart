import 'package:flutter/material.dart';

class OvalImage extends StatefulWidget {

  Widget widget;
  double width;
  double height;

  OvalImage.assets(
    Widget widget,
    {
      this.width,
      this.height
    });

  @override
  _OvalImageState createState() => _OvalImageState();
}

class _OvalImageState extends State<OvalImage> {
  @override
  Widget build(BuildContext context) {
    return ClipOval(
        child: Container(
            width: 70,
            height: 70,
            child: Container(
              padding: EdgeInsets.all(16),
              child: Image.asset(
                  'assets/icon_camera_norm.png',
                  width: 30,
                  height: 30
              ),
            )
        )
    );
  }
}
