import 'package:redproject/pack.dart';
import 'package:redproject/FolderScreen.dart';
import 'package:redproject/ScrapViewScreen.dart';
import 'package:redproject/DTO/Scrap.dart';
import 'package:http/http.dart' as http;
import 'dart:developer' as developer;

class ScrapListScreen extends StatefulWidget {

  String scrapFolder;
  String folderName;

  ScrapListScreen(this.scrapFolder, this.folderName);

  @override
  _ScrapListScreenState createState() => _ScrapListScreenState();
}

class _ScrapListScreenState extends State<ScrapListScreen> with WidgetsBindingObserver {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  List<Scrap> scraps = List();
  List<String> selectedScrap = List();

  bool isSelectMode = false;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    getScrapList();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    print('life sycle = $state');
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  void selectMode() {
    setState(() {
      if(!isSelectMode) {
        isSelectMode = true;
      } else {
        isSelectMode = false;
      }
    });
  }

  void clickItem(int index) async {
    // 삭제 모드일 때
    if(isSelectMode) {
      setState(() {
        if(!scraps[index].isSelected) {
          scraps[index].isSelected = true;
          selectedScrap.add(scraps[index].scrapId);
        } else {
          scraps[index].isSelected = false;
          selectedScrap.remove(scraps[index].scrapId);
        }
      });

    } else {
      await Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => ScrapViewScreen(scraps[index], widget.folderName)));
      getScrapList();
    }
  }

  getScrapList() async {
    var url = 'https://api.takebook.org/Account/UserScrap?folder_id=${widget.scrapFolder}';

    TokenManager tokenManager = TokenManager();
    String token = await tokenManager.getToken();

    var response = await http.get(url, headers: {'Authorization' : '$token'});
    ResultManager resultManager = ResultManager(response);
    if(!resultManager.isSuccess()) {
      // 실패
      String resultCode = resultManager.getResultCode()[0];
      MessageSnack.show(_scaffoldKey, resultManager.getResultCode()[1]);
      developer.log(resultManager.getResultCode()[1], name: 'TakeBook - ERROR');
      return;
    }

    developer.log(response.body, name: 'TakeBook - SUCCESS');

    Map<String, dynamic> map = resultManager.getResult();
    var item = map['item'] as List;

    setState(() {
      scraps = item.map<Scrap>((json) => Scrap.fromJson(json)).toList();
    });

    if(scraps.length == 0) {
      Navigator.pop(context);
    }
  }

  void removeScrap() async {

    var url = 'https://api.takebook.org/Account/UserScrap';

    TokenManager tokenManager = TokenManager();
    String token = await tokenManager.getToken();

    HttpClient httpClient = HttpClient();
    HttpClientRequest request = await httpClient.deleteUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    request.headers.add('Authorization', '$token');
    request.add(utf8.encode(json.encode({'scrap_id' : selectedScrap.toList()})));

    Log.d('ScrapListScreen - removeScrap', 'scrap_id = ${selectedScrap.toList()}');

    HttpClientResponse response = await request.close();

    if(response.statusCode != 200) {
      return;
    }
    httpClient.close();

    String result = await response.transform(utf8.decoder).join();
    Map<String, dynamic> map = json.decode(result);
    String resultCode = map['Result_Code'];
    String resultMessage = map['Message'];
    String msg = '$resultCode :::: $resultMessage';

    Log.d('ScrapListScreen - removeScrap', '$msg');

    if(ERROR_CODES.contains(resultCode)) {
      // 실패
      MessageSnack.show(_scaffoldKey, '스크랩을 삭제하는데 오류가 발생했습니다.');
      return;
    }

    MessageSnack.show(_scaffoldKey, '스크랩을 삭제했습니다.');
    selectedScrap.clear();

    setState(() {
      isSelectMode = false;
    });

    getScrapList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Color.fromRGBO(237, 237, 237, 1.0),
      body: Column(
        children: <Widget>[
          Stack(
            children: <Widget>[
              titleBar(),
              Visibility(
                visible: isSelectMode,
                child: Container(
                  height: 50,
                  margin: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
                  color: Colors.white,
                  child: Stack(
                    children: <Widget>[
                      Align(
                          alignment: Alignment.centerLeft,
                          child: GestureDetector(
                            onTap: (){
                              selectMode();
                            },
                            child: Container(
                              width: 18,
                              height: 18,
                              margin: EdgeInsets.only(left: 12),
                              child: Image.asset('assets/icon_close.png'),
                            ),
                          )
                      ),
                      Align(
                        alignment: Alignment.centerRight,
                        child: Container(
                          margin: EdgeInsets.only(right: 12),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              GestureDetector(
                                onTap: () async {
                                  if (selectedScrap.length > 0) {
                                    String s = await Navigator.push(
                                        context,
                                        MaterialPageRoute(builder: (BuildContext context) => FolderScreen(selectedScrap)));

                                    if(s == '000') {
                                      MessageSnack.show(_scaffoldKey, '폴더를 이동했습니다.');

                                      setState(() {
                                        selectedScrap.clear();
                                        isSelectMode = false;
                                      });
                                    }

                                  } else {
                                    MessageSnack.show(_scaffoldKey, '이동할 스크랩을 선택해주세요');
                                  }
                                },
                                child: Row(
                                  children: <Widget>[
                                    Container(
                                      width: 16,
                                      height: 16,
                                      margin: EdgeInsets.only(right: 8),
                                      child: Image.asset('assets/icon_edit.png'),
                                    ),
                                    Text('이동', style: TextStyle(fontSize: 14)),
                                  ],
                                ),
                              ),

                              Container(
                                width: 12,
                              ),
                              GestureDetector(
                                onTap: (){
                                  removeScrap();
                                },
                                child: Row(
                                  children: <Widget>[
                                    Container(
                                      width: 16,
                                      height: 16,
                                      margin: EdgeInsets.only(right: 8),
                                      child: Image.asset('assets/icon_trash.png'),
                                    ),
                                    Text('삭제', style: TextStyle(fontSize: 14)),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
          Expanded(
            child: Container(
                child: ListView.builder(
                    scrollDirection: Axis.vertical,
                    itemCount: scraps.length,
                    itemBuilder: (BuildContext context, int index) => GestureDetector(
                        onTap: (){
                          clickItem(index);
                        },
                        child: Container(
                          height: 160,
                          padding: EdgeInsets.all(8),
                          child: Container(
                            child: Stack(
                              children: <Widget>[
                                Container(
                                  width: double.infinity,
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(30),
                                  ),
                                ),
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Container(
                                    padding: EdgeInsets.symmetric(horizontal: 12),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: <Widget>[
                                        Container(
                                          width: 60,
                                          height: 60,
                                          margin: EdgeInsets.only(right: 8),
                                          child: Image.network(scraps[index].imageURL, fit: BoxFit.cover,),
                                        ),
                                        Expanded(
                                          child: Text(
                                            scraps[index].contents,
                                            maxLines: 5,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Align(
                                  alignment: Alignment.bottomRight,
                                  child: Container(
                                    margin: EdgeInsets.only(right: 16, bottom: 16),
                                    child: Text(scraps[index].creationDate, style: TextStyle(fontSize: 12)),
                                  )
                                ),
                                deleteBox(index),
                              ],
                            ),
                          ),
                        )))
            ),
          ),
        ],
      ),
    );
  }

  Widget _simplePopup() {

    int _btnVal;

    return PopupMenuButton<int>(
      itemBuilder: (context) =>
      [
        PopupMenuItem(
          value: 1,
          child: Text("선택"),
        ),
      ],
      onSelected: (int newValue) {
        _btnVal = newValue;

        switch (_btnVal) {
          case 1:
            selectMode();
            break;
        }
      },
      child: Container(
        width: 20,
        height: 20,
        margin: EdgeInsets.fromLTRB(4, 0, 4, 0),
        child: Image.asset(
          'assets/icon_more_norm.png',
          width: 20,
          height: 20,
        ),
      ),
    );
  }

  Widget titleBar() {
    return Container(
      color: Colors.white,
      margin: EdgeInsets.fromLTRB(0, MediaQuery
          .of(context)
          .padding
          .top, 0, 0),
      height: 50,
      child: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              height: 1,
              color: Colors.black12,
            ),
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: GestureDetector(
              onTap: () {
                Navigator.of(context).pop();
              },
              child: Container(
                width: 18,
                height: 18,
                margin: EdgeInsets.only(left: 12),
                child: Image.asset('assets/icon_back.png'),
              ),
            ),
          ),
          Align(
              alignment: Alignment.center,
              child:
              Text(widget.folderName)
          ),
          Align(
            alignment: Alignment.centerRight,
            child: Padding(
              padding: EdgeInsets.fromLTRB(0, 0, 12, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  _simplePopup()
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget deleteBox(int index) {
    bool visibility = false;

    if(isSelectMode) visibility = scraps[index].isSelected;
    else visibility = false;

    return Visibility(
      visible: visibility,
      child: Align(
        alignment: Alignment.topLeft,
        child: Image.asset('assets/icon_selected.png', width: 30),
      ),
    );
  }
}
