import 'package:redproject/pack.dart';
import 'package:redproject/DTO/BookSearch.dart';
import 'package:redproject/DTO/BookCover.dart';
import 'package:redproject/DTO/Scrap.dart';
import 'package:http/http.dart' as http;
import 'package:async/async.dart';
import 'dart:developer' as developer;

class ScrapPreviewScreen extends StatefulWidget {

  String imagePath;
  Scrap scrap;

  ScrapPreviewScreen({this.imagePath, this.scrap});

  @override
  _ScrapPreviewScreenState createState() => _ScrapPreviewScreenState();
}

class _ScrapPreviewScreenState extends State<ScrapPreviewScreen> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final sourceController = TextEditingController();
  final searchController = TextEditingController();
  final analyzeController = TextEditingController();

  Scrap scrap;

  List<ScrapSearch> searchResult = List();
  List<BookCover> myResult = List();

  String scrapID;
  String isbn;

  bool isSuccess = false;

  @override
  void initState() {
    if(widget.imagePath != null) {
      uploadImage();
    } else {
      scrap = widget.scrap;
      scrapID = scrap.scrapId;
      sourceController.text = scrap.source;
      analyzeController.text = scrap.contents;
      isSuccess = true;
    }
  }

  void search() async {
    String keyword = searchController.text;

    var url = 'https://api.takebook.org/Elasticsearch/Book?keyword=$keyword&category=title';

    TokenManager tokenManager = TokenManager();
    String token = await tokenManager.getToken();

    var response = await http.get(url);

    developer.log(response.body, name: 'Scrap - Search');

    ResultManager resultManager = ResultManager(response);
    if (!resultManager.isSuccess()) {
      // 실패
      String resultCode = resultManager.getResultCode()[0];
      MessageSnack.show(_scaffoldKey, '검색 결과를 불러오는데 실패했습니다.');
      developer.log(resultManager.getResultCode()[1], name: 'TakeBook - ERROR');
      return;
    }

    Map<String, dynamic> map = resultManager.getResult();
    var item = map['item'] as List;

    setState(() {
      searchResult = item.map<ScrapSearch>((json) => ScrapSearch.fromJson(json)).toList();
    });

    getBookList();
  }

  void getBookList() async {
    String keyword = searchController.text;

    var url = 'https://api.takebook.org/Account/UserBook?keyword=$keyword&category=title';

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('userToken');

    var response = await http.get(url, headers: {'Authorization': '$token'});

    ResultManager resultManager = ResultManager(response);
    if(!resultManager.isSuccess()) {
      // 실패
      String resultCode = resultManager.getResultCode()[0];
      MessageSnack.show(_scaffoldKey, '책 목록을 불러오는데 실패했습니다.');
      developer.log(resultManager.getResultCode()[1], name: 'TakeBook - ERROR');
      return;
    }

    Map<String, dynamic> map = resultManager.getResult();
    var item = map['item'] as List;

    setState(() {
      myResult = item.map<BookCover>((json) => BookCover.fromJson(json)).toList();
    });

  }

  void uploadImage() async {
    developer.log('upload', name: 'Scrap - Upload');
    String folderName = sourceController.text;
    File imageFile = File(widget.imagePath);

    var stream = new http.ByteStream(DelegatingStream.typed(imageFile.openRead()));
    var length = await imageFile.length();

    developer.log('Length = $length', name: 'Scrap - Upload');

    var uri = Uri.parse("https://api.takebook.org/Account/UserScrap");
    var request = new http.MultipartRequest("POST", uri);

    developer.log('URI', name: 'Scrap - Upload');
    developer.log(uri.toString(), name: 'Scrap - Upload');

    var multipartFile = new http.MultipartFile('image_file', stream, length,
        filename: imageFile.path.split('/').last);

    TokenManager tokenManager = TokenManager();
    String token = await tokenManager.getToken();

    request.files.add(multipartFile);
    request.headers['Authorization'] = token;

    developer.log(token, name: 'Scrap - Upload');

    var response = await request.send();
    developer.log(response.toString(), name: 'Scrap - Upload');

    response.stream.transform(utf8.decoder).listen((value) {
      Map<String, dynamic> map = json.decode(value);

      String errorCode = map['Result_Code'];
      if(errorCode != 'RS000') {
        MessageSnack.show(_scaffoldKey, '사진을 서버로 전송하는데 실패했습니다. 초기 화면으로 돌아갑니다.');
        Navigator.of(context).pop();
        return;
      }

      final dir = Directory(imageFile.path);
      dir.deleteSync(recursive: true);

      scrapID = map['Response']['scrap_id'];
      analyzeScrapImage(scrapID);

    });
  }

  Future analyzeScrapImage(String scrapID) async {
    var url = 'https://api.takebook.org/Account/AnalyzeScrapImage?scrap_id=$scrapID';

    String token = await TokenManager().getToken();

    var response = await http.get(url, headers: {'Authorization': '$token'});

    ResultManager resultManager = ResultManager(response);
    String resultCode = resultManager.getResultCode()[0];

    Log.d('ScrapPreviewScreen - analyzeScrapImage', response.body);
    Log.d('ScrapPreviewScreen - analyzeScrapImage', resultManager.getFullMessage());

    if(!resultManager.isSuccess()) {
      // 실패
      setState(() {
        isSuccess = true;
      });
      MessageSnack.show(_scaffoldKey, '분석에 오류가 발생했습니다.');
      return;
    }

    Map<String, dynamic> map = resultManager.getResult();
    String text = map['text'];

    setState(() {
      analyzeController.text = text;
      isSuccess = true;
    });
  }

  void startUpload(String isbn) async {
    editContext(isbn);
  }

  Future editContext(String isbn) async {
    String token = await TokenManager().getToken();

    String text = analyzeController.text;

    if(text == '') {
      text = '정보 없음';
    }

    Map<String, dynamic> mapBody = Map();

    if(sourceController.text != '') {
      mapBody['modify_source_title'] = sourceController.text;
      mapBody['modify_source_isbn'] = isbn;
    }

    var response = await http.put(
        'https://api.takebook.org/Account/UserScrap',
        headers: {'Authorization': '$token'},
        body: {
          'scrap_id' : '$scrapID',
          'modify_contents' : '$text',
          'modify_source_title' : '${sourceController.text}',
          'modify_source_isbn' : '$isbn'
        }
    );

    ResultManager resultManager = ResultManager(response);
    String resultCode = resultManager.getResultCode()[0];
    String resultMessage = resultManager.getResultCode()[1];
    String msg = '$resultCode :: $resultMessage';

    Log.d('ScrapPreviewScreen = editContext', response.body);
    Log.d('ScrapPreviewScreen = editContext', resultManager.getFullMessage());

    if(!resultManager.isSuccess()) {
      // 실패
      MessageSnack.show(_scaffoldKey, '스크랩을 수정하는데 오류가 발생했습니다.');
      return;
    }

    Navigator.pop(context);
  }

  Future editSource() async {
    String token = await TokenManager().getToken();

    var response = await http.put(
        'https://api.takebook.org/Account/UserScrapSource',
        headers: {'Authorization': '$token'},
        body: {
          'scrap_id' : '$scrapID',
          'modify_source' : '${sourceController.text}'
        }
    );

    ResultManager resultManager = ResultManager(response);
    String resultCode = resultManager.getResultCode()[0];
    String resultMessage = resultManager.getResultCode()[1];
    String msg = '$resultCode :: $resultMessage';

    Log.d('ScrapPreviewScreen = editContext', response.body);
    Log.d('ScrapPreviewScreen = editContext', resultManager.getFullMessage());

    if(!resultManager.isSuccess()) {
      // 실패
      MessageSnack.show(_scaffoldKey, '출처를 수정하는데 오류가 발생했습니다.');
      return;
    }

    Navigator.pop(context);
  }

  Widget bookImage(String imageURL) {
    double w = 70;

    if(imageURL == null) {
      return Container(
        color: Color(BACKGROUND_COLOR),
        width: w,
      );
    } else {
      return Image.network(imageURL, width: w);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Color(BACKGROUND_COLOR),
      body: Column(
        children: <Widget>[
          titleBar(),
          Expanded(
            child: SingleChildScrollView(
              padding: EdgeInsets.all(0),
              child: Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(24),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(bottomRight: Radius.circular(20), bottomLeft: Radius.circular(20)),
                    ),
                    child: Column(
                      children: <Widget>[
                        Container(
                          width: double.infinity,
                          height: 300,
                          child: viewImage(),
                        ),
                        Container(
                          child: Column(
                              children: <Widget>[
                                Container(
                                    height: 24
                                ),
                                Stack(
                                  children: <Widget>[
                                    Container(
                                      width: MediaQuery.of(context).size.width,
                                      color: Colors.white,
                                      child: TextField(
                                        maxLines: null,
                                        controller: analyzeController,
                                        decoration: InputDecoration(
                                            hintText: '텍스트 결과 없음',
                                            filled: true,
                                            fillColor: Colors.white,
                                            contentPadding: EdgeInsets.all(4),
                                            border: InputBorder.none
                                        ),
                                      ),
                                    ),
                                    Visibility(
                                      visible: !isSuccess,
                                      child: Align(
                                        alignment: Alignment.center,
                                        child: Container(
                                            width: MediaQuery.of(context).size.width,
                                            height: 25,
                                            color: Colors.white,
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children: <Widget>[
                                                Container(
                                                  width: 25,
                                                  height: 25,
                                                  child: CircularProgressIndicator(
                                                    valueColor: new AlwaysStoppedAnimation<Color>(Color(COLOR_PINK)),
                                                  ),
                                                ),
                                                Container(
                                                  width: 12,
                                                ),
                                                Text('텍스트 추출 중')
                                              ],
                                            )
                                        ),
                                      )
                                    ),
                                  ],
                                ),
                                Container(
                                    height: 24
                                ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Text('출처 - '),
                                    Flexible(
                                      child: TextField(
                                        controller: sourceController,
                                        decoration: InputDecoration(
                                          filled: true,
                                          fillColor: Color(BACKGROUND_COLOR),
                                          contentPadding: EdgeInsets.all(4),
                                          border: InputBorder.none
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ]
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(height: 12),
                  Container(
                    color: Colors.white,
                    padding: EdgeInsets.all(24),
                    child: Stack(
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(left: 8, right: 30),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(30),
                            color: Color(BACKGROUND_COLOR)
                          ),
                          child: TextField(
                            controller: searchController,
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.all(10),
                              border: InputBorder.none,
                            ),
                            style: TextStyle(fontSize: 14),
                          ),
                        ),
                        Align(
                          alignment: Alignment.centerRight,
                          child: Container(
                            child: ClipOval(
                                child: GestureDetector(
                                    onTap: (){
                                      FocusScope.of(context).requestFocus(new FocusNode());
                                      search();
                                    },
                                    child: Container(
                                        width: 34,
                                        height: 34,
                                        decoration: BoxDecoration(
                                          color: Color(COLOR_YELLOW)
                                        ),
                                        child: Container(
                                          padding: EdgeInsets.all(8),
                                          child: Image.asset(
                                            'assets/icon_search_norm.png',
                                            width: 30,
                                            height: 30,
                                            color: Colors.white,
                                          ),
                                        )
                                    )
                                )
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Column(
                    children: <Widget>[
                      Container(
                        color: Colors.white,
                        height: 200,
                        child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount: searchResult.length,
                            itemBuilder: (BuildContext context, int index) => GestureDetector(
                                onTap: (){
                                  setState(() {
                                    sourceController.text = searchResult[index].title;
                                    isbn = searchResult[index].isbn;
                                  });
                                },
                                child: Container(
                                  width: 80,
                                  child: Column(
                                    children: <Widget>[
                                      bookImage(searchResult[index].imageURL),
                                      Text(
                                        searchResult[index].title,
                                        style: TextStyle(
                                            fontSize: 10
                                        ),
                                      )
                                    ],
                                  )
                                ),
                            )
                        )
                      ),
                      Container(
                        color: Colors.white,
                        height: 200,
                        child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount: myResult.length,
                            itemBuilder: (BuildContext context, int index) => GestureDetector(
                              onTap: (){
                                setState(() {
                                  sourceController.text = myResult[index].title;
                                  isbn = myResult[index].isbn;
                                });
                              },
                              child: Container(
                                width: 80,
                                  child: Column(
                                    children: <Widget>[
                                      bookImage(myResult[index].image_url),
                                      Text(
                                        myResult[index].title,
                                        style: TextStyle(
                                          fontSize: 10
                                        ),)
                                    ],
                                  )
                              ),
                            )
                        )
                      )
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget titleBar() {
    return Container(
      color: Colors.white,
      margin: EdgeInsets.fromLTRB(0, MediaQuery.of(context).padding.top, 0, 0),
      height: 50,
      child: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              height: 1,
              color: Colors.black12,
            ),
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: GestureDetector(
              onTap: (){
                Navigator.of(context).pop();
              },
              child: Container(
                width: 18,
                height: 18,
                margin: EdgeInsets.only(left: 12),
                child: Image.asset('assets/icon_back.png'),
              ),
            ),
          ),
          Align(
              alignment: Alignment.center,
              child:
              Text('스크랩 등록')
          ),
          Align(
            alignment: Alignment.centerRight,
            child: Padding(
              padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  GestureDetector(
                    onTap: (){
                      if(!isSuccess) {
                        showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              // return object of type Dialog
                              return AlertDialog(
                                title: new Text("문자 분석 중"),
                                content: new Text("현재 서버에서 문자를 분석중에 있습니다. 분석이 완료되면 분석 결과를 볼 수 있습니다.\n\n그래도 스크랩을 등록 하시겠습니까?"),
                                actions: <Widget>[
                                  // usually buttons at the bottom of the dialog
                                  new FlatButton(
                                    child: new Text("취소"),
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                  ),
                                  new FlatButton(
                                    child: new Text("등록하기"),
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                      startUpload(isbn);
                                    },
                                  ),
                                ],
                              );
                            }
                        );
                      } else {
                        startUpload(isbn);
                      }
                    },
                    child: Container(
                      width: 50,
                      height: 50,
                      padding: EdgeInsets.symmetric(horizontal: 16),
                      color: Colors.transparent,
                      margin: EdgeInsets.only(left: 12),
                      child: Image.asset('assets/icon_done.png'),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget viewImage() {
    if(widget.imagePath == null) {
      return Image.network(scrap.imageURL);
    } else {
      return Image.file(File(widget.imagePath));
    }
  }
}
