import 'package:flutter/material.dart';

class BookImage {
  Widget bookImage(String url) {
    if(url != null) {
      return Image.network(url, height: 150);
    } else {
      return Container(
        height: 150,
        color: Colors.transparent,
        child: Align(
          alignment: Alignment.center,
          child: Text('이미지 없음'),
        ),
      );
    }
  }
}