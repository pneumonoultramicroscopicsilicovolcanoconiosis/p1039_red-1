import 'package:redproject/pack.dart';

class LoadingDialog extends StatelessWidget {

  String loading = '로딩 중...';
  BuildContext context;

  void dismiss() {
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {

    this.context = context;

    return Dialog(
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                width: 100,
                height: 100,
                child: Image.asset('assets/img_loading.png'),
              ),
              Text(loading, style: TextStyle(color: Colors.white),),
            ],
          )
        ],
      ),
    );
  }
}

