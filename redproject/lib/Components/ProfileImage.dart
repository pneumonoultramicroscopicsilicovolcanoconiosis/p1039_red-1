import 'package:flutter/material.dart';
import 'package:redproject/pack.dart';

/*class ProfileImage {
  ImageProvider profileImage(String url) {
    if(url == null) {
      return AssetImage('assets/icon_profile_norm.png');
    } else {
      return NetworkImage(url);
    }
  }
}*/

class ProfileImage extends StatefulWidget {

  double width = 10;
  double height = 10;
  String url;

  ProfileImage(this.url, {this.width, this.height});

  @override
  _ProfileImageState createState() => _ProfileImageState();
}

class _ProfileImageState extends State<ProfileImage> {

  Image image;

  @override
  Widget build(BuildContext context) {

    Log.d('ProfileImage', widget.url);

    if(widget.url == null) {
      image = Image.asset('assets/icon_profile_norm.png', fit: BoxFit.cover);
    } else {
      image = Image.network(widget.url, fit: BoxFit.cover);
    }

    return Container(
      width: widget.width,
      height: widget.height,
      child: ClipOval(
        child: image,
      ),
    );
  }
}
