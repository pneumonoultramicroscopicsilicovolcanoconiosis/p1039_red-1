import 'package:flutter/material.dart';

class CheckCircle extends StatelessWidget {

  double width = 10.0;
  double height = 10.0;
  bool isChecked;


  CheckCircle({
    this.width,
    this.height,
    this.isChecked
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: this.width,
      height: this.height,
      color: Colors.white,
      child: Stack(
        children: <Widget>[
          Visibility(
            visible: !isChecked,
            child: Image.asset('assets/icon_selected_grey.png'),
          ),
          Visibility(
            visible: isChecked,
            child: Image.asset('assets/icon_selected.png'),
          )
        ],
      ),
    );
  }
}
