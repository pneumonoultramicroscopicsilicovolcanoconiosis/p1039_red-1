import 'package:flutter/material.dart';
import 'package:redproject/pack.dart';
import 'package:redproject/DTO/ScrapFolder.dart';
import 'package:http/http.dart' as http;

class FolderScreen extends StatefulWidget {

  List<String> selectedList;

  FolderScreen(this.selectedList);

  @override
  _FolderScreenState createState() => _FolderScreenState();
}

class _FolderScreenState extends State<FolderScreen> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  List<ScrapFolder> scrapFolders = new List();


  @override
  void initState() {
    getScrapFolderList();
  }

  void getScrapFolderList() async {

    var url = 'https://api.takebook.org/Account/UserScrapFolder';

    TokenManager tokenManager = TokenManager();
    String token = await tokenManager.getToken();

    var response = await http.get(url, headers: {'Authorization' : '$token'});
    ResultManager resultManager = ResultManager(response);
    String resultCode = resultManager.getResultCode()[0];
    String resultMessage = resultManager.getResultCode()[1];
    String msg = '$resultCode :::: $resultMessage';

    Log.d('UserInfoScreen - getScrapFolderList', '${resultManager.getFullMessage()}');

    if(!resultManager.isSuccess()) {
      // 실패
      MessageSnack.show(_scaffoldKey, '폴더 리스트를 불러오는데 실패했습니다.');
      return;
    }

    Log.d('HomeScreen - getScrapFolderList', response.body);

    Map<String, dynamic> map = resultManager.getResult();
    var item = map['item'] as List;

    setState(() {
      scrapFolders = item.map<ScrapFolder>((json) => ScrapFolder.fromJson(json)).toList();
    });

  }

  Future moveFolder(String folderID) async {
    //String token = await TokenManager().getToken();

//    var response = await http.put(
//        'https://api.takebook.org/Account/ChangeScrapFolder',
//        headers: {'Authorization': '$token'},
//        body: {
//          'folder_id' : '$folderID',
//          'scrap_id_list' : widget.selectedList.toList()
//        }
//    );
//
//    Log.d('FolderScreen - moveFolder', '${widget.selectedList.toList()}');
//
//    ResultManager resultManager = ResultManager(response);
//    String resultCode = resultManager.getResultCode()[0];
//    String resultMessage = resultManager.getResultCode()[1];
//    String msg = '$resultCode :::: $resultMessage';
//
//    Log.d('FolderScreen - moveFolder', resultManager.getFullMessage(text: '폴더를 이동하는데 오류가 발생했습니다.'));
//
//    if(!resultManager.isSuccess()) {
//      // 실패
//      MessageSnack.show(_scaffoldKey, '폴더를 이동하는데 오류가 발생했습니다.\n$msg');
//      return;
//    }

    var url = 'https://api.takebook.org/Account/ChangeScrapFolder';

    TokenManager tokenManager = TokenManager();
    String token = await tokenManager.getToken();

    HttpClient httpClient = HttpClient();
    HttpClientRequest request = await httpClient.putUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    request.headers.add('Authorization', '$token');
    request.add(utf8.encode(json.encode({'folder_id' : '$folderID','scrap_id_list' : widget.selectedList.toList()})));

    Log.d('HomeScreen - removeFolder', 'scrap_id_list = ${widget.selectedList.toList()}');

    HttpClientResponse response = await request.close();

    if(response.statusCode != 200) {
      return;
    }
    httpClient.close();

    String result = await response.transform(utf8.decoder).join();
    Map<String, dynamic> map = json.decode(result);
    String resultCode = map['Result_Code'];
    String resultMessage = map['Message'];
    String msg = '$resultCode :::: $resultMessage';

    Log.d('HomeScreen - removeFolder', '$msg');

    if(ERROR_CODES.contains(resultCode)) {
      // 실패
      MessageSnack.show(_scaffoldKey, '폴더를 이동하는데 오류가 발생했습니다.');
      return;
    }

    Navigator.pop(context, '000');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: Container(
        child: Column(
          children: <Widget>[
            titleBar(),
            Expanded(
              child: Container(
                  child: ListView.builder(
                      scrollDirection: Axis.vertical,
                      itemCount: scrapFolders.length,
                      itemBuilder: (BuildContext context, int index) => GestureDetector(
                          onTap: (){
                            moveFolder(scrapFolders[index].folderID);
                          },
                          child: Stack(
                            children: <Widget>[
                              Container(
                                height: 160,
                                padding: EdgeInsets.all(8),
                                child: Container(
                                  child: Stack(
                                    children: <Widget>[
                                      Container(
                                        width: double.infinity,
                                        child: Image.asset('assets/img_folder.png', fit: BoxFit.fill, centerSlice: Rect.fromLTWH(145, 80, 10, 10),),
                                      ),
                                      Positioned(
                                        top: 40,
                                        left: 25,
                                        child: Text(scrapFolders[index].folderTitle, style: TextStyle(fontSize: 16)),
                                      ),
                                      Align(
                                        alignment: Alignment.bottomRight,
                                        child: Container(
                                          margin: EdgeInsets.only(right: 18, bottom: 18),
                                          child: Text('2019-01-01'),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          )
                      )
                  )
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget titleBar() {
    return Container(
      margin: EdgeInsets.fromLTRB(0, MediaQuery.of(context).padding.top, 0, 0),
      height: 40,
      child: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              height: 1,
              color: Colors.black12,
            ),
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: GestureDetector(
              onTap: (){
                Navigator.of(context).pop();
              },
              child: Container(
                width: 18,
                height: 18,
                margin: EdgeInsets.only(left: 12),
                child: Image.asset('assets/icon_close.png'),
              ),
            )
          ),
          Align(
            alignment: Alignment.centerRight,
            child: Padding(
              padding: EdgeInsets.fromLTRB(0, 8, 12, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[

                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
