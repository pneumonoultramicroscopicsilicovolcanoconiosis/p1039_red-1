import 'package:flutter/material.dart';

class MessageSnack {
  static void show(GlobalKey<ScaffoldState> scaffoldKey, String text) {
    final snackBar = SnackBar(content: Text(text));
    scaffoldKey.currentState.showSnackBar(snackBar);
  }
}