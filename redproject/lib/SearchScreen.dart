import 'package:flutter/material.dart';
import 'package:redproject/pack.dart';
import 'package:redproject/DTO/BookSearch.dart';
import 'package:redproject/DTO/BookCover.dart';
import 'package:redproject/BookInfoScreen.dart';
import 'package:http/http.dart' as http;

class SearchScreen extends StatefulWidget {

  String bookID;
  String searchText;

  SearchScreen({this.bookID, this.searchText});

  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final searchController = TextEditingController();

  List<ScrapSearch> searchResult = List();
  List<BookCover> myResult = List();

  @override
  void initState() {
    if(widget.bookID != null && widget.searchText != null) {
      setState(() {
        searchController.text = widget.searchText;
      });

      search();
    }
  }

  void search() async {
    String keyword = searchController.text;

    var url = 'https://api.takebook.org/Elasticsearch/Book?keyword=$keyword&category=title';

    TokenManager tokenManager = TokenManager();
    String token = await tokenManager.getToken();

    var response = await http.get(url);

    ResultManager resultManager = ResultManager(response);
    String resultCode = resultManager.getResultCode()[0];

    Log.d('SearchScreen - getBookList', response.body);
    Log.d('SearchScreen - getBookList', resultManager.getFullMessage());

    if (!resultManager.isSuccess()) {
      // 실패
      MessageSnack.show(_scaffoldKey, '검색 목록을 불러오는데 실패했습니다.');
      return;
    }

    Map<String, dynamic> map = resultManager.getResult();
    var item = map['item'] as List;

    setState(() {
      searchResult = item.map<ScrapSearch>((json) => ScrapSearch.fromJson(json)).toList();
    });

    getBookList();
  }

  void getBookList() async {
    String keyword = searchController.text;

    var url = 'https://api.takebook.org/Account/UserBook?keyword=$keyword&category=title';

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('userToken');

    var response = await http.get(url, headers: {'Authorization': '$token'});

    ResultManager resultManager = ResultManager(response);
    String resultCode = resultManager.getResultCode()[0];

    Log.d('SearchScreen - getBookList', response.body);
    Log.d('SearchScreen - getBookList', resultManager.getFullMessage());

    if(!resultManager.isSuccess()) {
      // 실패
      MessageSnack.show(_scaffoldKey, '책 목록을 불러오는데 실패했습니다.');
      return;
    }

    Map<String, dynamic> map = resultManager.getResult();
    var item = map['item'] as List;

    setState(() {
      myResult = item.map<BookCover>((json) => BookCover.fromJson(json)).toList();
    });

  }

  Future editBook(String isbn) async {
    String token = await TokenManager().getToken();

    var url = 'https://api.takebook.org/Account/UserBook';
    var map = {'book_id' : '${widget.bookID}', 'modify_isbn' : '$isbn'};

    HttpClient httpClient = HttpClient();
    HttpClientRequest request = await httpClient.putUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    request.headers.add('Authorization', '$token');
    request.add(utf8.encode(json.encode(map)));

    HttpClientResponse response = await request.close();
    String result = await response.transform(utf8.decoder).join();

    if(response.statusCode != 200) {
      return;
    }

    httpClient.close();

    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      body: Column(
        children: <Widget>[
          titleBar(),
          Container(
            color: Colors.white,
            padding: EdgeInsets.all(24),
            child: Stack(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(left: 8, right: 30),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30),
                      color: Color(BACKGROUND_COLOR)
                  ),
                  child: TextField(
                    controller: searchController,
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.all(10),
                      border: InputBorder.none,
                    ),
                    style: TextStyle(fontSize: 14),
                  ),
                ),
                Align(
                  alignment: Alignment.centerRight,
                  child: Container(
                    child: ClipOval(
                        child: GestureDetector(
                            onTap: (){
                              FocusScope.of(context).requestFocus(new FocusNode());
                              search();
                            },
                            child: Container(
                                width: 34,
                                height: 34,
                                decoration: BoxDecoration(
                                    color: Color(COLOR_YELLOW)
                                ),
                                child: Container(
                                  padding: EdgeInsets.all(8),
                                  child: Image.asset(
                                    'assets/icon_search_norm.png',
                                    width: 30,
                                    height: 30,
                                    color: Colors.white,
                                  ),
                                )
                            )
                        )
                    ),
                  ),
                )
              ],
            ),
          ),
          Expanded(
            child: SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Column(
                children: <Widget>[
                  Container(
                      color: Colors.white,
                      height: 200,
                      child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: searchResult.length,
                          itemBuilder: (BuildContext context, int index) => GestureDetector(
                            onTap: (){
                              if(widget.bookID != null && widget.searchText != null) {
                                editBook(searchResult[index].isbn);
                              } else {
                                setState(() {
                                  Navigator.push(context, MaterialPageRoute(
                                      builder: (context) =>
                                          BookinfoScreen(
                                              searchResult[index].isbn, null)));
                                });
                              }
                            },
                            child: Container(
                                width: 80,
                                child: Column(
                                  children: <Widget>[
                                    bookImage(searchResult[index].imageURL),
                                    Text(
                                      searchResult[index].title,
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                          fontSize: 10
                                      ),
                                    )
                                  ],
                                )
                            ),
                          )
                      )
                  ),
                  Container(
                      color: Colors.white,
                      height: 200,
                      child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: myResult.length,
                          itemBuilder: (BuildContext context, int index) => GestureDetector(
                            onTap: () async {
                              if(widget.bookID != null && widget.searchText != null) {
                                editBook(myResult[index].isbn);
                              } else {
                                setState(() {
                                  Navigator.push(context, MaterialPageRoute(
                                      builder: (context) => BookinfoScreen(myResult[index].isbn, null)));
                                });
                              }
                            },
                            child: Container(
                                width: 80,
                                height: 120,
                                child: Column(
                                  children: <Widget>[
                                    bookImage(myResult[index].image_url),
                                    Text(
                                      myResult[index].title,
                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 1,
                                      style: TextStyle(
                                          fontSize: 10,
                                      ),)
                                  ],
                                )
                            ),
                          )
                      )
                  ),
                ],
              ),
            ),
          ),

        ],
      ),
    );
  }

  Widget titleBar() {
    return Container(
      color: Colors.white,
      margin: EdgeInsets.fromLTRB(0, MediaQuery
          .of(context)
          .padding
          .top, 0, 0),
      height: 50,
      child: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              height: 1,
              color: Colors.black12,
            ),
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: GestureDetector(
              onTap: () {
                Navigator.of(context).pop();
              },
              child: Container(
                width: 18,
                height: 18,
                color: Colors.white,
                margin: EdgeInsets.only(left: 12),
                child: Image.asset('assets/icon_back.png'),
              ),
            ),
          ),
          Align(
              alignment: Alignment.center,
              child:
              Text('검색')
          ),
        ],
      ),
    );
  }

  Widget bookImage(String imageURL) {
    double w = 70;
    double h = 110;

    if(imageURL == null) {
      return Container(
        color: Color(BACKGROUND_COLOR),
        width: w,
        height: h,
      );
    } else {
      return Image.network(imageURL, width: w, height: h);
    }
  }
}
