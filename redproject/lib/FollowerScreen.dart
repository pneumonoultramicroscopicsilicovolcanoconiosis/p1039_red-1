import 'package:flutter/material.dart';
import 'package:redproject/pack.dart';
import 'package:redproject/DTO/BookCover.dart';
import 'package:redproject/BookInfoScreen.dart';
import 'package:redproject/DTO/User.dart';
import 'package:http/http.dart' as http;

class FollowerScreen extends StatefulWidget {

  String userID;

  FollowerScreen(this.userID);

  @override
  _FollowerScreenState createState() => _FollowerScreenState();
}

class _FollowerScreenState extends State<FollowerScreen> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  List<BookCover> books = List();
  User user;

  @override
  void initState() {
    loadBookList();
    loadUserInfo(widget.userID);
  }

  void loadBookList() async {
    var url = 'https://api.takebook.org/Account/UserBook?user_id=${widget.userID}';

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('userToken');

    var response = await http.get(url, headers: {'Authorization': '$token'});

    ResultManager resultManager = ResultManager(response);
    Log.d('FollowerScreen - loadBookList', response.body);
    Log.d('FollowerScreen - loadBookList', resultManager.getFullMessage());

    if(!resultManager.isSuccess()) {
      // 실패
      MessageSnack.show(_scaffoldKey, '책 목록을 불러오는데 실패했습니다.');
      return;
    }

    Map<String, dynamic> map = resultManager.getResult();
    var item = map['item'] as List;

    setState(() {
      books = item.map<BookCover>((json) => BookCover.fromJson(json)).toList();
    });

  }

  Future loadUserInfo(String userID) async {
    String token = await TokenManager().getToken();

    var url = Uri.https('api.takebook.org', 'Account/OtherUserInfo', {'user_id' : '$userID'});
    var response = await http.get(url, headers: {'Authorization' : '$token'});

    ResultManager resultManager = ResultManager(response);
    String resultCode = resultManager.getResultCode()[0];
    String resultMessage = resultManager.getResultCode()[1];

    Log.d('FollwerScreen - loadUserInfo', response.body);
    Log.d('FollwerScreen - loadUserInfo', resultManager.getFullMessage());

    if(!resultManager.isSuccess()) {
      // 실패
      MessageSnack.show(_scaffoldKey, '유저 정보를 불러오는데 오류가 발생했습니다.');
      return;
    }

    Map<String, dynamic> map = resultManager.getResult();

    setState(() {
      user = User.fromJson(map);
    });
  }

  Future doFollow(String ID) async {
    String token = await TokenManager().getToken();

    var response = await http.post(
        'https://api.takebook.org/Account/FollowUser',
        headers: {'Authorization': '$token'},
        body: {
          'followee_id' : '$ID',
        }
    );

    Log.d('UserInfoScreen - doFollow', 'followee_id = $ID');
    Log.d('UserInfoScreen - doFollow', '$token');

    ResultManager resultManager = ResultManager(response);
    String resultCode = resultManager.getResultCode()[0];
    String resultMessage = resultManager.getResultCode()[1];
    String msg = '$resultCode :: $resultMessage';

    Log.d('UserInfoScreen - doFollow', '${resultManager.getFullMessage()}');

    if(!resultManager.isSuccess()) {
      // 실패
      MessageSnack.show(_scaffoldKey, '구독하는데 오류가 발생했습니다.');
      return;
    }

    loadUserInfo(widget.userID);
  }

  Future cancelFollow(String ID) async {
    String token = await TokenManager().getToken();
    var url = 'https://api.takebook.org/Account/FollowUser';

    Log.d('UserInfoScreen - cancelFollow', 'followee_id = $ID');
    Log.d('UserInfoScreen - cancelFollow', '$token');

    HttpClient httpClient = HttpClient();
    HttpClientRequest request = await httpClient.deleteUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    request.headers.add('Authorization', '$token');
    request.add(utf8.encode(json.encode({'followee_id' : '$ID'})));

    HttpClientResponse response = await request.close();

    int statusCode = int.parse(response.statusCode.toString());

    if(statusCode != 200) {
      return;
    }

    String result = await response.transform(utf8.decoder).join();
    Map<String, dynamic> map = json.decode(result);
    String resultCode = map['Result_Code'];
    String resultMessage = map['Message'];
    String msg = '$resultCode :::: $resultMessage';

    Log.d('UserInfoScreen - cancelFollow', '$msg');

    if(ERROR_CODES.contains(resultCode)) {
      // 실패

      MessageSnack.show(_scaffoldKey, '구독을 취소하는데 오류가 발생했습니다.');
      return;
    }

    loadUserInfo(widget.userID);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: Column(
        children: <Widget>[
          titleBar(),
          viewProfile(),
          Expanded(
            child: viewBooks(),
          ),
        ],
      ),
    );
  }

  Widget titleBar() {
    return Container(
      margin: EdgeInsets.fromLTRB(0, MediaQuery.of(context).padding.top, 0, 0),
      height: 50,
      child: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment.centerLeft,
            child: GestureDetector(
              onTap: (){
                Navigator.of(context).pop();
              },
              child: Container(
                width: 18,
                height: 18,
                margin: EdgeInsets.only(left: 12),
                child: Image.asset('assets/icon_back.png'),
              ),
            ),
          ),
          Align(
              alignment: Alignment.center,
              child:
              Text('책장')
          ),
          Align(
            alignment: Alignment.centerRight,
            child: Padding(
              padding: EdgeInsets.fromLTRB(0, 8, 12, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[

                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget viewProfile() {
    return Container(
      color: Color(COLOR_YELLOW),
      height: 130,
      padding: EdgeInsets.only(left: 12),
      child: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment.centerLeft,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text('${user.name}님의 책장', style: TextStyle(fontSize: 28)),
                Text('구독자 ${user.followeeCount} | 구독중 ${user.followerCount}', style: TextStyle(fontSize: 14)),
                Text('${user.stateMessage}', style: TextStyle(fontSize: 14)),
              ],
            ),
          ),
          Align(
            alignment: Alignment.centerRight,
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 8),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  ProfileImage(
                    user.profileURL,
                    width: 80,
                    height: 80,
                  ),
                  Container(
                    width: 1,
                    height: 8,
                  ),
                  followButton(user.isFollow)
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget followButton(int follow) {
    if(follow == 0) {
      return GestureDetector(
        onTap: (){
          doFollow(user.userID);
        },
        child: Container(
            width: 90,
            height: 25,
            decoration: BoxDecoration(
              color: Color(COLOR_YELLOW),
              borderRadius: BorderRadius.circular(8),
              border: Border.all(color: Color(0xFFcf8b20))
            ),
            child: Align(
              alignment: Alignment.center,
              child: Text('구독', style: TextStyle(fontSize: 12),),
            )
        ),
      );
    } else {
      return GestureDetector(
        onTap: (){
          cancelFollow(user.userID);
        },
        child: Container(
            width: 90,
            height: 25,
            decoration: BoxDecoration(
                color: Color(0xFFcf8b20),
                borderRadius: BorderRadius.circular(8),
                border: Border.all(
                    color: Color(0xFFcf8b20),
                    width: 1
                )
            ),
            child: Align(
              alignment: Alignment.center,
              child: Text('구독 취소', style: TextStyle(fontSize: 12, color: Colors.white),),
            )
        ),
      );
    }
  }

  Widget viewBooks() {
    var size = MediaQuery.of(context).size;

    /*24 is for notification bar on Android*/
    final double itemWidth = size.width / 3;
    final double itemHeight = (itemWidth / 3) * 7;

    print("$itemWidth / $itemHeight");

    if(books.length == 0 || books == null) {
      return Container(
        child: Text('책 없음'),
      );
    }

    return Stack(
      children: <Widget>[
        GridView.builder(
            itemCount: books.length,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 3, childAspectRatio: (itemWidth / itemHeight)),
            itemBuilder: (BuildContext context, int index) => GestureDetector(
              onTap: (){
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => BookinfoScreen(books[index].isbn, books[index].book_id)));
              },
              child: Stack(
                children: <Widget>[
                  Container(
                      child: Column(
                        children: <Widget>[
                          Container(
                            alignment: Alignment.center,
                            child: Column(
                              children: <Widget>[
                                Container(
                                  child: Align(
                                    alignment: Alignment.centerLeft,
                                    child: Text(
                                      books[index].registration_date.split(" ")[0],
                                      style: TextStyle(
                                        fontSize: 10,
                                      ),
                                    ),
                                  ),
                                  padding: EdgeInsets.fromLTRB(14, 0, 0, 0),
                                ),
                                Image.network(
                                  books[index].image_url,
                                  fit: BoxFit.fitHeight,
                                  height: 150,
                                ),
                              ],
                            ),
                          ),
                          Container(
                              alignment: Alignment.bottomCenter,
                              padding: EdgeInsets.fromLTRB(0, 14, 0, 0),
                              child: Stack(
                                children: <Widget>[
                                  Container(
                                    height: 50,
                                    decoration: BoxDecoration(
                                        gradient: LinearGradient(
                                            begin: Alignment.topCenter,
                                            end: Alignment.bottomCenter,
                                            colors: [
                                              Colors.black12,
                                              Colors.transparent
                                            ])),
                                  ),
                                  Column(children: <Widget>[
                                    Container(
                                      padding: EdgeInsets.fromLTRB(14, 12, 14, 0),
                                      child: Align(
                                        alignment: Alignment.centerLeft,
                                        child: Text(
                                          books[index].title,
                                          style: TextStyle(fontSize: 12),
                                          maxLines: 2,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      padding: EdgeInsets.fromLTRB(14, 0, 14, 0),
                                      child: Align(
                                        alignment: Alignment.centerLeft,
                                        child: Text(
                                          books[index].author,
                                          style: TextStyle(fontSize: 12, color: Colors.black45),
                                          maxLines: 1,
                                        ),
                                      ),
                                    ),
                                  ],)
                                ],
                              )),
                        ],
                      )
                  ),
                ],
              ),
            )
        ),
      ],
    );
  }
}
