import 'package:redproject/pack.dart';
import 'package:http/http.dart' as http;
import 'dart:developer' as developer;

class UserManager {
  static UserManager _manager = UserManager._internal();

  String _userID;
  String _name;
  String _signUpDate;
  String _profileURL;
  String _updateDate;
  int _accessState;
  String _stateMessage;

  static UserManager get manager => _manager;

  factory UserManager() {
    return _manager;
  }

  UserManager._internal();

  reloadData() {
    _getUserInfo();
  }

  removeUser() {
    _manager = null;
  }

  _getUserInfo() async {
    var url = 'https://api.takebook.org/Account/UserInfo';

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('userToken');

    var response = await http.get(url, headers: {'Authorization': '$token'});

    print('Home Response status: ${response.statusCode}');
    print('Home Response body: ${response.body}');

    Log.d('UserManager', response.body);

    ResultManager resultManager = ResultManager(response);
    if(!resultManager.isSuccess()) {
      // 실패
      String resultCode = resultManager.getResultCode()[0];
      developer.log(resultManager.getResultCode()[1], name: 'TakeBook - ERROR');
      return;
    }

    Map<String, dynamic> map = resultManager.getResult()['user_info'];

    _userID = map['user_id'];
    _name = map['name'];
    _signUpDate = map['signup_date'];
    _profileURL = map['profile_url'];
    _updateDate = map['update_date'];
    _accessState = map['access_state'];
    _stateMessage = map['state_message'];

    if(_stateMessage == null) {
      _stateMessage = '';
    }
  }

  String get userID => _userID;

  String get name => _name;

  String get signUpDate => _signUpDate;

  String get profileURL => _profileURL;

  String get updateDate => _updateDate;

  int get accessState => _accessState;

  String get stateMessage => _stateMessage;


}