import 'package:flutter/material.dart';
import 'package:redproject/pack.dart';
import 'package:redproject/DTO/Scrap.dart';
import 'package:redproject/ScrapPreviewScreen.dart';
import 'package:http/http.dart' as http;

class ScrapViewScreen extends StatefulWidget {

  Scrap scrap;
  String folderName;

  ScrapViewScreen(this.scrap, this.folderName);

  @override
  _ScrapViewScreenState createState() => _ScrapViewScreenState();
}

class _ScrapViewScreenState extends State<ScrapViewScreen> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  Scrap scrap;

  @override
  void initState() {
    scrap = widget.scrap;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: Column(
        children: <Widget>[
          titleBar(),
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Container(
                    child: Image.network(scrap.imageURL),
                  ),
                  Container(
                      padding: EdgeInsets.all(24),
                      child: Align(
                        alignment: Alignment.topLeft,
                        child: Text(scrap.contents),
                      )
                  ),
                  Container(
                      padding: EdgeInsets.only(right: 24),
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: Text(scrap.creationDate, style: TextStyle(fontSize: 12)),
                      )
                  ),
                  Container(
                      padding: EdgeInsets.only(right: 24, bottom: 24),
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: Text('출처 - ${scrap.source}', style: TextStyle(fontSize: 12)),
                      )
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget titleBar() {
    return Container(
      color: Colors.white,
      margin: EdgeInsets.fromLTRB(0, MediaQuery
          .of(context)
          .padding
          .top, 0, 0),
      height: 50,
      child: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              height: 1,
              color: Colors.black12,
            ),
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: GestureDetector(
              onTap: () {
                Navigator.of(context).pop();
              },
              child: Container(
                width: 18,
                height: 18,
                margin: EdgeInsets.only(left: 12),
                child: Image.asset('assets/icon_back.png'),
              ),
            ),
          ),
          Align(
            alignment: Alignment.center,
            child:
              Text('스크랩 상세 보기'),
          ),
          Align(
            alignment: Alignment.centerRight,
            child: Padding(
              padding: EdgeInsets.fromLTRB(0, 0, 12, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  GestureDetector(
                    onTap: () async {
                      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => ScrapPreviewScreen(imagePath: null, scrap: scrap)));
                      },
                    child: Container(
                      width: 22,
                      height: 22,
                      color: Colors.white,
                      margin: EdgeInsets.only(left: 12),
                      child: Image.asset('assets/icon_edit.png'),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
