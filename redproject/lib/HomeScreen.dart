import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:redproject/Constant.dart';
import 'package:redproject/DTO/BookCover.dart';
import 'package:redproject/DTO/ScrapFolder.dart';
import 'package:redproject/DTO/BookCandidate.dart';
import 'package:redproject/Components/CheckCircle.dart';
import 'package:redproject/Components/BookImage.dart';
import 'package:redproject/CameraHomeScreen.dart';
import 'package:redproject/ScrapListScreen.dart';
import 'package:redproject/BookinfoScreen.dart';
import 'package:redproject/LoginScreen.dart';
import 'package:redproject/InsertingScreen.dart';
import 'package:redproject/ScrapPreviewScreen.dart';
import 'package:redproject/UserInfoScreen.dart';
import 'package:redproject/SearchScreen.dart';
import 'package:redproject/SettingScreen.dart';
import 'package:simple_permissions/simple_permissions.dart';
import 'package:redproject/pack.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:http/http.dart' as http;
import 'dart:developer' as developer;
import 'dart:convert';
import 'dart:io';

class HomeScreen extends StatelessWidget {

  HomeScreen();

  @override
  Widget build(BuildContext context) {
    return MyHomePage();
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with SingleTickerProviderStateMixin, WidgetsBindingObserver {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  final folderController = TextEditingController();

  TabController _tabController;
  ScrollController _scrollController = ScrollController();
  RefreshController _refreshController;
  RefreshController _refreshScrapFolderController;
  VoidCallback onChanged;
  UserManager userManager;
  LoadingDialog loadingDialog = LoadingDialog();

  double topPosition;

  bool _allowWriteFile = false;
  bool hasList = false;
  bool isSelectMode = false;
  bool isAddFolder = false;
  bool isScrapPage = false;
  bool isFolderEdit = false;
  bool isBookDialogVisible = false;
  bool isScrapDialogVisible = false;
  bool isCandidate = false;

  int pageNumber = 0;
  List<String> leftTab = ['assets/img_tab_left_enabled.png', 'assets/img_tab_left_disabled.png'];
  List<String> rightTab = ['assets/img_tab_right_disabled.png', 'assets/img_tab_right_enabled.png'];
  List<String> selectedBooks = List();
  List<String> selectedFolder = List();
  List<int> selectedIndex = List();
  List<bool> selectedSortBook = [false, false, false, true];
  List<bool> selectedSortScrap = [true, false, false];
  List<bool> selectedOrderBook = [false, true];
  List<bool> selectedOrderScrap = [false, true];

  List<BookCover> bookList = new List();
  List<ScrapFolder> scrapFolders = new List();
  List<BookCandidate> bookCandidates = new List();
  List<int> sortBookIndexes = [0, 0];
  List<int> sortScrapIndexes = [0, 0];
  BookCandidate modifyBook;
  int insertingCount = 0;
  String modifyBookID;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    topPosition = -80;
    _scrollController.addListener(_onScroll);
    _refreshController = RefreshController(initialRefresh: false);
    _refreshScrapFolderController = RefreshController(initialRefresh: false);
    requestWritePermission();
    getUserInfo();
    loadBookList();
    getInsertingList();
    loadScrapFolderList();
    _tabController = new TabController(length: 2, vsync: this);
    _tabController.animation.addListener(_tabAnimation);
    onChanged = () {

      setState(() {
        pageNumber = this._tabController.index;
      });
    };
    _tabController.addListener(onChanged);
  }

  _tabAnimation() {

    double value = _tabController.animation.value;

    setState(() {
      if(value < 0.5) {
        pageNumber = 0;
      } else {
        pageNumber = 1;
      }
    });
  }

  double _getOpacity() {
    double op = (topPosition + 80) / 80;
    return op > 1 || op < 0 ? 1 : op;
  }

  _onScroll() {
    if(_scrollController.offset > 50) {
      if(topPosition < 0) {
        setState(() {
          topPosition = -130 + _scrollController.offset;
          if(_scrollController.offset > 130) topPosition = 0;
        });
      }
    } else {
      if(topPosition > -80) {
        setState(() {
          topPosition--;
          if(_scrollController.offset <= 0) topPosition = -80;
        });
      }
    }
  }

  _onHorizontalScroll() {
    double off = _tabController.offset;
    developer.log('$off', name: 'TabController');
  }

  Color editColor() {
    if(selectedFolder.length == 1) {
      return Colors.black;
    } else {
      return Colors.black12;
    }
  }

  void sortBookType(int index) {
    setState(() {
      selectedSortBook[0] = false;
      selectedSortBook[1] = false;
      selectedSortBook[2] = false;
      selectedSortBook[3] = false;
      selectedSortBook[index] = true;
    });

    sortBookIndexes[0] = index;
  }

  void sortScrapType(int index) {
    setState(() {
      selectedSortScrap[0] = false;
      selectedSortScrap[1] = false;
      selectedSortScrap[2] = false;
      selectedSortScrap[3] = false;
      selectedSortScrap[index] = true;
    });

    sortScrapIndexes[0] = index;
  }

  void orderBook(int index) {
    setState(() {
      selectedOrderBook[0] = false;
      selectedOrderBook[1] = false;
      selectedOrderBook[index] = true;
    });

    sortBookIndexes[1] = index;
  }

  void orderScrap(int index) {
    setState(() {
      selectedOrderScrap[0] = false;
      selectedOrderScrap[1] = false;
      selectedOrderScrap[index] = true;
    });

    sortScrapIndexes[1] = index;
  }

  Widget _simplePopup() {

    int _btnVal;

    if(pageNumber == 0) {
      setState(() {
        isScrapPage = false;
      });
      return PopupMenuButton<int>(
        itemBuilder: (context) =>
        [
          PopupMenuItem(
            value: 1,
            child: Text('선택'),
          ),
          PopupMenuItem(
            value: 3,
            child: Text('로그아웃'),
          ),
          PopupMenuItem(
            value: 4,
            child: Text('설정'),
          ),
        ],
        onSelected: (int newValue) async {
          _btnVal = newValue;

          switch (_btnVal) {
            case 1:
              selectMode();
              break;

            case 2:
              showDialog(context: context, builder: (context) => loadingDialog);
              break;

            case 3:
              TokenManager().removeToken();
              Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (BuildContext context) => LoginScreen()));
              break;

            case 4:
              await Navigator.push(context, MaterialPageRoute(builder: (context) => SettingScreen()));
              loadBookList();
              break;
          }
        },
        child: Container(
          width: 20,
          height: 20,
          margin: EdgeInsets.fromLTRB(4, 0, 4, 0),
          child: Image.asset(
            'assets/icon_more_norm.png',
            width: 20,
            height: 20,
          ),
        ),
      );
    } else {
      setState(() {
        isScrapPage = true;
      });
      return PopupMenuButton<int>(
        itemBuilder: (context) =>
        [
          PopupMenuItem(
            value: 1,
            child: Text('선택'),
          ),
          PopupMenuItem(
            value: 2,
            child: Text('폴더 추가'),
          ),
          PopupMenuItem(
            value: 3,
            child: Text('로그아웃'),
          ),
          PopupMenuItem(
            value: 4,
            child: Text('설정'),
          ),
        ],
        onSelected: (int newValue) {
          _btnVal = newValue;

          switch (_btnVal) {
            case 1:
              selectMode();
              break;

            case 2:
              setState(() {
                isAddFolder = true;
                folderController.text = '';
              });
              break;

            case 3:
              TokenManager().removeToken();
              Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (BuildContext context) => LoginScreen()));
              break;

            case 4:
              Navigator.push(context, MaterialPageRoute(builder: (context) => SettingScreen()));
              break;
          }
        },
        child: Container(
          width: 20,
          height: 20,
          margin: EdgeInsets.fromLTRB(4, 0, 4, 0),
          child: Image.asset(
            'assets/icon_more_norm.png',
            width: 20,
            height: 20,
          ),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {

    return DefaultTabController(
      length: 2,
      child: Scaffold(
          key: _scaffoldKey,
          body: Stack(
            children: <Widget>[
              home(),
              Visibility(
                visible: isBookDialogVisible,
                child: sortBookDialog(),
              ),
              Visibility(
                visible: isCandidate,
                child: candidateBookWidget(),
              )
            ],
          ),
      ),
    );
  }

  Widget sortBookDialog() {
    return Stack(
        alignment: Alignment.center,
        children: <Widget>[
          GestureDetector(
            onTap: (){
              setState(() {
                isBookDialogVisible = false;
              });
            },
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              color: Colors.black54,
            ),
          ),
          Container(
            width: 400.0,
            height: 300.0,
            margin: EdgeInsets.all(24),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(30)
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(8),
                  child: Text('스크랩 정렬 방식', style: TextStyle(fontWeight: FontWeight.bold)),
                ),
                Padding(
                    padding: EdgeInsets.symmetric(horizontal: 24, vertical: 6),
                    child: GestureDetector(
                      onTap: (){
                        sortBookType(0);
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Text('폴더 이름'),
                          CheckCircle(
                            width: 20,
                            height: 20,
                            isChecked: selectedSortBook[0],
                          ),
                        ],
                      ),
                    )
                ),
                Padding(
                    padding: EdgeInsets.symmetric(horizontal: 24, vertical: 6),
                    child: GestureDetector(
                      onTap: (){
                        sortBookType(1);
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Text('생성일'),
                          CheckCircle(
                            width: 20,
                            height: 20,
                            isChecked: selectedSortBook[1],
                          ),
                        ],
                      ),
                    )
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 24, vertical: 6),
                  child: GestureDetector(
                    onTap: (){
                      sortBookType(2);
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text('수정일'),
                        CheckCircle(
                          width: 20,
                          height: 20,
                          isChecked: selectedSortBook[2],
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  height: 1,
                  margin: EdgeInsets.symmetric(horizontal: 12, vertical: 6),
                  color: Colors.black12,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 24, vertical: 6),
                  child: GestureDetector(
                    onTap: (){
                      orderScrap(0);
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text('오름차순'),
                        CheckCircle(
                          width: 20,
                          height: 20,
                          isChecked: selectedOrderScrap[0],
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 24, vertical: 6),
                  child: GestureDetector(
                    onTap: (){
                      orderScrap(1);
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text('내림차순'),
                        CheckCircle(
                          width: 20,
                          height: 20,
                          isChecked: selectedOrderScrap[1],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      );
  }

  Widget sortScrapDialog() {
    return Stack(
      alignment: Alignment.center,
      children: <Widget>[
        GestureDetector(
          onTap: (){
            setState(() {
              isBookDialogVisible = false;
            });
          },
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            color: Colors.black54,
          ),
        ),
        Container(
          width: 400.0,
          height: 300.0,
          margin: EdgeInsets.all(24),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(30)
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(8),
                child: Text('도서 정렬 방식', style: TextStyle(fontWeight: FontWeight.bold)),
              ),
              Padding(
                  padding: EdgeInsets.symmetric(horizontal: 24, vertical: 6),
                  child: GestureDetector(
                    onTap: (){
                      sortBookType(0);
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text('제목'),
                        CheckCircle(
                          width: 20,
                          height: 20,
                          isChecked: selectedSortBook[0],
                        ),
                      ],
                    ),
                  )
              ),
              Padding(
                  padding: EdgeInsets.symmetric(horizontal: 24, vertical: 6),
                  child: GestureDetector(
                    onTap: (){
                      sortBookType(1);
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text('지은이/역자'),
                        CheckCircle(
                          width: 20,
                          height: 20,
                          isChecked: selectedSortBook[1],
                        ),
                      ],
                    ),
                  )
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 24, vertical: 6),
                child: GestureDetector(
                  onTap: (){
                    sortBookType(2);
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text('출판사'),
                      CheckCircle(
                        width: 20,
                        height: 20,
                        isChecked: selectedSortBook[2],
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 24, vertical: 6),
                child: GestureDetector(
                  onTap: (){
                    sortBookType(3);
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text('등록 날짜'),
                      CheckCircle(
                        width: 20,
                        height: 20,
                        isChecked: selectedSortBook[3],
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                height: 1,
                margin: EdgeInsets.symmetric(horizontal: 12, vertical: 6),
                color: Colors.black12,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 24, vertical: 6),
                child: GestureDetector(
                  onTap: (){
                    orderBook(0);
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text('오름차순'),
                      CheckCircle(
                        width: 20,
                        height: 20,
                        isChecked: selectedOrderBook[0],
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 24, vertical: 6),
                child: GestureDetector(
                  onTap: (){
                    orderBook(1);
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text('내림차순'),
                      CheckCircle(
                        width: 20,
                        height: 20,
                        isChecked: selectedOrderBook[1],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget home() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Container(
          color: Colors.white,
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).padding.top + 100,
          padding: EdgeInsets.fromLTRB(
              0, MediaQuery.of(context).padding.top, 0, 0),
          child: Column(
            children: <Widget>[
              Flexible(
                flex: 1,
                child: Stack(
                  children: <Widget>[
                    Container(
                      color: Colors.white,
                      child: Stack(
                        children: <Widget>[
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Padding(
                              padding: EdgeInsets.fromLTRB(12, 8, 0, 0),
                              child: Image.asset('assets/icon_title.png', width: 60),
                            ),
                          ),
                          Align(
                            alignment: Alignment.centerRight,
                            child: Padding(
                              padding: EdgeInsets.fromLTRB(0, 8, 12, 0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[
                                  Container(
                                    width: 20,
                                    height: 20,
                                    color: Colors.white,
                                    margin: EdgeInsets.fromLTRB(4, 0, 4, 0),
                                    child: GestureDetector(
                                      onTap: (){
                                        Navigator.push(context, MaterialPageRoute(builder: (context) => SearchScreen()));
                                      },
                                      child: Image.asset(
                                        'assets/icon_search_norm.png',
                                        width: 20,
                                        height: 20,
                                      ),
                                    ),
                                  ),
                                  GestureDetector(
                                    onTap: () async {
                                      await Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => UserInfoScreen()));
                                      print('UserInfoScreen -> HomeScreen');
                                      getUserInfo();
                                    },
                                    child: Container(
                                      width: 24,
                                      height: 24,
                                      margin: EdgeInsets.fromLTRB(4, 0, 4, 0),
                                      child: GestureDetector(
                                        child: Container(
                                          width: 20,
                                          height: 20,
                                          child: CircleAvatar(
                                            minRadius: 30,
                                            backgroundImage: profileImage(userManager.profileURL),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  _simplePopup()
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    Visibility(
                      visible: isSelectMode,
                      child: Container(
                        color: Colors.white,
                        child: Stack(
                          children: <Widget>[
                            Align(
                                alignment: Alignment.centerLeft,
                                child: GestureDetector(
                                  onTap: (){
                                    selectMode();
                                  },
                                  child: Container(
                                    width: 18,
                                    height: 18,
                                    margin: EdgeInsets.only(left: 12),
                                    child: Image.asset('assets/icon_close.png'),
                                  ),
                                )
                            ),
                            Align(
                              alignment: Alignment.centerRight,
                              child: Container(
                                margin: EdgeInsets.only(right: 12),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Visibility(
                                      visible: false,
                                      child: Row(
                                        children: <Widget>[
                                          Container(
                                            width: 16,
                                            height: 16,
                                            margin: EdgeInsets.only(right: 8),
                                            child: Image.asset('assets/icon_edit.png'),
                                          ),
                                          Text('이동', style: TextStyle(fontSize: 14)),
                                        ],
                                      ),
                                    ),
                                    Container(
                                      width: 12,
                                    ),
                                    Visibility(
                                      visible: isScrapPage,
                                      child: GestureDetector(
                                        onTap: (){
                                          setState(() {
                                            isAddFolder = true;
                                            isFolderEdit = true;
                                            folderController.text = scrapFolders[selectedIndex[0]].folderTitle;
                                          });
                                        },
                                        child: Row(
                                          children: <Widget>[
                                            Container(
                                              width: 16,
                                              height: 16,
                                              margin: EdgeInsets.only(right: 4),
                                              child: Image.asset('assets/icon_edit.png'),
                                            ),
                                            Text('수정', style: TextStyle(fontSize: 14, color: editColor())),
                                          ],
                                        ),
                                      ),
                                    ),
                                    Container(
                                      width: 12,
                                    ),
                                    GestureDetector(
                                      onTap: (){
                                        if(pageNumber == 0) removeBook();
                                        else removeFolder();
                                      },
                                      child: Row(
                                        children: <Widget>[
                                          Container(
                                            width: 16,
                                            height: 16,
                                            margin: EdgeInsets.only(right: 8),
                                            child: Image.asset('assets/icon_trash.png'),
                                          ),
                                          Text('삭제', style: TextStyle(fontSize: 14)),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Flexible(
                flex: 1,
                child: Container(
                    width: MediaQuery.of(context).size.width,
                    child: Container(
                        child: Row(
                          children: <Widget>[
                            Flexible(
                              flex: 1,
                              child: GestureDetector(
                                onTap: (){
                                  _tabController.animateTo(0);
                                },
                                child: Container(
                                    width: MediaQuery.of(context).size.width / 2,
                                    child: Stack(
                                      children: <Widget>[
                                        Image.asset(
                                          leftTab[pageNumber % 2],
                                          fit: BoxFit.fill,
                                        ),
                                        Align(
                                          alignment: Alignment.center,
                                          child: Container(
                                            padding: EdgeInsets.only(bottom: 12),
                                            child: Text('도서'),
                                          ),
                                        ),
                                      ],
                                    )
                                ),
                              )
                            ),
                            Flexible(
                                flex: 1,
                                child: GestureDetector(
                                  onTap: (){
                                    _tabController.animateTo(1);
                                  },
                                  child: Stack(
                                    children: <Widget>[
                                      Image.asset(
                                        rightTab[pageNumber % 2],
                                        fit: BoxFit.fill,
                                      ),
                                      Align(
                                        alignment: Alignment.center,
                                        child: Container(
                                          padding: EdgeInsets.only(bottom: 12),
                                          child: Text('스크랩'),
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                            )
                          ],
                        )
                    )
                ),
              )
            ],
          ),
        ),
        Expanded(
            child: Container(
              height: MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top - 80,
              child: Stack(
                children: <Widget>[
                  TabBarView(
                    controller: _tabController,
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          GestureDetector(
                            child: InsertingBar(insertingCount),
                            onTap: () {
                              Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => InsertingScreen()));
                            },
                          ),
                          Expanded(
                            child: firstPage(),
                          ),
                        ],
                      ),
                      Stack(
                        children: <Widget>[
                          secondPage(),
                          Visibility(
                            visible: isAddFolder,
                            child: dialogFolder(),
                          ),
                        ],
                      ),
                    ],
                  ),
                  Container(
                      child: Align(
                          alignment: Alignment.bottomCenter,
                          child: Container(
                            margin: EdgeInsets.all(24),
                            child: ClipOval(
                                child: GestureDetector(
                                    onTap: () async {
                                      if(Platform.isIOS) {
                                        // iOS
                                        final result = await Navigator.push(context, MaterialPageRoute(builder: (context) => CameraHomeScreen(pageNumber)));

                                        Log.d('HomeScreen', 'result = $result');
                                      } else {
                                        // Android
                                        _startCamera();
                                      }
                                    },
                                    child: Container(
                                        width: 70,
                                        height: 70,
                                        decoration: BoxDecoration(
                                            gradient: LinearGradient(
                                                begin: Alignment.topLeft,
                                                end: Alignment.bottomRight,
                                                colors: [Color(COLOR_YELLOW), Color(COLOR_PINK)]
                                            )
                                        ),
                                        child: Container(
                                          padding: EdgeInsets.all(16),
                                          child: Image.asset(
                                              'assets/icon_camera_norm.png',
                                              width: 30,
                                              height: 30
                                          ),
                                        )
                                    )
                                )
                            ),
                          )
                      )),
                ],
              ),
            )
        )
      ],
    );
  }

  // 카메라 여는 부분
  void _startCamera() async {
    const String _channel = 'CameraActivity';
    const platform = const MethodChannel(_channel);

    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String token = prefs.getString('userToken');

      var st = await platform.invokeMethod('startCameraActivity', {'token' : token, 'cameraMode' : pageNumber});

      if(st['mode'] == '1') {
        await Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => ScrapPreviewScreen(imagePath: st['dir'], scrap: null,)));
        loadBookList();
      } else {
        MessageSnack.show(_scaffoldKey, '사진을 등록하여 서버에서 분석중입니다.\n잠시 기다렸다가 스크롤을 당겨 새로고침해주세요.');
      }

      getInsertingList();

    } on PlatformException catch (e) {
      print('FAIL');
      print(e.message);
    }
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    _tabController.removeListener(onChanged);
    _tabController.dispose();
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if(state == AppLifecycleState.resumed){
      Log.d('LifeCycle', 'Resumed');
      loadBookList();
      getUserInfo();
    }
  }

  void requestWritePermission() async {
    PermissionStatus permissionStatus = await SimplePermissions.requestPermission(Permission.WriteExternalStorage);
    PermissionStatus permissionCamera = await SimplePermissions.requestPermission(Permission.Camera);

    if (permissionStatus == PermissionStatus.authorized && permissionCamera == PermissionStatus.authorized) {
      setState(() {
        _allowWriteFile = true;
      });
    }
  }

  void getUserInfo() async {
//    var url = 'https://api.takebook.org/Account/UserInfo';
//
//    SharedPreferences prefs = await SharedPreferences.getInstance();
//    String token = prefs.getString('userToken');
//
//    var response = await http.get(url, headers: {'Authorization': '$token'});
//
//    print('Home Response status: ${response.statusCode}');
//    print('Home Response body: ${response.body}');
//
//    developer.log(response.body, name: 'apple');

    setState(() {
      userManager = UserManager();
      userManager.reloadData();
    });

    Log.d('LoginUser', userManager.userID);
  }

  void loadBookList() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String sSort = prefs.getString('sortBook');
    String sOrder = prefs.getString('orderBook');

    if(sSort == null) sSort = 'registration_date';
    if(sOrder == null) sOrder = 'desc';

    String pSort = 'sort_key=$sSort';
    String pOrder = 'sort_method=$sOrder';
    String url = 'https://api.takebook.org/Account/UserBook?$pSort&$pOrder';

    Log.d('HomeScreen - lookBookList', url);

    String token = prefs.getString('userToken');

    var response = await http.get(url, headers: {'Authorization': '$token'});

    ResultManager resultManager = ResultManager(response);
    String resultCode = resultManager.getResultCode()[0];

    Log.d('HomeScreen - loadBookList', response.body);
    Log.d('HomeScreen - loadBookList', resultManager.getFullMessage());

    if(!resultManager.isSuccess()) {
      // 실패
      MessageSnack.show(_scaffoldKey, '책 목록을 불러오는데 실패했습니다.');
      return;
    }

    Map<String, dynamic> map = resultManager.getResult();
    var item = map['item'] as List;

    setState(() {
      bookList = item.map<BookCover>((json) => BookCover.fromJson(json)).toList();
    });

    if(bookList.length == 0) {
      return;
    }

    selectedBooks.clear();

  }

  void getInsertingList() async {
    var url = 'https://api.takebook.org/Account/ImageList';

    TokenManager tokenManager = TokenManager();
    String token = await tokenManager.getToken();

    var response = await http.get(url, headers: {'Authorization' : '$token'});
    ResultManager resultManager = ResultManager(response);
    if(!resultManager.isSuccess()) {
      // 실패
      String resultCode = resultManager.getResultCode()[0];
      MessageSnack.show(_scaffoldKey, resultManager.getResultCode()[1]);
      developer.log(resultManager.getResultCode()[1], name: 'TakeBook - ERROR');
      return;
    }

    Map<String, dynamic> map = resultManager.getResult();
    setState(() {
      insertingCount = map['count'];
    });
  }

  void removeBook() async {

    var url = 'https://api.takebook.org/Account/UserBook';

    TokenManager tokenManager = TokenManager();
    String token = await tokenManager.getToken();

    HttpClient httpClient = HttpClient();
    HttpClientRequest request = await httpClient.deleteUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    request.headers.add('Authorization', '$token');
    request.add(utf8.encode(json.encode({'book_id' : selectedBooks.toList()})));

    Log.d('HomeScreen - removeBook', 'book_id = ${selectedBooks.toList()}');

    HttpClientResponse response = await request.close();

    if(response.statusCode != 200) {
      return;
    }
    httpClient.close();

    String result = await response.transform(utf8.decoder).join();
    Map<String, dynamic> map = json.decode(result);
    String resultCode = map['Result_Code'];
    String resultMessage = map['Message'];
    String msg = '$resultCode :::: $resultMessage';

    Log.d('HomeScreen - removeBook', '$msg');

    if(ERROR_CODES.contains(resultCode)) {
      // 실패
      MessageSnack.show(_scaffoldKey, '책을 삭제하는데 오류가 발생했습니다.');
      return;
    }

    MessageSnack.show(_scaffoldKey, '책을 삭제했습니다.');

    selectedBooks.clear();

    loadBookList();
  }

  void loadScrapFolderList() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String sSort = prefs.getString('sortScrap');
    String sOrder = prefs.getString('orderScrap');

    if(sSort == null) sSort = 'name';
    if(sOrder == null) sOrder = 'desc';

    String pSort = 'sort_key=$sSort';
    String pOrder = 'sort_method=$sOrder';
    String url = 'https://api.takebook.org/Account/UserScrapFolder?$pSort&$pOrder';

    TokenManager tokenManager = TokenManager();
    String token = await tokenManager.getToken();

    var response = await http.get(url, headers: {'Authorization' : '$token'});
    ResultManager resultManager = ResultManager(response);
    String resultCode = resultManager.getResultCode()[0];

    if(!resultManager.isSuccess()) {
      // 실패
      MessageSnack.show(_scaffoldKey, '스크랩 목록을 불러오는데 실패했습니다.');
      return;
    }

    Log.d('HomeScreen - getScrapFolderList', response.body);

    Map<String, dynamic> map = resultManager.getResult();
    var item = map['item'] as List;

    setState(() {
      scrapFolders = item.map<ScrapFolder>((json) => ScrapFolder.fromJson(json)).toList();
    });

  }

  void editFolder(String folderID) async {
    String token = await TokenManager().getToken();

    var response = await http.put(
        'https://api.takebook.org/Account/UserScrapFolder',
        headers: {'Authorization': '$token'},
        body: {
          'folder_id' : '$folderID',
          'modify_name' : '${folderController.text}'
        }
    );

    ResultManager resultManager = ResultManager(response);
    String resultCode = resultManager.getResultCode()[0];
    Log.d('HomeScreen - editFolder', response.body);
    Log.d('HomeScreen - editFolder', resultManager.getFullMessage());

    if(!resultManager.isSuccess()) {
      // 실패
      MessageSnack.show(_scaffoldKey, '폴더 수정 중 오류가 발생했습니다.');
      return;
    }

    if(resultCode == 'RS001') {
      MessageSnack.show(_scaffoldKey, '이미 존재하는 폴더');
    } else if(resultCode == 'RS002') {
      MessageSnack.show(_scaffoldKey, '폴더 이름을 변경할 수 없습니다.');
    }

    MessageSnack.show(_scaffoldKey, '폴더 이름을 변경했습니다.');

    loadScrapFolderList();
    selectedFolder.clear();
    selectedIndex.clear();

    setState(() {
      isAddFolder = false;
      isFolderEdit = false;
    });
  }

  void removeFolder() async {

    var url = 'https://api.takebook.org/Account/UserScrapFolder';

    TokenManager tokenManager = TokenManager();
    String token = await tokenManager.getToken();

    HttpClient httpClient = HttpClient();
    HttpClientRequest request = await httpClient.deleteUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    request.headers.add('Authorization', '$token');
    request.add(utf8.encode(json.encode({'folder_id' : selectedFolder.toList()})));

    Log.d('HomeScreen - removeFolder', 'folder_id = ${selectedFolder.toList()}');

    HttpClientResponse response = await request.close();

    if(response.statusCode != 200) {
      return;
    }
    httpClient.close();

    String result = await response.transform(utf8.decoder).join();
    Map<String, dynamic> map = json.decode(result);
    String resultCode = map['Result_Code'];
    String resultMessage = map['Message'];
    String msg = '$resultCode :::: $resultMessage';

    Log.d('HomeScreen - removeFolder', '$msg');

    if(ERROR_CODES.contains(resultCode)) {
      // 실패
      MessageSnack.show(_scaffoldKey, '폴더를 삭제하는데 오류가 발생했습니다.');
      return;
    }

    MessageSnack.show(_scaffoldKey, '폴더를 삭제했습니다.');
    selectedFolder.clear();
    selectedIndex.clear();

    loadScrapFolderList();
  }

  Future addFolder() async {

    String token = await TokenManager().getToken();

    var url = Uri.parse("https://api.takebook.org/Account/UserScrapFolder");
    var response = await http.post(url, headers: {'Authorization' : '$token'},
        body: {'folder_name' : '${folderController.text}'});

    ResultManager resultManager = ResultManager(response);

    if(!resultManager.isSuccess()) {
      // 실패
      MessageSnack.show(_scaffoldKey, '폴더를 등록하는데 오류가 발생했습니다.');
      return;
    }

    if(resultManager.getResultCode()[0] == 'RS001') {
      MessageSnack.show(_scaffoldKey, '이미 등록된 폴더입니다.');
      return;
    }

    MessageSnack.show(_scaffoldKey, '\'${folderController.text}\'폴더를 추가했습니다. 변경했습니다.');

    loadScrapFolderList();

    setState(() {
      isAddFolder = false;
    });
  }

  Future loadCandidate(BookCover book) async {
    showDialog(context: context, barrierDismissible: false, builder: (context) => loadingDialog);
    String token = await TokenManager().getToken();

    var url = Uri.https('api.takebook.org', 'Account/CandidateBook', {'book_id' : '${book.book_id}'});
    var response = await http.get(url, headers: {'Authorization' : '$token'});

    ResultManager resultManager = ResultManager(response);
    Log.d('HomeScreen - loadCandidate', response.body);
    Log.d('HomeScreen - loadCandidate', resultManager.getFullMessage());

    if(!resultManager.isSuccess()) {
      // 실패
      MessageSnack.show(_scaffoldKey, '정보를 불러오는데 오류가 발생했습니다.');
      return;
    }

    Map<String, dynamic> map = resultManager.getResult();
    var item = map['item'] as List;

    setState(() {
      bookCandidates = item.map<BookCandidate>((json) => BookCandidate.fromJson(json)).toList();
    });

    modifyBook = BookCandidate(
      isbn: book.isbn,
      title: book.title,
      author: book.author,
      publisher: book.publisher,
      imageURL: book.image_url
    );

    loadingDialog.dismiss();
  }

  Future editBook(BookCandidate book) async {
    String token = await TokenManager().getToken();

    var url = 'https://api.takebook.org/Account/UserBook';
    var map = {'book_id' : '$modifyBookID', 'modify_isbn' : '${book.isbn}'};

    HttpClient httpClient = HttpClient();
    HttpClientRequest request = await httpClient.putUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    request.headers.add('Authorization', '$token');
    request.add(utf8.encode(json.encode(map)));

    HttpClientResponse response = await request.close();
    String result = await response.transform(utf8.decoder).join();

    if(response.statusCode != 200) {
      return;
    }

    httpClient.close();

    setState(() {
      isCandidate = false;
    });

    loadBookList();
  }

  void selectMode() {
    setState(() {
      if(!isSelectMode) {
        isSelectMode = true;
      } else {
        isAddFolder = false;
        isSelectMode = false;
      }
    });
  }

  Widget deleteBookBox(int index) {
    bool visibility = false;

    if(isSelectMode) visibility = bookList[index].isSelected;
    else visibility = false;

    return Visibility(
      visible: visibility,
      child: Align(
        alignment: Alignment.topLeft,
        child: Image.asset('assets/icon_selected.png', width: 30),
      ),
    );
  }

  Widget deleteFolderBox(int index) {
    bool visibility = false;

    if(isSelectMode) visibility = scrapFolders[index].isSelected;
    else visibility = false;

    return Visibility(
      visible: visibility,
      child: Align(
        alignment: Alignment.topLeft,
        child: Image.asset('assets/icon_selected.png', width: 30),
      ),
    );
  }

  void clickBookItem(int index) async {
    // 삭제 모드일 때
    if(isSelectMode) {
      setState(() {
        if(!bookList[index].isSelected) {
          bookList[index].isSelected = true;
          selectedBooks.add(bookList[index].book_id);
        } else {
          bookList[index].isSelected = false;
          selectedBooks.remove(bookList[index].book_id);
        }
      });

    } else {
      Log.d('HomeScreen - clickBookItem', 'bookID = ${bookList[index].book_id}');
      await Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => BookinfoScreen(bookList[index].isbn, bookList[index].book_id,)));
      loadBookList();
    }
  }

  void clickFolderItem(int index) async {
    // 삭제 모드일 때
    if(isSelectMode) {
      setState(() {
        if(!scrapFolders[index].isSelected) {
          scrapFolders[index].isSelected = true;
          selectedFolder.add(scrapFolders[index].folderID);
          selectedIndex.add(index);
        } else {
          scrapFolders[index].isSelected = false;
          selectedFolder.remove(scrapFolders[index].folderID);
          selectedIndex.remove(index);
        }
      });

    } else {
      if(scrapFolders[index].count > 0) {
        await Navigator.of(context).push(MaterialPageRoute(
            builder: (BuildContext context) =>
                ScrapListScreen(scrapFolders[index].folderID,
                    scrapFolders[index].folderTitle)));

        loadScrapFolderList();

      } else {
        MessageSnack.show(_scaffoldKey, '스크랩이 없습니다.\n스크랩을 등록해주세요.');
      }
    }
  }

  ImageProvider profileImage(String url) {
    if(url == null) {
      return AssetImage('assets/icon_profile_norm.png');
    } else {
      return NetworkImage(url);
    }
  }

  Widget candidateBookWidget() {
    return Stack(
      children: <Widget>[
        GestureDetector(
          onTap: (){
            setState(() {
              isCandidate = false;
            });
          },
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            color: Colors.black54,
          ),
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(30)
            ),
            height: 500,
            child: candidateDialogItem(),
          ),
        ),
      ],
    );
  }

  Widget candidateDialogItem() {
    if(bookCandidates == null) {
      return Container();
    } else {
      return Stack(
        children: <Widget>[
          ListView.builder(
            scrollDirection: Axis.vertical,
            itemCount: bookCandidates.length + 2,
            itemBuilder: (BuildContext context, int index) => candidateListItem(index),
          )
        ],
      );
    }
  }

  Widget candidateListItem(int index) {
    TextEditingController searchController = TextEditingController();

    if(index == 0) {
      return Container(
        child: Column(
          children: <Widget>[
            Text(
              '분석한 결과에서 가장 유사한 5개의 책 목록입니다.\n책이 없다면 검색을 통해 찾을 수 있습니다.',
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.black54),
            ),
            GestureDetector(
              child: Container(
                color: Colors.transparent,
                padding: EdgeInsets.all(24),
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(left: 8, right: 30),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(30),
                          color: Color(BACKGROUND_COLOR)
                      ),
                      child: TextField(
                        controller: searchController,
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(10),
                          border: InputBorder.none,
                        ),
                        style: TextStyle(fontSize: 14),
                      ),
                    ),
                    Align(
                      alignment: Alignment.centerRight,
                      child: Container(
                        child: ClipOval(
                            child: GestureDetector(
                                onTap: () async {
                                  if(searchController.text != null && searchController.text != '') {
                                    await Navigator.push(context, MaterialPageRoute(builder: (context) => SearchScreen(bookID: modifyBookID, searchText: searchController.text)));
                                    loadBookList();
                                    isCandidate = false;
                                  }
                                },
                                child: Container(
                                    width: 34,
                                    height: 34,
                                    decoration: BoxDecoration(
                                        color: Color(COLOR_YELLOW)
                                    ),
                                    child: Container(
                                      padding: EdgeInsets.all(8),
                                      child: Image.asset(
                                        'assets/icon_search_norm.png',
                                        width: 30,
                                        height: 30,
                                        color: Colors.white,
                                      ),
                                    )
                                )
                            )
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      );
    } else if(index == 1) {
      return GestureDetector(
        onTap: (){
          editBook(modifyBook);
          //Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => ScrapListScreen(scrapFolders[index].folderID)))
        },
        child: candidateItem(modifyBook),
      );
    } else {
      return GestureDetector(
        onTap: (){
          editBook(bookCandidates[index - 2]);
          //Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => ScrapListScreen(scrapFolders[index].folderID)))
        },
        child: candidateItem(bookCandidates[index - 2]),
      );
    }
  }

  Widget candidateItem(BookCandidate book) {
    return Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.symmetric(horizontal: 24),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width - 170,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(book.title, maxLines: 3, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16)),
                    Text(book.author, maxLines: 3, style: TextStyle(fontSize: 14)),
                  ],
                ),
              ),
              Container(
                width: 120,
                padding: EdgeInsets.all(12),
                child: BookImage().bookImage(book.imageURL),
              ),
            ],
          ),
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            height: 1,
            margin: EdgeInsets.symmetric(horizontal: 24),
            color: Colors.black12,
          ),
        ),
      ],
    );
  }

  Widget dateWidget(int index) {
    BookCover cover = bookList[index];

    if(cover.second_candidate != null || cover.third_candidate != null || cover.fourth_candidate != null || cover.fifth_candidate != null) {
      return Text('New', style: TextStyle(color: Color(COLOR_YELLOW), fontSize: 10));
    } else {
      return Text(cover.registration_date.split(' ')[0], style: TextStyle(fontSize: 10));
    }
  }

  Widget firstPage() {
    var size = MediaQuery.of(context).size;

    /*24 is for notification bar on Android*/
    final double itemWidth = size.width / 3;
    final double itemHeight = (itemWidth / 3) * 7;

    print("$itemWidth / $itemHeight");

    if(bookList.length == 0 || bookList == null) {
      return SmartRefresher(
        controller: _refreshController,
        onRefresh: () {
          loadBookList();
          getInsertingList();
          Log.d('HomeScreen - SmartRefresher', 'onRefresh');
          _refreshController.refreshCompleted();
        },
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Container(
                      height: 100,
                    ),
                    Container(
                      height: 50,
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: [
                                Colors.transparent,
                                Colors.black12
                              ])),
                    ),
                    Container(
                      height: 12,
                    ),
                    Container(
                      height: 50,
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: [
                                Colors.black12,
                                Colors.transparent
                              ])),
                    ),
                  ],
                ),
                Column(
                  children: <Widget>[
                    Container(
                      height: 50,
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: [
                                Colors.transparent,
                                Colors.black12
                              ])),
                    ),
                    Container(
                      height: 12,
                    ),
                    Container(
                      height: 50,
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: [
                                Colors.black12,
                                Colors.transparent
                              ])),
                    ),
                  ],
                ),
                Container(
                  height: 100,
                ),
              ],
            ),
            Container(
              padding: EdgeInsets.only(bottom: 86),
              child: Text('등록된 도서가 없습니다.\n책을 찍어 도서를 등록해보세요.', style: TextStyle(color: Colors.black54), textAlign: TextAlign.center,),
            ),
          ],
        )
      );
    }

    return Stack(
      children: <Widget>[
        SmartRefresher(
          controller: _refreshController,
          onRefresh: () {
            loadBookList();
            getInsertingList();
            Log.d('HomeScreen - SmartRefresher', 'onRefresh');
            _refreshController.refreshCompleted();
          },
          child: GridView.builder(
              controller: _scrollController,
              itemCount: bookList.length,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3, childAspectRatio: (itemWidth / itemHeight)),
              itemBuilder: (BuildContext context, int index) => GestureDetector(
                onTap: () async {
                  if(bookList[index].second_candidate != null && !isSelectMode) {
                    modifyBookID = bookList[index].book_id;
                    await loadCandidate(bookList[index]);
                    setState(() {
                      isCandidate = true;
                    });
                  } else {
                    clickBookItem(index);
                  }
                  //removeBook(bookList[index].book_id, index)
                },
                onLongPress: (){
                  setState(() {
                    isSelectMode = true;
                    selectedBooks.add(bookList[index].book_id);
                    bookList[index].isSelected = true;
                  });
                },
                child: Stack(
                  children: <Widget>[
                    Container(
                        child: Column(
                          children: <Widget>[
                            Container(
                              alignment: Alignment.center,
                              child: Column(
                                children: <Widget>[
                                  Container(
                                    child: Align(
                                      alignment: Alignment.centerLeft,
                                      child: dateWidget(index),
                                    ),
                                    padding: EdgeInsets.fromLTRB(14, 6, 0, 0),
                                  ),
                                  BookImage().bookImage(bookList[index].image_url)
                                ],
                              ),
                            ),
                            Container(
                                alignment: Alignment.bottomCenter,
                                padding: EdgeInsets.fromLTRB(0, 14, 0, 0),
                                child: Stack(
                                  children: <Widget>[
                                    Container(
                                      height: 50,
                                      decoration: BoxDecoration(
                                          gradient: LinearGradient(
                                              begin: Alignment.topCenter,
                                              end: Alignment.bottomCenter,
                                              colors: [
                                                Colors.black12,
                                                Colors.transparent
                                              ])),
                                    ),
                                    Column(children: <Widget>[
                                      Container(
                                        padding: EdgeInsets.fromLTRB(14, 12, 14, 0),
                                        child: Align(
                                          alignment: Alignment.centerLeft,
                                          child: Text(
                                            bookList[index].title,
                                            style: TextStyle(fontSize: 12),
                                            maxLines: 2,
                                          ),
                                        ),
                                      ),
                                      Container(
                                        padding: EdgeInsets.fromLTRB(14, 0, 14, 0),
                                        child: Align(
                                          alignment: Alignment.centerLeft,
                                          child: Text(
                                            bookList[index].author,
                                            style: TextStyle(fontSize: 12, color: Colors.black45),
                                            maxLines: 1,
                                          ),
                                        ),
                                      ),
                                    ],)
                                  ],
                                )),
                          ],
                        )
                    ),
                    deleteBookBox(index),
                  ],
                ),
              )
          ),
        ),
      ],
    );
  }

  Widget secondPage() {
    return SmartRefresher(
        controller: _refreshScrapFolderController,
        onRefresh: () {
          loadScrapFolderList();
          Log.d('HomeScreen - SmartRefresher', 'onRefresh');
          _refreshScrapFolderController.refreshCompleted();
        },
        child: ListView.builder(
              scrollDirection: Axis.vertical,
              itemCount: scrapFolders.length,
              itemBuilder: (BuildContext context, int index) => GestureDetector(
                onTap: (){
                  clickFolderItem(index);
                  //Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => ScrapListScreen(scrapFolders[index].folderID)))
                },
                child: Stack(
                  children: <Widget>[
                    Container(
                      height: 160,
                      padding: EdgeInsets.all(8),
                      child: Container(
                        child: Stack(
                          children: <Widget>[
                            Container(
                              width: double.infinity,
                              child: Image.asset('assets/img_folder.png', fit: BoxFit.fill, centerSlice: Rect.fromLTWH(145, 80, 10, 10),),
                            ),
                            Positioned(
                              top: 40,
                              left: 25,
                              child: Text('${scrapFolders[index].folderTitle}', style: TextStyle(fontSize: 16)),
                            ),
                            Align(
                              alignment: Alignment.bottomRight,
                              child: Container(
                                margin: EdgeInsets.only(right: 18, bottom: 18),
                                child: Text(scrapFolders[index].createDate.split(' ')[0]),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    deleteFolderBox(index),
                  ],
                )
            )
        )
    );
  }

  Widget dialogFolder() {
    List<String> titles = ['새 폴더', '폴더 수정'];

    int menu = 0;

    if(!isFolderEdit) {
      menu = 0;
    } else {
      menu = 1;
    }

    return Stack(
      children: <Widget>[
        GestureDetector(
          onTap: (){
            setState(() {
              isAddFolder = false;
              isFolderEdit = false;
            });
          },
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            color: Colors.black54,
          ),
        ),
        Align(
          alignment: Alignment.center,
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: 170,
            padding: EdgeInsets.all(24),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(30),
              color: Colors.white,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(titles[menu], style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18), textAlign: TextAlign.left,),
                Container(
                  height: 12,
                ),
                TextField(
                  controller: folderController,
                  style: TextStyle(fontSize: 16),
                  decoration: InputDecoration(
                      filled: true,
                      fillColor: Color(BACKGROUND_COLOR),
                      contentPadding: EdgeInsets.all(8),
                      border: InputBorder.none
                  ),
                ),
                Align(
                  alignment: Alignment.bottomRight,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      GestureDetector(
                        onTap: (){
                          setState(() {
                            isAddFolder = false;
                          });
                        },
                        child: Container(
                          color: Colors.white,
                          padding: EdgeInsets.all(12),
                          child: Text('취소', style: TextStyle(fontSize: 16),),
                        ),
                      ),
                      Container(
                        width: 12,
                      ),
                      GestureDetector(
                        onTap: (){
                          if(menu == 0) {
                            addFolder();
                          } else {
                            editFolder(selectedFolder[0]);
                          }
                        },
                        child: Container(
                          color: Colors.white,
                          padding: EdgeInsets.all(12),
                          child: Text('확인', style: TextStyle(fontSize: 16,),),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}

class InsertingBar extends StatelessWidget {

  int insertingCount = 0;

  InsertingBar(int insertingCount) {
    this.insertingCount = insertingCount;
  }

  @override
  Widget build(BuildContext context) {
    if(insertingCount == 0) {
      return Container();
    } else {
      return Container(
        child: Container(
          margin: EdgeInsets.fromLTRB(12, 0, 12, 0),
          width: MediaQuery.of(context).size.width - 24,
          height: 30,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(20),
            child: Container(
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.centerLeft,
                        end: Alignment.centerRight,
                        colors: [Color(COLOR_YELLOW), Color(COLOR_PINK)]
                    )
                ),
                child: Stack(
                  children: <Widget>[
                    Align(
                        alignment: Alignment.centerLeft,
                        child: Row(
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.fromLTRB(12, 0, 6, 0),
                              child: Image.asset(
                                'assets/icon_inserting_list.png',
                                width: 18,
                              ),
                            ),
                            Text('등록 리스트', style: TextStyle(fontSize: 12),),
                          ],
                        )
                    ),
                    Align(
                        alignment: Alignment.centerRight,
                        child: Container(
                          margin: EdgeInsets.fromLTRB(0, 0, 12, 0),
                          child: Text('$insertingCount',
                              style: TextStyle(fontSize: 12)),
                        )
                    )
                  ],
                )
            ),
          ),
        ),
      );
    }
  }
}

class SortBookDialog extends StatefulWidget {
  @override
  _SortBookDialogState createState() => _SortBookDialogState();
}

class _SortBookDialogState extends State<SortBookDialog> {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

