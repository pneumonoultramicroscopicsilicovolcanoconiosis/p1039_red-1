import 'package:shared_preferences/shared_preferences.dart';

class TokenManager {
  Future<String> getToken() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('userToken');

    return token;
  }

  Future removeToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('userToken');
  }
}