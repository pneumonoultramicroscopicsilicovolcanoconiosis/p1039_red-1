import 'package:flutter/material.dart';
import 'package:redproject/DTO/Book.dart';
import 'package:redproject/DTO/Comment.dart';
import 'package:redproject/DTO/Reader.dart';
import 'package:redproject/Constant.dart';
import 'package:redproject/pack.dart';
import 'package:redproject/ReplyScreen.dart';
import 'package:redproject/FollowerScreen.dart';
import 'package:expandable/expandable.dart';
import 'package:sprintf/sprintf.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:http/http.dart' as http;
import 'dart:developer' as developer;
import 'dart:ui';

class BookinfoScreen extends StatefulWidget {

  String isbn;
  String bookID;

  BookinfoScreen(this.isbn, this.bookID);

  @override
  _BookinfoScreenState createState() => _BookinfoScreenState();
}

class _BookinfoScreenState extends State<BookinfoScreen> {
  final double profileIconSize = 40.0;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final commentController = TextEditingController();

  Book book;
  Comment myComment;
  UserManager userManager;

  List<String> sendButtonColors = ['assets/icon_comment_send_grey.png', 'assets/icon_comment_send_color.png'];

  List<Comment> comments = List();
  List<Comment> replies = List();
  List<Reader> readers = List();

  List<String> bookmarks = ['등록 안 함', '등록'];

  String bookID;
  var bookRate;
  int starRate = 0;
  int comment = 0;          // 댓글을 등록할 수 있는지 확인
  bool isLoaded = false;    // 정보를 불러왔는지 확인
  bool isEditing = false;   // 댓글을 수정할 때
  bool isExist = false;     // 책장에 있는 책
  bool isBuy = false;       // 구매

  Future doVote(String commentID, int commentVote) async {
    String text = '';

    if(commentVote == 1) text = '좋아요';
    else if(commentVote == -1) text = '싫어요';

    Log.d('BookInfoScreen - doVote', 'commentVote = $commentVote');

    String token = await TokenManager().getToken();

    var response = await http.post(
        'https://api.takebook.org/Account/VoteComment',
        headers: {'Authorization': '$token'},
        body: {
          'comment_id': '$commentID',
          'vote': '$commentVote'
        }
    );

    ResultManager resultManager = ResultManager(response);
    String resultCode = resultManager.getResultCode()[0];

    Log.d('BookInfoScreen - doVote', response.body);
    Log.d('BookInfoScreen - doVote', resultManager.getFullMessage());

    if (!resultManager.isSuccess()) {
      // 실패
      MessageSnack.show(_scaffoldKey, '$text를 하는데 오류가 발생했습니다.');
      return;
    }

    if(resultCode == 'RS001') {
      if(commentVote == 1) {
        await cancelVote(commentID);
        doVote(commentID, 1);
      } else {
        await cancelVote(commentID);
        doVote(commentID, -1);
      }
    }

    loadComment(widget.isbn);
  }

  Future cancelVote(String commentID) async {
    String token = await TokenManager().getToken();

    var url = 'https://api.takebook.org/Account/VoteComment';
    var params = {'comment_id' : '$commentID'};

    HttpClient httpClient = HttpClient();
    HttpClientRequest request = await httpClient.deleteUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    request.headers.add('Authorization', '$token');
    request.add(utf8.encode(json.encode(params)));

    HttpClientResponse response = await request.close();

    if(response.statusCode != 200) {
      return;
    }
    httpClient.close();

    String result = await response.transform(utf8.decoder).join();
    Map<String, dynamic> map = json.decode(result);
    String resultCode = map['Result_Code'];
    String resultMessage = map['Message'];
    String msg = '$resultCode :::: $resultMessage';

    Log.d('BookInfoScreen - cancelVote', '$msg');

    if(ERROR_CODES.contains(resultCode)) {
      // 실패
      MessageSnack.show(_scaffoldKey, '좋아요/싫어요를 제거하는데 오류가 발생했습니다.');
      return;
    }

    loadComment(widget.isbn);
  }

  bool isMyComment(String writer) {
    UserManager userManager = UserManager();
    Log.d('BookInfoScreen', 'user = ${userManager.userID}, writer = $writer');

    if(userManager.userID == writer) return true;
    else return false;
  }

  void detectCommentTextChanging(String text) {
    setState(() {
      if(text.length > 0 && text != '' && starRate > 0) {
        comment = 1;
      } else {
        comment = 0;
      }
    });
  }

  void loadBook(String isbn) async {

    TokenManager tokenManager = TokenManager();
    String token = await tokenManager.getToken();

    var url = Uri.parse("https://api.takebook.org/Book/Detail?isbn=$isbn");
    var response = await http.get(url);

    developer.log(response.body, name: 'BookInfo');

    ResultManager resultManager = ResultManager(response);

    if(!resultManager.isSuccess()) {
      // 실패
      MessageSnack.show(_scaffoldKey, '책 정보를 불러오는데 오류가 발생했습니다.');
      return;
    }

    Map<String, dynamic> map = resultManager.getResult();
    isLoaded = true;

    setState(() {
      book = Book.fromJson(map);
    });
  }

  void loadComment(String isbn) async {

    TokenManager tokenManager = TokenManager();
    String token = await tokenManager.getToken();

    developer.log(token, name: 'TakeBook - loadComment');

    //var url = Uri.parse("https://api.takebook.org/Account/Comment?isbn=$isbn");
    var url = Uri.https('api.takebook.org', 'Account/Comment', {'isbn' : '$isbn'});
    var response = await http.get(url, headers: {'Authorization' : '$token'});

    Log.d('BookInfoScreen - loadComment', response.body);

    ResultManager resultManager = ResultManager(response);

    if(!resultManager.isSuccess()) {
      // 실패
      MessageSnack.show(_scaffoldKey, '댓글 정보를 불러오는데 오류가 발생했습니다.');
      return;
    }

    Map<String, dynamic> map = resultManager.getResult();
    var item = map['item'] as List;

    setState(() {
      comments = item.map<Comment>((json) => Comment.fromJson(json)).toList();
    });
  }

  void loadMyComment(String isbn) async {
    var url = 'https://api.takebook.org/Account/MyComment?isbn=$isbn';

    TokenManager tokenManager = TokenManager();
    String token = await tokenManager.getToken();

    var response = await http.get(url, headers: {'Authorization' : '$token'});
    ResultManager resultManager = ResultManager(response);
    String resultCode = resultManager.getResultCode()[0];

    Log.d('BookInfoScreen - loadMyComment', response.body);
    Log.d('BookInfoScreen - loadMyComment', resultManager.getFullMessage());

    if(!resultManager.isSuccess()) {
      // 실패
      MessageSnack.show(_scaffoldKey, '내 댓글을 불러오는데 오류가 발생했습니다.');
      return;
    }

    if(resultCode == 'RS001') {
      myComment = null;
      return;
    }

    Map<String, dynamic> map = resultManager.getResult();

    myComment = Comment(
        commentID: map['comment_id'],
        userID: map['user_id'],
        registrationDate: map['registration_date'],
        contents: map['contents'],
        goodCount: map['good_cnt'],
        badCount: map['bad_cnt'],
        recommentCount: map['recomment_cnt'],
        vote: map['vote'],
        grade: map['grade'],
        name: map['name'],
        profileURL: map['profile_url']
    );

    setState(() {
      commentController.text = myComment.contents;
      starRate = myComment.grade;
    });

  }

  Future loadReader(String isbn) async {
    String token = await TokenManager().getToken();

    var url = Uri.https('api.takebook.org', 'Account/UserReadSameBook', {'isbn' : '$isbn'});
    var response = await http.get(url, headers: {'Authorization' : '$token'});

    ResultManager resultManager = ResultManager(response);
    String resultCode = resultManager.getResultCode()[0];
    String resultMessage = resultManager.getResultCode()[1];

    Log.d('BookInfoScreen - loadReader', response.body);
    Log.d('BookInfoScreen - loadReader', resultManager.getFullMessage());

    if(!resultManager.isSuccess()) {
      // 실패
      MessageSnack.show(_scaffoldKey, '이 책을 읽는 사용자 목록을 불러오는데 오류가 발생했습니다.');
      return;
    }

    Map<String, dynamic> map = resultManager.getResult();
    var item = map['item'] as List;

    setState(() {
      readers = item.map<Reader>((json) => Reader.fromJson(json)).toList();
    });
  }

  Future loadGrade(String isbn) async {
    String token = await TokenManager().getToken();

    var url = Uri.https('api.takebook.org', 'Account/Grade', {'isbn' : '$isbn'});
    var response = await http.get(url, headers: {'Authorization' : '$token'});

    ResultManager resultManager = ResultManager(response);
    String resultCode = resultManager.getResultCode()[0];
    String resultMessage = resultManager.getResultCode()[1];

    Log.d('BookInfoScreen - loadGrade', response.body);
    Log.d('BookInfoScreen - loadGrade', resultManager.getFullMessage());

    if(!resultManager.isSuccess()) {
      // 실패
      MessageSnack.show(_scaffoldKey, '평점 정보를 불러오는데 오류가 발생했습니다.');
      return;
    }

    Map<String, dynamic> map = resultManager.getResult();

    setState(() {
      if(map['grade'] == null) bookRate = 0;
      else bookRate = map['grade'];
    });
  }

  Future checkExistBook(String isbn) async {
    String token = await TokenManager().getToken();

    var url = Uri.https('api.takebook.org', 'Account/CheckUserISBNExists', {'isbn' : '$isbn'});
    var response = await http.get(url, headers: {'Authorization' : '$token'});

    ResultManager resultManager = ResultManager(response);
    String resultCode = resultManager.getResultCode()[0];
    String resultMessage = resultManager.getResultCode()[1];

    Log.d('BookInfoScreen - checkExistBook', response.body);
    Log.d('BookInfoScreen - checkExistBook', resultManager.getFullMessage());

    if(!resultManager.isSuccess()) {
      // 실패
      MessageSnack.show(_scaffoldKey, '책장 존재 유무를 불러오는데 오류가 발생했습니다.');
      return;
    }

    setState(() {
      if(resultCode == 'RS000') {
        isExist = true;
      } else {
        isExist = false;
      }
    });
  }

  Future uploadComment() async {
    TokenManager tokenManager = TokenManager();
    String token = await tokenManager.getToken();

    developer.log(token, name: 'TakeBook - loadComment');

    var url = Uri.parse("https://api.takebook.org/Account/Comment");
    var response = await http.post(url, headers: {'Authorization' : '$token'},
        body: {'isbn' : '${book.isbn}', 'contents' : '${commentController.text}'});

    developer.log(response.body, name: 'Upload Comment');

    ResultManager resultManager = ResultManager(response);

    if(!resultManager.isSuccess()) {
      // 실패
      MessageSnack.show(_scaffoldKey, '댓글을 등록하는데 오류가 발생했습니다.');
      return;
    }

    if(resultManager.getResultCode()[0] == 'RS001') {
      MessageSnack.show(_scaffoldKey, '이미 댓글을 작성하셨습니다. 답글만 입력 가능합니다.');
      return;
    }

    FocusScope.of(context).requestFocus(new FocusNode());
    MessageSnack.show(_scaffoldKey, '댓글을 등록했습니다.');

    uploadGrade();
  }

  Future uploadGrade() async {
    String token = await TokenManager().getToken();

    var response = await http.post(
        'https://api.takebook.org/Account/Grade',
        headers: {'Authorization': '$token'},
        body: {
          'isbn' : '${widget.isbn}',
          'grade' : '$starRate'
        }
    );

    ResultManager resultManager = ResultManager(response);
    String resultCode = resultManager.getResultCode()[0];
    String resultMessage = resultManager.getResultCode()[1];
    String msg = '$resultCode :: $resultMessage';

    Log.d('UserInfoScreen - uploadGrade', '${resultManager.getFullMessage()}');

    if(!resultManager.isSuccess()) {
      // 실패
      MessageSnack.show(_scaffoldKey, '평점을 등록하는데 오류가 발생했습니다.');
      developer.log(resultManager.getResultCode()[1], name: 'TakeBook - ERROR');
      return;
    }

    if(resultManager.getResultCode()[0] == 'RS001') {
      MessageSnack.show(_scaffoldKey, '이미 평점을 등록했습니다.');
      return;
    }

    loadComment(book.isbn);
  }

  Future addBook(String isbn) async {
    String token = await TokenManager().getToken();

    var response = await http.post(
        'https://api.takebook.org/Account/UserBook',
        headers: {'Authorization': '$token'},
        body: {
          'isbn' : '${widget.isbn}'
        }
    );

    ResultManager resultManager = ResultManager(response);

    Log.d('UserInfoScreen - addBook', response.body);
    Log.d('UserInfoScreen - addBook', resultManager.getFullMessage());

    if(!resultManager.isSuccess()) {
      // 실패
      MessageSnack.show(_scaffoldKey, '책을 등록하는데 오류가 발생했습니다.');
      return;
    }

    Map<String, dynamic> map = resultManager.getResult();
    bookID = map['book_id'];

    MessageSnack.show(_scaffoldKey, '책을 등록했습니다.');
    
    checkExistBook(widget.isbn);
  }

  void removeBook() async {

    var url = 'https://api.takebook.org/Account/UserBooks';

    TokenManager tokenManager = TokenManager();
    String token = await tokenManager.getToken();

    Log.d('BookInfoScreen - removeBook', 'isbn = ${widget.isbn}');

    HttpClient httpClient = HttpClient();
    HttpClientRequest request = await httpClient.deleteUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    request.headers.add('Authorization', '$token');
    request.add(utf8.encode(json.encode({'isbn' : '${widget.isbn}'})));

    Log.d('BookInfoScreen - removeBook', 'book_id = ${widget.isbn}');

    HttpClientResponse response = await request.close();

    if(response.statusCode != 200) {
      return;
    }
    httpClient.close();

    String result = await response.transform(utf8.decoder).join();
    Map<String, dynamic> map = json.decode(result);
    String resultCode = map['Result_Code'];
    String resultMessage = map['Message'];
    String msg = '$resultCode :::: $resultMessage';

    Log.d('BookInfoScreen - removeBook', '$msg');

    if(ERROR_CODES.contains(resultCode)) {
      // 실패
      MessageSnack.show(_scaffoldKey, '책을 삭제하는데 오류가 발생했습니다.');
      return;
    }

    MessageSnack.show(_scaffoldKey, '책을 삭제했습니다.');

    checkExistBook(widget.isbn);
  }

  Future editComment(String commentID) async {
    String token = await TokenManager().getToken();

    var url = 'https://api.takebook.org/Account/Comment';
    var map = {'comment_id' : '$commentID', 'contents' : '${commentController.text}'};

    HttpClient httpClient = HttpClient();
    HttpClientRequest request = await httpClient.putUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    request.headers.add('Authorization', '$token');
    request.add(utf8.encode(json.encode(map)));

    HttpClientResponse response = await request.close();
    String result = await response.transform(utf8.decoder).join();

    if(response.statusCode != 200) {
      return;
    }

    httpClient.close();

    FocusScope.of(context).requestFocus(new FocusNode());
    MessageSnack.show(_scaffoldKey, '댓글을 수정했습니다.');

    loadComment(widget.isbn);
  }

  Future editGrade(String isbn, int grade) async {
    String token = await TokenManager().getToken();

    var url = 'https://api.takebook.org/Account/Comment';
    var map = {'isbn' : '$isbn', 'grade' : '$grade'};

    HttpClient httpClient = HttpClient();
    HttpClientRequest request = await httpClient.putUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    request.headers.add('Authorization', '$token');
    request.add(utf8.encode(json.encode(map)));

    HttpClientResponse response = await request.close();
    String result = await response.transform(utf8.decoder).join();

    if(response.statusCode != 200) {
      return;
    }

    httpClient.close();

    loadComment(widget.isbn);
  }

  Future removeComment(String commentID) async {
    String token = await TokenManager().getToken();

    var url = 'https://api.takebook.org/Account/Comment';
    var params = {'comment_id' : '$commentID'};

    HttpClient httpClient = HttpClient();
    HttpClientRequest request = await httpClient.deleteUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    request.headers.add('Authorization', '$token');
    request.add(utf8.encode(json.encode(params)));

    HttpClientResponse response = await request.close();

    if(response.statusCode != 200) {
      return;
    }
    httpClient.close();

    String result = await response.transform(utf8.decoder).join();
    Map<String, dynamic> map = json.decode(result);
    String resultCode = map['Result_Code'];
    String resultMessage = map['Message'];
    String msg = '$resultCode :::: $resultMessage';

    Log.d('UserInfoScreen - cancelFollow', '$msg');

    if(ERROR_CODES.contains(resultCode)) {
      // 실패
      MessageSnack.show(_scaffoldKey, '댓글을 삭제하는데 오류가 발생했습니다.');
      return;
    }

    MessageSnack.show(_scaffoldKey, '댓글을 삭제했습니다.');

    removeGrade();
    //loadComment(widget.isbn);
  }

  Future removeGrade() async {
    String token = await TokenManager().getToken();

    var url = 'https://api.takebook.org/Account/Grade';
    var params = {'isbn' : '${widget.isbn}'};

    HttpClient httpClient = HttpClient();
    HttpClientRequest request = await httpClient.deleteUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    request.headers.add('Authorization', '$token');
    request.add(utf8.encode(json.encode(params)));

    HttpClientResponse response = await request.close();

    if(response.statusCode != 200) {
      return;
    }
    httpClient.close();

    String result = await response.transform(utf8.decoder).join();
    Map<String, dynamic> map = json.decode(result);
    String resultCode = map['Result_Code'];
    String resultMessage = map['Message'];
    String msg = '$resultCode :::: $resultMessage';

    Log.d('UserInfoScreen - cancelFollow', '$msg');

    if(ERROR_CODES.contains(resultCode)) {
      // 실패
      MessageSnack.show(_scaffoldKey, '평점을 삭제하는데 오류가 발생했습니다.');
      return;
    }

    MessageSnack.show(_scaffoldKey, '평점을 삭제했습니다.');

    setState(() {
      starRate = 0;
      commentController.text = '';
      isEditing = false;
      comment = 0;
    });

    loadGrade(widget.isbn);
    loadComment(widget.isbn);
  }

  bool hasReaders() {
    if(readers.length == 0) {
      return false;
    } else {
      return true;
    }
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    TextStyle styleWhite = TextStyle(
      color: Colors.black,
      fontSize: 14,
    );

    TextStyle styleTitle = TextStyle(
      fontSize: 18,
      fontWeight: FontWeight.bold,
    );

    if(isLoaded) {
      return Scaffold(
        key: _scaffoldKey,
        body: Stack(
          children: <Widget>[
            Column(children: <Widget>[
              Container(
                margin: EdgeInsets.fromLTRB(0, MediaQuery.of(context).padding.top, 0, 0),
                height: 50,
                child: Stack(
                  children: <Widget>[
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Container(
                        height: 1,
                        color: Colors.black12,
                      ),
                    ),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: GestureDetector(
                        onTap: (){
                          Navigator.of(context).pop();
                        },
                        child: Container(
                          width: 18,
                          height: 18,
                          margin: EdgeInsets.only(left: 12),
                          child: Image.asset('assets/icon_back.png'),
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.center,
                      child: Padding(
                        padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                        child: Text(book.title),
                      ),
                    ),
                    Align(
                      alignment: Alignment.centerRight,
                      child: Padding(
                        padding: EdgeInsets.fromLTRB(0, 0, 12, 0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            bookmarkButton(),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Expanded(child:
              SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  verticalDirection: VerticalDirection.down,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Container(
                          height: 24,
                        ),
                        Container(
                          width: 150,
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(0.0),
                            child: Image.network(book.image_url),
                          ),
                        ),
                        Container(
                          width: 250,
                          height: 20,
                          decoration: BoxDecoration(
                              border: Border.all(
                                  width: 1,
                                  color: Colors.black38
                              )
                          ),
                        ),
                        Stack(
                          children: <Widget>[
                            Container(
                              width: 250,
                              height: 50,
                              decoration: BoxDecoration(
                                  gradient: LinearGradient(
                                      begin: Alignment.topCenter,
                                      end: Alignment.bottomCenter,
                                      colors: [
                                        Colors.black12,
                                        Colors.transparent
                                      ])),
                            ),
                            Container(
                                width: 250,
                                margin: EdgeInsets.only(top: 12),
                                child: Center(
                                  child: Text(
                                    book.title,
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                )
                            ),
                          ],
                        ),
                        Text(book.author),
                        Container(height: 12,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Image.asset('assets/icon_star_color.png', width: 16, height: 16),
                            Container(width: 5,),
                            Text('$bookRate'),
                            Container(width: 16,),
                            Image.asset('assets/icon_comment.png', width: 16, height: 16),
                            Container(width: 5,),
                            Text('${comments.length}'),
                          ],
                        ),
                        Container(height: 12),
                        Text('${book.publisher} | ${book.published_date} | ${book.price}'),
                        Container(height: 12),
                      ],
                    ),
                    dividerContainer(),
                    Visibility(
                      visible: hasReaders(),
                      child: Column(
                        children: <Widget>[
                          Align(
                            alignment: Alignment.center,
                            child: Text('함께 읽고있는 북친'),
                          ),
                          Container(
                            height: 100,
                            padding: EdgeInsets.symmetric(vertical: 20),
                            child: listReader(),
                          ),
                        ],
                      ),
                    ),
                    dividerContainer(),
                    listContainer(
                      ExpandablePanel(
                        header: Container(
                          height: 50,
                          padding: EdgeInsets.only(left: 10),
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text('소개', style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
                          ),
                        ),
                        collapsed: Container(
                          padding: EdgeInsets.only(left: 10),
                          child: Text(book.discriptions, softWrap: true, maxLines: 3, overflow: TextOverflow.ellipsis,),
                        ),
                        expanded: Container(
                          padding: EdgeInsets.only(left: 10),
                          child:  Text(book.discriptions, softWrap: true, ),
                        ),
                      ),
                    ),
                    dividerContainer(),
                    listContainer(
                      ExpandablePanel(
                        header: Container(
                          height: 50,
                          padding: EdgeInsets.only(left: 10),
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text('목차', style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
                          ),
                        ),
                        collapsed: Container(
                          padding: EdgeInsets.only(left: 10, right: 10),
                          child: Text(book.contents, softWrap: true, maxLines: 3, overflow: TextOverflow.ellipsis,),
                        ),
                        expanded: Container(
                          padding: EdgeInsets.only(left: 10, right: 10),
                          child:  Text(book.contents, softWrap: true, ),
                        ),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      padding: EdgeInsets.symmetric(vertical: 6, horizontal: 12),
                      color: Colors.black12,
                      child: Text('정보 출처 - 알라딘', style: TextStyle(color: Colors.black54,), textAlign: TextAlign.right,),
                    ),
                    listContainer(
                      Column(
                        children: <Widget>[
                          Container(
                            height: 50,
                            padding: EdgeInsets.only(left: 10),
                            child: Stack(
                              children: <Widget>[
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text('댓글', style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
                                ),
                              ],
                            ),
                          ),
                          Column(
                            children: <Widget>[
                              Container(
                                  height: 1,
                                  color: Colors.black12
                              ),
                              Container(
                                  margin: EdgeInsets.only(left: 12, top: 8),
                                  child: Align(
                                    alignment: Alignment.centerLeft,
                                    child: Text('내 평점과 댓글', style: TextStyle(color: Colors.black54, fontWeight: FontWeight.bold)),
                                  )
                              ),
                              Container(
                                height: 50,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: <Widget>[
                                        star(1),
                                        dividerStar(),
                                        star(2),
                                        dividerStar(),
                                        star(3),
                                        dividerStar(),
                                        star(4),
                                        dividerStar(),
                                        star(5)
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                height: 80,
                                padding: EdgeInsets.only(left: 12),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                                  children: <Widget>[
                                    ProfileImage(
                                        userManager.profileURL,
                                        width: 46,
                                        height: 46,
                                      ),
                                    Flexible(
                                      child: TextField(
                                        controller: commentController,
                                        style: TextStyle(fontSize: 14),
                                        keyboardType: TextInputType.multiline,
                                        maxLines: null,
                                        onChanged: (text){
                                          detectCommentTextChanging(text);
                                        },
                                        decoration: InputDecoration(
                                          hintText: '댓글 쓰기',
                                          contentPadding: EdgeInsets.all(10),
                                          border: InputBorder.none,
                                        ),
                                      ),
                                    ),
                                    sendButton()
                                  ],
                                ),
                              ),
                              Container(
                                  height: 1,
                                  color: Colors.black12
                              ),
                              Container(
                                  child: ListView.builder(
                                      shrinkWrap: true,
                                      physics: const NeverScrollableScrollPhysics(),
                                      scrollDirection: Axis.vertical,
                                      itemCount: comments.length,
                                      itemBuilder: (BuildContext context, int index) => GestureDetector(
                                          onTap: () => {
                                            //Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => ScrapListScreen(scrapFolders[index].folderID)))
                                          },
                                          child: commentContainer(
                                            Column(
                                              children: <Widget>[
                                                ExpandablePanel(
                                                  header: Container(
                                                    height: 50,
                                                    padding: EdgeInsets.only(left: 12),
                                                    child: Align(
                                                      alignment: Alignment.centerLeft,
                                                      child: Row(
                                                        children: <Widget>[
                                                          GestureDetector(
                                                            onTap: (){
                                                              Navigator.push(context, MaterialPageRoute(builder: (context) => FollowerScreen(comments[index].userID)));
                                                            },
                                                            child: ProfileImage(
                                                              comments[index].profileURL,
                                                              width: 46,
                                                              height: 46,
                                                            ),
                                                          ),
                                                          Column(
                                                            mainAxisAlignment: MainAxisAlignment.center,
                                                            crossAxisAlignment: CrossAxisAlignment.start,
                                                            children: <Widget>[
                                                              Container(
                                                                padding: EdgeInsets.only(left: 8),
                                                                child: Text(comments[index].name, maxLines: 1, style: TextStyle(fontWeight: FontWeight.bold),),
                                                              ),
                                                              Container(
                                                                padding: EdgeInsets.only(left: 8),
                                                                child: starGrade(comments[index].grade),
                                                              ),
                                                            ],
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                  collapsed: Container(
                                                      padding: EdgeInsets.only(left: 66, right: 10),
                                                      child: Align(
                                                        alignment: Alignment.centerLeft,
                                                        child: Text(comments[index].contents, softWrap: true, maxLines: 3, overflow: TextOverflow.ellipsis,),
                                                      )
                                                  ),
                                                  expanded: Container(
                                                      padding: EdgeInsets.only(left: 66, right: 10),
                                                      child:  Align(
                                                        alignment: Alignment.centerLeft,
                                                        child: Text(comments[index].contents, softWrap: true,),
                                                      )
                                                  ),
                                                ),
                                                Container(
                                                    padding: EdgeInsets.only(left: 66, top: 12, right: 18),
                                                    child: Stack(
                                                      children: <Widget>[
                                                        Row(
                                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                          children: <Widget>[
                                                            Row(
                                                              children: <Widget>[
                                                                GestureDetector(
                                                                  onTap: (){
                                                                    voteUp(index);
                                                                  },
                                                                  child: Container(
                                                                    child: Row(
                                                                      children: <Widget>[
                                                                        Container(
                                                                          width: 12,
                                                                          height: 12,
                                                                          margin: EdgeInsets.only(right: 4),
                                                                          child: voteArrow(comments[index].goodCount, 0),
                                                                        ),
                                                                        Text('${comments[index].goodCount}', style: TextStyle(fontSize: 12)),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                ),
                                                                Container(
                                                                  width: 18,
                                                                ),
                                                                GestureDetector(
                                                                  onTap: (){
                                                                    voteDown(index);
                                                                  },
                                                                  child: Container(
                                                                    color: Colors.white,
                                                                    child: Row(
                                                                      children: <Widget>[
                                                                        Container(
                                                                          width: 12,
                                                                          height: 12,
                                                                          margin: EdgeInsets.only(right: 4),
                                                                          child: voteArrow(comments[index].badCount, 1),
                                                                        ),
                                                                        Text('${comments[index].badCount}', style: TextStyle(fontSize: 12)),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                ),
                                                                Container(
                                                                  width: 18,
                                                                ),
                                                                GestureDetector(
                                                                  onTap: (){
                                                                    Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => ReplyScreen(comments[index], book.isbn)));
                                                                  },
                                                                  child: Row(
                                                                    children: <Widget>[
                                                                      Container(
                                                                        width: 12,
                                                                        height: 12,
                                                                        margin: EdgeInsets.only(right: 4),
                                                                        child: Image.asset('assets/icon_comment_reply.png'),
                                                                      ),
                                                                      Text('${comments[index].recommentCount}', style: TextStyle(fontSize: 12)),
                                                                    ],
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                            Visibility(
                                                              visible: isMyComment(comments[index].userID),
                                                              child: Row(
                                                                children: <Widget>[
                                                                  GestureDetector(
                                                                    onTap:(){
                                                                      removeComment(comments[index].commentID);
                                                                    },
                                                                    child: Row(
                                                                      children: <Widget>[
                                                                        Container(
                                                                          width: 12,
                                                                          height: 12,
                                                                          margin: EdgeInsets.only(right: 4),
                                                                          child: Image.asset('assets/icon_trash.png'),
                                                                        ),
                                                                        Text('삭제', style: TextStyle(fontSize: 12)),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ],
                                                    )
                                                ),
                                              ],
                                            ),
                                          )
                                      )
                                  )
                              )
                            ],
                          ),
                        ],
                      ),

                    ),
                    Container(
                      height: 62,
                      color: Colors.black12,
                    ),
                  ],
                ),
              ),
              ),
            ],
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child:
              Container(
                color: Colors.red,
                child: buyButton(),
              ),
            ),
            Visibility(
              visible: isBuy,
              child: GestureDetector(
                onTap: (){
                  setState(() {
                    isBuy = false;
                  });
                },
                child: Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height,
                    color: Colors.black54,
                    child: Align(
                      alignment: Alignment.bottomCenter,
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        height: 130,
                        padding: EdgeInsets.symmetric(horizontal: 24),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(30),
                            topRight: Radius.circular(30),
                          ),
                        ),
                        child: Column(
                          children: <Widget>[
                            Container(
                              height: 12,
                            ),
                            Text(
                              '구매 가능 사이트',
                              style: TextStyle(
                                fontSize: 12,
                                color: Colors.black45,
                              ),
                            ),
                            GestureDetector(
                              onTap: () {
                                _launchURL(book.url_alladin);
                              },
                              child: Container(
                                width: MediaQuery.of(context).size.width,
                                height: 50,
                                margin: EdgeInsets.only(left: 12, right: 12, top: 24, bottom: 6),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(0),
                                  border: Border.all(
                                    width: 1,
                                    color: Colors.black12,
                                  ),
                                ),
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Container(
                                        padding: EdgeInsets.only(bottom: 6),
                                        child: Image.asset('assets/logo_aladin.png', width: 90),
                                      ),
                                      Container(width: 12),
                                      Text('알라딘에서 구매', style: TextStyle(fontWeight: FontWeight.bold)),
                                    ],
                                  ),
                                ),
                                )
                            ),
                          ],
                        ),
                      ),
                    )
                ),
              )
            ),
          ],
        )

      );
    } else {
      return Scaffold(
          key: _scaffoldKey,
          body: Column(children: <Widget>[
            Container(
              margin: EdgeInsets.fromLTRB(0, MediaQuery.of(context).padding.top, 0, 0),
              height: 40,
              child: Stack(
                children: <Widget>[
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Padding(
                      padding: EdgeInsets.fromLTRB(12, 8, 0, 0),
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerRight,
                    child: Padding(
                      padding: EdgeInsets.fromLTRB(0, 8, 12, 0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ],)

      );
    }
  }

  Widget star(int rate) {
    String starColor = 'assets/icon_star_color.png';
    String starGrey = 'assets/icon_star_grey.png';
    String starIcon = starGrey;

    if(rate <= starRate) {
      starIcon = starColor;
    } else {
      starIcon = starGrey;
    }

    return GestureDetector(
      onTap: () => {
        setState(() {
          starRate = rate;
          detectCommentTextChanging(commentController.text);
        })
      },
      child: Image.asset(starIcon, width: 24, height: 24),
    );
  }

  Widget starList(int rate) {

  }

  Widget bookmarkButton() {
    List<String> icons = ['assets/icon_bookmark_grey.png', 'assets/icon_bookmark_color.png'];
    int index = 0;

    if(!isExist) {
      index = 0;
    } else {
      index = 1;
    }

    return GestureDetector(
      onTap: (){
        if(!isExist) addBook(widget.isbn);
        else removeBook();
      },
      child: Container(
        width: 22,
        height: 22,
        color: Colors.white,
        margin: EdgeInsets.only(left: 12),
        child: Image.asset(icons[index]),
      ),
    );
  }

  Widget dividerContainer() {
    return Container(
      height: 10,
      color: Colors.black12,
    );
  }

  Widget dividerStar() {
    return Container(
      width: 8,
    );
  }

  Widget listContainer(Widget widget) {
    return Container(
      color: Colors.black12,
      child: Container(
        padding: EdgeInsets.only(left: 20, right: 20, bottom: 20),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(30),
        ),
        child: widget
      ),
    );
  }

  Widget commentContainer(Widget widget) {
    return Column(
      children: <Widget>[
        Container(
        color: Colors.black,
          child: Container(
              padding: EdgeInsets.only(bottom: 20),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(0),
              ),
              child: widget
          ),
        ),
      ],
    );
  }

  Widget sendButton() {
    return GestureDetector(
      onTap: (){
        if(comment == 1) {
          if(myComment == null) {
            uploadComment();
          } else {
            editComment(myComment.commentID);
          }
        }
      },
      child: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment.center,
            child: Container(
              width: 40,
              height: 40,
              color: Colors.white,
            ),
          ),
          Align(
            alignment: Alignment.center,
            child: Container(
              width: 20,
              height: 20,
              child: Image.asset(sendButtonColors[comment]),
            ),
          ),
        ],
      )
    );
  }

  Widget voteArrow(int vote, int arrow) {
    // arrow: up(0), down(1)
    int v = vote;

    List<String> arrowGrey = ['assets/icon_like_up_grey.png', 'assets/icon_like_down_grey.png'];
    List<String> arrowColor = ['assets/icon_like_up_color.png', 'assets/icon_like_down_color.png'];

    if(v == null) {
      v = 0;
    }

    if(vote > 0) {
      return Image.asset(arrowColor[arrow]);
    } else {
      return Image.asset(arrowGrey[arrow]);
    }
  }

  Widget buyButton() {
    return GestureDetector(
      onTap: (){
        setState(() {
          isBuy = true;
        });
      },
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: 50,
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.centerLeft,
                end: Alignment.centerRight,
                colors: [Color(COLOR_PINK), Color(COLOR_YELLOW)]
            )
        ),
        child: Align(
          alignment: Alignment.center,
          child: Text('구매하기', style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white)),
        ),
      ),
    );
  }

  void voteUp(int index) {
    int vote = comments[index].vote;

    if(vote == 0 || vote == -1) {
      doVote(comments[index].commentID, 1);
    } else {
      cancelVote(comments[index].commentID);
    }
  }

  void voteDown(int index) {
    int vote = comments[index].vote;

    if(vote == 0 || vote == 1) {
      doVote(comments[index].commentID, -1);
    } else {
      cancelVote(comments[index].commentID);
    }
  }

  Widget miniStar(int rate, int bookRate) {
    String starColor = 'assets/icon_star_color.png';
    String starGrey = 'assets/icon_star_grey.png';
    String starIcon = starGrey;

    if(rate <= bookRate) {
      starIcon = starColor;
    } else {
      starIcon = starGrey;
    }

    return GestureDetector(
      child: Image.asset(starIcon, width: 14, height: 14),
    );
  }

  Widget starGrade(int bookRate) {
    double dividerWidth = 2;
    Log.d('BookInfoScreen - starGrade', '$bookRate');
    return Container(
      child: Row(
        children: <Widget>[
          miniStar(1, bookRate),
          Container(width: dividerWidth),
          miniStar(2, bookRate),
          Container(width: dividerWidth),
          miniStar(3, bookRate),
          Container(width: dividerWidth),
          miniStar(4, bookRate),
          Container(width: dividerWidth),
          miniStar(5, bookRate),
        ],
      ),
    );
  }

  Widget listReader() {
    return Container(
      child: ListView.builder(
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          itemCount: readers.length,
          itemBuilder: (BuildContext context, int index) => GestureDetector(
              onTap: () => {
                //Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => ScrapListScreen(scrapFolders[index].folderID)))
              },
              child: Column(
                children: <Widget>[
                  ProfileImage(
                    readers[index].profileURL,
                    width: 46,
                    height: 46,
                  ),
                  Text(readers[index].name, style: TextStyle(fontSize: 12))
                ],
              ),
          )
      ),
    );
  }

  @override
  void initState() {
    userManager = UserManager();

    Log.d('BookInfoScreen - initState', 'bookID = $bookID');

    if(widget.bookID == null) {
      bookID = widget.bookID;
    }

    loadBook(widget.isbn);
    loadComment(widget.isbn);
    loadMyComment(widget.isbn);
    loadReader(widget.isbn);
    loadGrade(widget.isbn);
    checkExistBook(widget.isbn);
  }
}
