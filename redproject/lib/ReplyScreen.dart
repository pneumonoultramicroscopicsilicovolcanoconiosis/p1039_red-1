import 'package:flutter/material.dart';
import 'package:redproject/pack.dart';
import 'package:redproject/DTO/Comment.dart';
import 'package:expandable/expandable.dart';
import 'package:http/http.dart' as http;

class ReplyScreen extends StatefulWidget {

  Comment comment;
  String isbn;

  ReplyScreen(this.comment, this.isbn);

  @override
  _ReplyScreenState createState() => _ReplyScreenState();
}

class _ReplyScreenState extends State<ReplyScreen> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final commentController = TextEditingController();

  UserManager userManager;

  List<Comment> replies = List();
  int reply = 0;
  bool isChangeMode = false;
  Comment changeReply;

  @override
  void initState() {
    userManager = UserManager();
    loadReply(widget.comment.commentID);
    Log.d('ReplyScreen - initState', userManager.profileURL);
  }

  Future loadReply(String commentID) async {
    String token = await TokenManager().getToken();

    var url = Uri.https('api.takebook.org', 'Account/ReComment', {'comment_id' : '$commentID'});
    var response = await http.get(url, headers: {'Authorization' : '$token'});

    ResultManager resultManager = ResultManager(response);
    Log.d('ReplyScreen - loadReply', response.body);
    Log.d('ReplyScreen - loadReply', resultManager.getFullMessage());

    if(!resultManager.isSuccess()) {
      // 실패
      MessageSnack.show(_scaffoldKey, '답글을 불러오는데 오류가 발생했습니다.');
      return;
    }

    Map<String, dynamic> map = resultManager.getResult();
    var item = map['item'] as List;

    setState(() {
      replies = item.map<Comment>((json) => Comment.fromJson(json)).toList();
    });
    Log.d('ReplyScreen - loadReply', 'comments.length = ${replies.length}');
  }

  Future uploadComment() async {

    if(isChangeMode) {
      editComment(changeReply.commentID);
      return;
    }

    String token = await TokenManager().getToken();

    var url = Uri.parse("https://api.takebook.org/Account/Comment");
    var response = await http.post(url, headers: {'Authorization' : '$token'},
        body: {'isbn' : '${widget.isbn}', 'contents' : '${commentController.text}', 'upper_comment' : '${widget.comment.commentID}'});

    ResultManager resultManager = ResultManager(response);

    if(!resultManager.isSuccess()) {
      // 실패
      MessageSnack.show(_scaffoldKey, '답글을 등록하는데 오류가 발생했습니다.');
      return;
    }

    if(resultManager.getResultCode()[0] == 'RS001') {
      MessageSnack.show(_scaffoldKey, '이미 댓글을 작성하셨습니다. 답글만 입력 가능합니다.');
      return;
    }

    loadReply(widget.comment.commentID);
    setState(() {
      commentController.text = '';
      reply = 0;
    });
  }

  Future editComment(String commentID) async {
    String token = await TokenManager().getToken();

    var url = 'https://api.takebook.org/Account/Comment';
    var map = {'comment_id' : '$commentID', 'contents' : '${commentController.text}'};

    HttpClient httpClient = HttpClient();
    HttpClientRequest request = await httpClient.putUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    request.headers.add('Authorization', '$token');
    request.add(utf8.encode(json.encode(map)));

    HttpClientResponse response = await request.close();
    String result = await response.transform(utf8.decoder).join();

    if(response.statusCode != 200) {
      return;
    }

    httpClient.close();
    
    loadReply(widget.comment.commentID);

    MessageSnack.show(_scaffoldKey, '댓글을 수정했습니다.');
    commentController.text = '';
    setState(() {
      isChangeMode = false;
    });
    changeReply = null;
  }

  void detectCommentTextChanging(String text) {
    setState(() {
      if(text.length > 0 && text != '') {
        reply = 1;
      } else {
        reply = 0;
      }
    });
  }

  Future removeComment(String commentID) async {
    String token = await TokenManager().getToken();

    var url = 'https://api.takebook.org/Account/Comment';
    var params = {'comment_id' : '$commentID'};

    HttpClient httpClient = HttpClient();
    HttpClientRequest request = await httpClient.deleteUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    request.headers.add('Authorization', '$token');
    request.add(utf8.encode(json.encode(params)));

    HttpClientResponse response = await request.close();

    if(response.statusCode != 200) {
      return;
    }
    httpClient.close();

    String result = await response.transform(utf8.decoder).join();
    Map<String, dynamic> map = json.decode(result);
    String resultCode = map['Result_Code'];
    String resultMessage = map['Message'];
    String msg = '$resultCode :::: $resultMessage';

    Log.d('UserInfoScreen - cancelFollow', '$msg');

    if(ERROR_CODES.contains(resultCode)) {
      // 실패
      MessageSnack.show(_scaffoldKey, '댓글을 삭제하는데 오류가 발생했습니다.');
      return;
    }

    MessageSnack.show(_scaffoldKey, '댓글을 삭제했습니다.');

    loadReply(widget.comment.commentID);
    //loadComment(widget.isbn);
  }

  bool isMyComment(String writer) {
    if(isChangeMode) {
      return false;
    }
    UserManager userManager = UserManager();
    Log.d('BookInfoScreen', 'user = ${userManager.userID}, writer = $writer');

    if(userManager.userID == writer) return true;
    else return false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      resizeToAvoidBottomPadding: false,
      body: Column(
        children: <Widget>[
          Stack(
            children: <Widget>[
              titleBar(),
              Visibility(
                visible: isChangeMode,
                child: Container(
                  height: 50,
                  margin: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
                  color: Colors.white,
                  child: Stack(
                    children: <Widget>[
                      Align(
                        alignment: Alignment.centerRight,
                        child: Container(
                          margin: EdgeInsets.only(right: 12),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              GestureDetector(
                                onTap: (){
                                  setState(() {
                                    isChangeMode = false;
                                    commentController.text = '';
                                  });
                                },
                                child: Row(
                                  children: <Widget>[
                                    Container(
                                      width: 16,
                                      height: 16,
                                      margin: EdgeInsets.only(right: 8),
                                      child: Image.asset('assets/icon_close.png'),
                                    ),
                                    Text('수정 취소', style: TextStyle(fontSize: 14)),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
          Container(
            padding: EdgeInsets.all(12),
            child: Row(
              children: <Widget>[
                ProfileImage(
                  widget.comment.profileURL,
                  width: 46,
                  height: 46,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(left: 8),
                      child: Text(widget.comment.name, maxLines: 1, style: TextStyle(fontWeight: FontWeight.bold),),
                    ),
                    Container(
                      padding: EdgeInsets.only(left: 8),
                      child: starGrade(widget.comment.grade),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Container(
            color: Colors.black12,
            padding: EdgeInsets.all(12),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                ProfileImage(
                  userManager.profileURL,
                  width: 46,
                  height: 46,
                ),
                Flexible(
                  child: TextField(
                    controller: commentController,
                    style: TextStyle(fontSize: 14),
                    keyboardType: TextInputType.multiline,
                    maxLines: null,
                    onChanged: (text){
                      detectCommentTextChanging(text);
                    },
                    decoration: InputDecoration(
                      hintText: '답글 쓰기',
                      contentPadding: EdgeInsets.all(10),
                      border: InputBorder.none,
                    ),
                  ),
                ),
                Container(
                  child: sendButton(),
                ),
              ],
            ),
          ),
          Expanded(
            child: Container(
                padding: EdgeInsets.all(12),
                child: ListView.builder(
                    scrollDirection: Axis.vertical,
                    itemCount: replies.length,
                    itemBuilder: (BuildContext context, int index) => GestureDetector(
                        onTap: () => {
                          //Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => ScrapListScreen(scrapFolders[index].folderID)))
                        },
                        child: commentContainer(
                          Column(
                            children: <Widget>[
                              ExpandablePanel(
                                header: Container(
                                  height: 50,
                                  padding: EdgeInsets.only(left: 0),
                                  child: Align(
                                    alignment: Alignment.centerLeft,
                                    child: Row(
                                      children: <Widget>[
                                        ProfileImage(
                                          replies[index].profileURL,
                                          width: 46,
                                          height: 46,
                                        ),
                                        Column(
                                          children: <Widget>[
                                            Container(
                                              padding: EdgeInsets.only(left: 8),
                                              child: Text(replies[index].name, maxLines: 1, style: TextStyle(fontWeight: FontWeight.bold),),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                collapsed: Container(
                                    padding: EdgeInsets.only(left: 52, right: 10),
                                    child: Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text(replies[index].contents, softWrap: true, maxLines: 3, overflow: TextOverflow.ellipsis,),
                                    )
                                ),
                                expanded: Container(
                                    padding: EdgeInsets.only(left: 52, right: 10),
                                    child:  Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text(replies[index].contents, softWrap: true,),
                                    )
                                ),
                              ),
                              Visibility(
                                  visible: isMyComment(replies[index].userID),
                                  child: Align(
                                    alignment: Alignment.centerRight,
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: <Widget>[
                                        GestureDetector(
                                          onTap: (){
                                            setState(() {
                                              isChangeMode = true;
                                            });
                                            changeReply = replies[index];
                                            commentController.text = replies[index].contents;
                                          },
                                          child: Row(
                                            children: <Widget>[
                                              Container(
                                                width: 12,
                                                height: 12,
                                                margin: EdgeInsets.only(right: 4),
                                                child: Image.asset('assets/icon_edit.png'),
                                              ),
                                              Text('수정', style: TextStyle(fontSize: 12)),
                                            ],
                                          ),
                                        ),
                                        Container(
                                          width: 18,
                                        ),
                                        GestureDetector(
                                          onTap:(){
                                            removeComment(replies[index].commentID);
                                          },
                                          child: Row(
                                            children: <Widget>[
                                              Container(
                                                width: 12,
                                                height: 12,
                                                margin: EdgeInsets.only(right: 4),
                                                child: Image.asset('assets/icon_trash.png'),
                                              ),
                                              Text('삭제', style: TextStyle(fontSize: 12)),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                              ),
                            ],
                          ),
                        )
                    )
                )
            ),
          ),
        ],
      )
    );
  }

  Widget titleBar() {
    return Container(
      margin: EdgeInsets.fromLTRB(0, MediaQuery.of(context).padding.top, 0, 0),
      height: 50,
      child: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment.centerLeft,
            child: GestureDetector(
              onTap: (){
                Navigator.of(context).pop();
              },
              child: Container(
                width: 18,
                height: 18,
                margin: EdgeInsets.only(left: 12),
                child: Image.asset('assets/icon_back.png'),
              ),
            ),
          ),
          Align(
              alignment: Alignment.center,
              child:
              Text('답글 보기')
          ),
          Align(
            alignment: Alignment.centerRight,
            child: Padding(
              padding: EdgeInsets.fromLTRB(0, 8, 12, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[

                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget commentContainer(Widget widget) {
    return Column(
      children: <Widget>[
        Container(
          color: Colors.black,
          child: Container(
              padding: EdgeInsets.only(bottom: 20),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(0),
              ),
              child: widget
          ),
        ),
      ],
    );
  }

  Widget miniStar(int rate, int bookRate) {
    String starColor = 'assets/icon_star_color.png';
    String starGrey = 'assets/icon_star_grey.png';
    String starIcon = starGrey;

    if(rate <= bookRate) {
      starIcon = starColor;
    } else {
      starIcon = starGrey;
    }

    return GestureDetector(
      child: Image.asset(starIcon, width: 14, height: 14),
    );
  }

  Widget starGrade(int bookRate) {
    double dividerWidth = 2;
    Log.d('BookInfoScreen - starGrade', '$bookRate');
    return Container(
      child: Row(
        children: <Widget>[
          miniStar(1, bookRate),
          Container(width: dividerWidth),
          miniStar(2, bookRate),
          Container(width: dividerWidth),
          miniStar(3, bookRate),
          Container(width: dividerWidth),
          miniStar(4, bookRate),
          Container(width: dividerWidth),
          miniStar(5, bookRate),
        ],
      ),
    );
  }

  Widget sendButton() {
    List<String> sendButtonColors = [
      'assets/icon_comment_send_grey.png',
      'assets/icon_comment_send_color.png'
    ];

    return GestureDetector(
        onTap: () {
          if (reply == 1) uploadComment();
        },
        child: Stack(
          alignment: AlignmentDirectional.center,
          children: <Widget>[
            Align(
              alignment: Alignment.center,
              child: Container(
                width: 40,
                height: 40,
                color: Colors.transparent,
              ),
            ),
            Align(
              alignment: Alignment.center,
              child: Container(
                width: 20,
                height: 20,
                child: Image.asset(sendButtonColors[reply]),
              ),
            ),
          ],
        )
    );
  }
}
