import 'package:flutter/material.dart';
import 'package:redproject/HomeScreen.dart';
import 'package:redproject/RegisterScreen.dart';
import 'package:redproject/Components/GradientButton.dart';
import 'package:redproject/Constant.dart';
import 'package:redproject/TokenManager.dart';
import 'package:redproject/ResultManager.dart';
import 'package:redproject/MessageSnack.dart';
import 'package:http/http.dart' as http;
import 'package:crypto/crypto.dart';
import 'package:redproject/Log.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {

  final idController = TextEditingController();
  final pwController = TextEditingController();

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: Container(
          alignment: Alignment.center,
          child: Container(
            width: 300,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  width: 120,
                  margin: EdgeInsets.only(bottom: 48),
                  child: Image.asset('assets/icon_title.png'),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Flexible(
                      child: TextField(
                        controller: idController,
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(14),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(0),
                            borderSide: BorderSide(color: Colors.grey, width: 0.0)
                          )
                        ),
                      ),
                    )
                  ],
                ),
                Container(
                  height: 8,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Flexible(
                      child: TextField(
                        controller: pwController,
                        obscureText: true,
                        decoration: InputDecoration(
                            contentPadding: EdgeInsets.all(14),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(0),
                                borderSide: BorderSide(color: Colors.grey, width: 0.0)
                            )
                        ),
                      ),
                    )
                  ],
                ),
                Container(
                  height: 8,
                ),
                GradientButton(
                  child: Text('로그인', style: TextStyle(
                    color: Colors.white
                  ),),
                  gradient: LinearGradient(
                      begin: Alignment.centerLeft,
                      end: Alignment.centerRight,
                      colors: [Color(COLOR_YELLOW), Color(COLOR_PINK)]
                  ),
                  onPressed: () => {
                    login(idController.text, pwController.text)
                  },
                ),
                Container(
                  height: 8,
                ),
                GradientButton(
                  child: Text('회원가입', style: TextStyle(
                      color: Colors.white
                  ),),
                  gradient: LinearGradient(
                      begin: Alignment.centerLeft,
                      end: Alignment.centerRight,
                      colors: [Color(COLOR_YELLOW), Color(COLOR_PINK)]
                  ),
                  onPressed: () async {
                      await Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => RegisterScreen()));
                      MessageSnack.show(_scaffoldKey, '회원가입이 완료되었습니다.');
                    },
                ),
                Container(
                  height: 8,
                ),
              ],
            ),
          )),
    );
  }


  @override
  void initState() {
    getToken();
  }

  login(String id, String pw) async {

    if(id == null || id == '' || pw == null || pw == '') {
      MessageSnack.show(_scaffoldKey, '공백이 있습니다.');
      return;
    }

    var url = 'https://api.takebook.org/Account/UserLogin';
    var bytes = utf8.encode(pw);
    var digest = sha256.convert(bytes);

    var response = await http.post(url, body: {'user_id' : '$id', "user_password" : '$digest'});

    ResultManager resultManager = ResultManager(response);
    String resultCode = resultManager.getResultCode()[0];
    Log.d('LoginScreen - login', response.body);

    if(!resultManager.isSuccess()) {
      // 실패

      if(resultCode == 'EC001') {
        MessageSnack.show(_scaffoldKey, '공백이 있습니다.');
      }

      return;
    }

    if(resultCode == 'RS001') {
      MessageSnack.show(_scaffoldKey, '아이디 또는 비밀번호를 확인해주세요.');
      return;
    }

    Map<String, dynamic> map = resultManager.getResult();
    Log.d('LoginScreen - login', map.toString());

    String userToken = map['user_token'];

    saveToken(userToken);
    Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (BuildContext context) => HomeScreen()));

    /*
    if(resultCode == 'RS000') {
      userToken = map['Response']['user_token'];
      saveToken(userToken);
      Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (BuildContext context) => HomeScreen()));
    } else if(resultCode == 'RS001') {
      final snackBar = SnackBar(content: Text('유효하지 않은 사용자'));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    } else if(resultCode == 'EC001') {
      final snackBar = SnackBar(content: Text('공백이 있음'));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }

    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');
    */
  }

  getToken() async {
    TokenManager tokenManager = TokenManager();

    String token = await tokenManager.getToken();

    if(token != null && token != '') {
      Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (BuildContext context) => HomeScreen()));
    }
  }

  saveToken(String token) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('userToken', token);
  }

  showMessage() {

  }
}

class Result {
  String resultCode;
  String message;
  String userToken;
  String userId;
  String userName;
  String userSignUpDate;
  String userProfileUrl;
  String userUpdateDate;

  Result({
    this.resultCode,
    this.message,
    this.userToken,
    this.userId,
    this.userName,
    this.userSignUpDate,
    this.userProfileUrl,
    this.userUpdateDate
  });

  Result.fromJson(Map<String, dynamic> json) {
    resultCode = json['Result_Code'];
    message = json['Message'];
    print(message);

    if(resultCode == 'RS000') {
      userToken = json['Response']['user_token'];
      print(userToken);
    } else if(resultCode == 'S001') {

    }
  }
}