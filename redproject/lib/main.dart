import 'package:camera/camera.dart';
import 'dart:async';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import 'package:flutter/material.dart';
import 'package:redproject/Constant.dart';
import 'package:redproject/CameraHomeScreen.dart';
import 'package:redproject/HomeScreen.dart';
import 'package:redproject/SplashScreen.dart';
import 'package:redproject/LoginScreen.dart';
import 'package:redproject/lab/CenterSliceLab.dart';

List<CameraDescription> cameras;

Future<void> main() async {

  try {
    FlutterStatusbarcolor.setStatusBarWhiteForeground(false);
  }  catch (e) {
    print(e);
  }

  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  @override
  void initState() {
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: '책을찍다',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.purple,
        primaryColor: Color(PRIMARY_COLOR),
        accentColor: Color(ACCENT_COLOR),
        canvasColor: Color(CANVAS_COLOR),
        primaryIconTheme: Theme.of(context).primaryIconTheme.copyWith(
          color: Colors.black
        ),
        primaryTextTheme: Theme.of(context).primaryTextTheme.apply(bodyColor: Colors.black)
      ),
      home: SplashScreen(),
    );
  }
}
