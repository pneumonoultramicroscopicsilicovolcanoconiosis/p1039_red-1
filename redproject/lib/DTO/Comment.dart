class Comment {
  String commentID;
  String userID;
  String registrationDate;
  String contents;
  int goodCount;
  int badCount;
  int recommentCount;
  int vote;
  int grade;
  String profileURL;
  String name;

  Comment({
    this.commentID,
    this.userID,
    this.registrationDate,
    this.contents,
    this.goodCount,
    this.badCount,
    this.recommentCount,
    this.vote,
    this.grade,
    this.profileURL,
    this.name
  });

  Comment.fromJson(Map<String, dynamic> json) {
    this.commentID = json['comment_id'];
    this.userID = json['user_id'];
    this.registrationDate = json['registration_date'];
    this.contents = json['contents'];
    this.goodCount = json['good_cnt'];
    this.badCount = json['bad_cnt'];
    this.recommentCount = json['recomment_cnt'];
    this.vote = json['vote'];
    this.grade = json['grade'];
    this.profileURL = json['profile_url'];
    this.name = json['name'];

    if(grade == null) grade = 0;
    if(vote == null) vote = 0;
    if(goodCount == null) goodCount = 0;
    if(badCount == null) badCount = 0;
  }
}