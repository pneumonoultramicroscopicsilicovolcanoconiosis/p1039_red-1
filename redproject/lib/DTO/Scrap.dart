class Scrap {
  String scrapId;
  String creationDate;
  String contents;
  String source;
  String folder;
  String imageURL;
  bool isSelected = false;

  Scrap({
    this.scrapId,
    this.creationDate,
    this.contents,
    this.source,
    this.folder,
    this.imageURL,
    this.isSelected = false
  });

  Scrap.fromJson(Map<String, dynamic> json) {
    this.scrapId = json['scrap_id'];
    this.creationDate = json['creation_date'];
    this.contents = json['contents'];
    this.source = json['source_title'];
    this.folder = json['folder'];
    this.imageURL = json['image_url'];

    if(this.contents == null) {
      this.contents = '정보 없음';
    }

    if(this.source == null) {
      this.source = '정보 없음';
    }
  }
}