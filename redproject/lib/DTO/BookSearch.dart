class ScrapSearch {
  String title;
  String isbn;
  String author;
  String publisher;
  String imageURL;

  ScrapSearch.fromJson(Map<String, dynamic> json) {
    this.title = json['title'];
    this.isbn = json['isbn'];
    this.author = json['author'];
    this.publisher = json['publisher'];
    this.imageURL = json['image_url'];
  }
}