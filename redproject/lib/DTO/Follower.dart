class Follower {
  String userID;
  String name;
  String stateMessage;
  String profileURL;
  int isFollow;

  Follower(
  {
    this.userID,
    this.name,
    this.stateMessage,
    this.profileURL,
    this.isFollow
  });

  Follower.fromJson(Map<String, dynamic> json) {
    this.userID = json['user_id'];
    this.name = json['name'];
    this.stateMessage = json['state_message'];
    this.profileURL = json['profile_url'];
    this.isFollow = json['is_follow'];

    if(stateMessage == null) {
      stateMessage = '';
    }
  }
}