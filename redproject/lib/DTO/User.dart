class User {
  String userID;
  String name;
  String profileURL;
  String stateMessage;
  int followerCount;
  int followeeCount;
  int isFollow;

  User.fromJson(Map<String, dynamic> json) {
    this.userID = json['user_id'];
    this.name = json['name'];
    this.profileURL = json['profile_url'];
    this.stateMessage = json['state_message'];
    this.followerCount = json['follower_cnt'];
    this.followeeCount = json['followed_cnt'];
    this.isFollow = json['isfollow'];

    if(stateMessage == null) stateMessage = '';
  }
}
