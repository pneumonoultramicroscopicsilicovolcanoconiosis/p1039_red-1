class BookCandidate {
  String isbn;
  String title;
  String publisher;
  String author;
  String imageURL;

  BookCandidate({
    this.isbn,
    this.title,
    this.publisher,
    this.author,
    this.imageURL
  });

  BookCandidate.fromJson(Map<String, dynamic> json) {
    this.isbn = json['isbn'];
    this.title = json['title'];
    this.publisher = json['publisher'];
    this.author = json['author'];
    this.imageURL = json['image_url'];
  }
}
