class ScrapFolder {
  String folderID;
  String folderTitle;
  String createDate;
  String updateDate;
  bool isSelected = false;
  int count;

  ScrapFolder({
    this.folderID,
    this.folderTitle,
    this.createDate,
    this.updateDate,
    this.isSelected = false
  });

  ScrapFolder.fromJson(Map<String, dynamic> json) {
    this.folderID = json['folder_id'];
    this.folderTitle = json['name'];
    this.createDate = json['creation_date'];
    this.updateDate = json['update_date'];
    this.count = json['scrap']['count'];

  }
}