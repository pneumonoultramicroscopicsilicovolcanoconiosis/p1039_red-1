class BookCover {
  String title;
  String author;
  String publisher;
  String image_url;
  String book_id;
  String registration_date;
  String isbn;
  String second_candidate;
  String third_candidate;
  String fourth_candidate;
  String fifth_candidate;
  bool isSelected = false;

  BookCover({
    this.title,
    this.author,
    this.publisher,
    this.image_url,
    this.book_id,
    this.registration_date,
    this.isbn,
    this.second_candidate,
    this.third_candidate,
    this.fourth_candidate,
    this.fifth_candidate,
    this.isSelected = false
  });

  BookCover.fromJson(Map<String, dynamic> json) {
    this.title = json['title'];
    this.author = json['author'];
    this.publisher = json['publisher'];
    this.image_url = json['image_url'];
    this.book_id = json['book_id'];
    this.registration_date = json['registration_date'];
    this.isbn = json['isbn'];
    this.second_candidate = json['second_candidate'];
    this.third_candidate = json['third_candidate'];
    this.fourth_candidate = json['fourth_candidate'];
    this.fifth_candidate = json['fifth_candidate'];

    if(this.title == null) this.title = '--';
    if(this.author == null) this.author = '--';
    if(this.publisher == null) this.publisher = '--';
  }
}