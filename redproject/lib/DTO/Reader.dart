class Reader {
  String userID;
  String profileURL;
  String name;

  Reader({
    this.userID,
    this.profileURL,
    this.name
  });

  Reader.fromJson(Map<String, dynamic> json) {
    this.userID = json['user_id'];
    this.profileURL = json['profile_url'];
    this.name = json['name'];

    if(userID == null) userID = '';
    if(name == null) name = '이름 없음';
  }
}