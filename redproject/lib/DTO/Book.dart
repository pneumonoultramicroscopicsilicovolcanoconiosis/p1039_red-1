class Book {
  String isbn;
  String title;
  String published_date;
  String author;
  String translator;
  String publisher;
  String url_alladin;
  String image_url;
  String contents;
  String discriptions;
  String result;
  String bookID;
  int price;

  Book(String title, String image_url) {
    this.title = title;
    this.image_url = image_url;
  }

  Book.fromJson(Map<String, dynamic> json) {
    this.isbn = json['isbn'];
    this.title = json['title'];
    this.published_date = json['published_date'];
    this.author = json['author'];
    this.translator = json['translator'];
    this.publisher = json['publisher'];
    this.url_alladin = json['alladin_url'];
    this.image_url = json['image_url'];
    this.contents = json['contents'];
    this.discriptions = json['discriptions'];
    this.bookID = json['book_id'];
    this.price = json['price'];

    if(this.title == null) this.title = '제목 정보 없음';
    if(this.author == null) this.author = '지은이 정보 없음';
    if(this.translator == null) this.translator = '';
    if(this.publisher == null || this.publisher == '') this.publisher = '출판사 정보 없음';
    if(this.contents == null || this.contents == '') this.contents = '내용 정보 없음';
    if(this.discriptions == null || this.discriptions == '') this.discriptions = '목차 정보 없음';
  }
}
