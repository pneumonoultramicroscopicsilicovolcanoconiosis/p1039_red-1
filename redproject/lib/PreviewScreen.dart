import 'package:flutter/material.dart';
import 'package:redproject/BookinfoScreen.dart';
import 'package:redproject/DTO/Book.dart';
import 'package:redproject/pack.dart';
import 'package:image_crop/image_crop.dart';
import 'package:http/http.dart' as http;
import 'package:async/async.dart';
import 'dart:convert';
import 'dart:io';
import 'dart:developer' as developer;

class PreviewScreen extends StatefulWidget {

  File imageFile;

  PreviewScreen(this.imageFile);

  @override
  State<StatefulWidget> createState() {
    return _PreviewPageState();
  }
}

class _PreviewPageState extends State<PreviewScreen> {
  final cropKey = GlobalKey<CropState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  List<String> labels = ['사진을 전송하는데 문제가 발생했습니다.\n다시 촬영해주세요.', '사진 전송에 성공했습니다.'];
  bool boolVisibility = false;
  bool isFinished = false;
  int success = 0;

  @override
  void initState() {
    uploadImage();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: Column(
        children: <Widget>[
          titleBar(),
          Expanded(
            child: Container(
              width: MediaQuery.of(context).size.width,
              color: Colors.black,
              child: Image.file(widget.imageFile, fit: BoxFit.fitHeight,),
            ),
          ),
          Stack(
            children: <Widget>[
              Container(
                height: 100,
                child: Align(
                  alignment: Alignment.center,
                  child: Text('사진을 전송중입니다...', style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)
                  ),
                ),
              ),
              Visibility(
                visible: isFinished,
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: 100,
                  color: Colors.white,
                  child: Align(
                    alignment: Alignment.center,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text(labels[success], style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
                        GestureDetector(
                          onTap: (){
                            Navigator.pop(context);
                          },
                          child: Container(
                            width: 200,
                            padding: EdgeInsets.all(24),
                            color: Colors.white,
                            child: Align(
                              alignment: Alignment.center,
                              child: Text('확인', style: TextStyle(fontWeight: FontWeight.bold)),
                            )
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget titleBar() {
    return Container(
      color: Colors.white,
      margin: EdgeInsets.fromLTRB(0, MediaQuery
          .of(context)
          .padding
          .top, 0, 0),
      height: 50,
      child: Stack(
        children: <Widget>[
          Align(
              alignment: Alignment.center,
              child:
              Text('사진 전송')
          ),
        ],
      ),
    );
  }

  void uploadImage() async {

    var stream = new http.ByteStream(DelegatingStream.typed(widget.imageFile.openRead()));
    var length = await widget.imageFile.length();

    var uri = Uri.parse("https://api.takebook.org/Account/AnalyzeBookImage");
    var request = new http.MultipartRequest("POST", uri);

    var multipartFile = new http.MultipartFile('book_image', stream, length,
        filename: widget.imageFile.path.split('/').last);

    TokenManager tokenManager = TokenManager();
    String token = await tokenManager.getToken();

    request.files.add(multipartFile);
    request.headers['Authorization'] = token;

    var response = await request.send();

    response.stream.transform(utf8.decoder).listen((value) {
      Map<String, dynamic> map = json.decode(value);

      setState(() {
        isFinished = true;
      });

      Log.d('PreviewScreen - uploadImage', value);

      String errorCode = map['Result_Code'];
      Log.d('PreviewScreen - uploadImage', 'errorCode = $errorCode');
      if(errorCode != 'RS000') {
        setState(() {
          success = 0;
        });
      } else {
        setState(() {
          success = 1;
        });
      }

    });
  }
}

class Result {
  String isbn;
  String fileName;

  Result({this.isbn, this.fileName});

  Result.fromJson(Map<String, dynamic> json) {
    isbn = json['isbn'];
    fileName = json['file_name'];
  }
}
