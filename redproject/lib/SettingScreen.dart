import 'package:flutter/material.dart';
import 'package:redproject/pack.dart';
import 'package:redproject/Components/CheckCircle.dart';
import 'package:path_provider/path_provider.dart';
import 'package:http/http.dart';
import 'dart:async';
import 'dart:io';

class SettingScreen extends StatefulWidget {
  @override
  _SettingScreenState createState() => _SettingScreenState();
}

class _SettingScreenState extends State<SettingScreen> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  List<String> paramBooks = ['title', 'author', 'publisher', 'registration_date'];
  List<String> paramScraps = ['name', 'creation_date', 'update_date'];
  List<String> orders = ['asc', 'desc'];
  List<String> koBooks = ['제목', '저자/역자', '출판사', '등록일'];
  List<String> koScraps = ['이름', '생성일', '수정일'];
  List<String> koOrders = ['오름차순', '내림차순'];

  List<bool> selectedSortBook = [false, false, false, true];
  List<bool> selectedSortScrap = [true, false, false];
  List<bool> selectedOrderBook = [false, true];
  List<bool> selectedOrderScrap = [false, true];
  List<int> sortBookIndexes = [0, 0];                         // index 0: 책 정렬 방법,    index 1: 오름차순 또는 내림차순
  List<int> sortScrapIndexes = [0, 0];                        // index 0: 스크랩 정렬 방법, index 1: 오름차순 또는 내림차순

  bool isBookDialogVisible = false;
  bool isScrapDialogVisible = false;

  int fileCount = 0;

  @override
  void initState() {
    loadConfig();
    if(Platform.isIOS) {
      getFiles();
    }
  }

  void sortBookType(int index) {
    setState(() {
      selectedSortBook[0] = false;
      selectedSortBook[1] = false;
      selectedSortBook[2] = false;
      selectedSortBook[3] = false;
      selectedSortBook[index] = true;
    });

    sortBookIndexes[0] = index;
  }

  void sortScrapType(int index) {
    setState(() {
      selectedSortScrap[0] = false;
      selectedSortScrap[1] = false;
      selectedSortScrap[2] = false;
      selectedSortScrap[index] = true;
    });

    sortScrapIndexes[0] = index;
  }

  void orderBook(int index) {
    setState(() {
      selectedOrderBook[0] = false;
      selectedOrderBook[1] = false;
      selectedOrderBook[index] = true;
    });

    sortBookIndexes[1] = index;
  }

  void orderScrap(int index) {
    setState(() {
      selectedOrderScrap[0] = false;
      selectedOrderScrap[1] = false;
      selectedOrderScrap[index] = true;
    });

    sortScrapIndexes[1] = index;
  }

  Future loadConfig() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    sortBookIndexes[0] = prefs.getInt('sortBookIndex');
    sortBookIndexes[1] = prefs.getInt('orderBookIndex');
    sortScrapIndexes[0] = prefs.getInt('sortScrapIndex');
    sortScrapIndexes[1] = prefs.getInt('orderScrapIndex');

    if(sortBookIndexes[0] == null) sortBookIndexes[0] = 3;
    if(sortBookIndexes[1] == null) sortBookIndexes[1] = 1;
    if(sortScrapIndexes[0] == null) sortScrapIndexes[0] = 0;
    if(sortScrapIndexes[1] == null) sortScrapIndexes[1] = 0;

    Log.d('SettingScreen - loadConfig', 'sortBookIndexes[0] = ${sortBookIndexes[0]}');
    Log.d('SettingScreen - loadConfig', 'sortBookIndexes[1] = ${sortBookIndexes[1]}');

    sortBookType(sortBookIndexes[0]);
    orderBook(sortBookIndexes[1]);
    sortScrapType(sortScrapIndexes[0]);
    orderScrap(sortScrapIndexes[1]);
  }

  Future saveConfig() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setInt('sortBookIndex', sortBookIndexes[0]);
    await prefs.setInt('orderBookIndex', sortBookIndexes[1]);
    await prefs.setInt('sortScrapIndex', sortScrapIndexes[0]);
    await prefs.setInt('orderScrapIndex', sortScrapIndexes[1]);
    await prefs.setString('sortBook', paramBooks[sortBookIndexes[0]]);
    await prefs.setString('orderBook', orders[sortBookIndexes[1]]);
    await prefs.setString('sortScrap', paramScraps[sortScrapIndexes[0]]);
    await prefs.setString('orderScrap', orders[sortScrapIndexes[1]]);
  }

  Future getFiles() async {

    final Directory extDir = await getApplicationDocumentsDirectory();
    final String dirPath = '${extDir.path}/FlutterDevs/Camera/Images';
    final dir = Directory(dirPath);
    var files = dir.listSync().toList();

    dir.exists().then((isThere){
      setState(() {
        if(isThere) {
          fileCount = files.length;
        } else {
          fileCount = 0;
        }
      });
    });

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      body: Stack(
        children: <Widget>[
          Column(
            children: <Widget>[
              titleBar(),
              Column(
                children: <Widget>[
                  listContainer(
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 24),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text('책 목록 정렬', style: TextStyle(fontWeight: FontWeight.bold)),
                          Text('${koBooks[sortBookIndexes[0]]} / ${koOrders[sortBookIndexes[1]]}')
                        ],
                      ),
                    ),
                    onTap: (){
                      setState(() {
                        isBookDialogVisible = true;
                      });
                    },
                  ),
                  listContainer(
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 24),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text('스크랩 폴더 목록 정렬', style: TextStyle(fontWeight: FontWeight.bold)),
                          Text('${koScraps[sortScrapIndexes[0]]} / ${koOrders[sortScrapIndexes[1]]}')
                        ],
                      ),
                    ),
                    onTap: (){
                      setState(() {
                        isScrapDialogVisible = true;
                      });
                    },
                  ),
                  listContainer(
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 24),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text('버전 정보', style: TextStyle(fontWeight: FontWeight.bold)),
                          Text('0.9.2')
                        ],
                      ),
                    ),
                    onTap: (){

                    },
                  ),
                  Visibility(
                    visible: Platform.isIOS,
                    child: listContainer(
                      child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: 24),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text('파일 지우기', style: TextStyle(fontWeight: FontWeight.bold)),
                            Text('$fileCount개의 파일')
                          ],
                        ),
                      ),
                      onTap: () async {
                        final Directory extDir = await getApplicationDocumentsDirectory();
                        final String dirPath = '${extDir.path}/FlutterDevs/Camera/Images';
                        final dir = Directory(dirPath);

                        dir.exists().then((isThere){
                          if(isThere) {
                            dir.deleteSync(recursive: true);
                            getFiles();
                            MessageSnack.show(_scaffoldKey, '파일을 모두 삭제했습니다.');
                            setState(() {
                              fileCount = 0;
                            });
                          } else {
                            MessageSnack.show(_scaffoldKey, '삭제할 파일이 없습니다.');
                          }
                        });
                      },
                    ),
                  ),
                ],
              ),
            ],
          ),
          Visibility(
            visible: isBookDialogVisible,
            child: sortBookDialog(),
          ),
          Visibility(
            visible: isScrapDialogVisible,
            child: sortScrapDialog(),
          ),
        ],
      ),
    );
  }

  Widget titleBar() {
    return Container(
      color: Colors.white,
      margin: EdgeInsets.fromLTRB(0, MediaQuery
          .of(context)
          .padding
          .top, 0, 0),
      height: 50,
      child: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              height: 1,
              color: Colors.black12,
            ),
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: GestureDetector(
              onTap: () {
                Navigator.of(context).pop();
              },
              child: Container(
                width: 18,
                height: 18,
                color: Colors.white,
                margin: EdgeInsets.only(left: 12),
                child: Image.asset('assets/icon_back.png'),
              ),
            ),
          ),
          Align(
              alignment: Alignment.center,
              child:
              Text('설정')
          ),
        ],
      ),
    );
  }

  Widget listContainer({Widget child, GestureTapCallback onTap}) {

    return GestureDetector(
      onTap: onTap,
      child: Container(
        height: 60,
        color: Colors.transparent,
        child: Stack(
          children: <Widget>[
            Align(
              alignment: Alignment.center,
              child: child,
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                height: 1,
                color: Colors.black12,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<bool> _onBackPressed() {
    setState(() {
      isBookDialogVisible = false;
      isScrapDialogVisible = false;
    });
  }

  Widget sortBookDialog() {
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          GestureDetector(
            onTap: (){
              setState(() {
                isBookDialogVisible = false;
              });
            },
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              color: Colors.black54,
            ),
          ),
          Container(
            width: 400.0,
            height: 300.0,
            margin: EdgeInsets.all(24),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(30)
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(8),
                  child: Text('도서 정렬 방식', style: TextStyle(fontWeight: FontWeight.bold)),
                ),
                Padding(
                    padding: EdgeInsets.symmetric(horizontal: 24, vertical: 6),
                    child: GestureDetector(
                      onTap: (){
                        sortBookType(0);
                        saveConfig();
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Text('제목'),
                          CheckCircle(
                            width: 20,
                            height: 20,
                            isChecked: selectedSortBook[0],
                          ),
                        ],
                      ),
                    )
                ),
                Padding(
                    padding: EdgeInsets.symmetric(horizontal: 24, vertical: 6),
                    child: GestureDetector(
                      onTap: (){
                        sortBookType(1);
                        saveConfig();
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Text('지은이/역자'),
                          CheckCircle(
                            width: 20,
                            height: 20,
                            isChecked: selectedSortBook[1],
                          ),
                        ],
                      ),
                    )
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 24, vertical: 6),
                  child: GestureDetector(
                    onTap: (){
                      sortBookType(2);
                      saveConfig();
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text('출판사'),
                        CheckCircle(
                          width: 20,
                          height: 20,
                          isChecked: selectedSortBook[2],
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 24, vertical: 6),
                  child: GestureDetector(
                    onTap: (){
                      sortBookType(3);
                      saveConfig();
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text('등록일'),
                        CheckCircle(
                          width: 20,
                          height: 20,
                          isChecked: selectedSortBook[3],
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  height: 1,
                  margin: EdgeInsets.symmetric(horizontal: 12, vertical: 6),
                  color: Colors.black12,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 24, vertical: 6),
                  child: GestureDetector(
                    onTap: (){
                      orderBook(0);
                      saveConfig();
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text('오름차순'),
                        CheckCircle(
                          width: 20,
                          height: 20,
                          isChecked: selectedOrderBook[0],
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 24, vertical: 6),
                  child: GestureDetector(
                    onTap: (){
                      orderBook(1);
                      saveConfig();
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text('내림차순'),
                        CheckCircle(
                          width: 20,
                          height: 20,
                          isChecked: selectedOrderBook[1],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget sortScrapDialog() {
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          GestureDetector(
            onTap: (){
              setState(() {
                isScrapDialogVisible = false;
              });
            },
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              color: Colors.black54,
            ),
          ),
          Container(
            width: 400.0,
            height: 300.0,
            margin: EdgeInsets.all(24),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(30)
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(8),
                  child: Text('스크랩 정렬 방식', style: TextStyle(fontWeight: FontWeight.bold)),
                ),
                Padding(
                    padding: EdgeInsets.symmetric(horizontal: 24, vertical: 6),
                    child: GestureDetector(
                      onTap: (){
                        sortScrapType(0);
                        saveConfig();
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Text('폴더 이름'),
                          CheckCircle(
                            width: 20,
                            height: 20,
                            isChecked: selectedSortScrap[0],
                          ),
                        ],
                      ),
                    )
                ),
                Padding(
                    padding: EdgeInsets.symmetric(horizontal: 24, vertical: 6),
                    child: GestureDetector(
                      onTap: (){
                        sortScrapType(1);
                        saveConfig();
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Text('생성일'),
                          CheckCircle(
                            width: 20,
                            height: 20,
                            isChecked: selectedSortScrap[1],
                          ),
                        ],
                      ),
                    )
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 24, vertical: 6),
                  child: GestureDetector(
                    onTap: (){
                      sortScrapType(2);
                      saveConfig();
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text('수정일'),
                        CheckCircle(
                          width: 20,
                          height: 20,
                          isChecked: selectedSortScrap[2],
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  height: 1,
                  margin: EdgeInsets.symmetric(horizontal: 12, vertical: 6),
                  color: Colors.black12,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 24, vertical: 6),
                  child: GestureDetector(
                    onTap: (){
                      orderScrap(0);
                      saveConfig();
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text('오름차순'),
                        CheckCircle(
                          width: 20,
                          height: 20,
                          isChecked: selectedOrderScrap[0],
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 24, vertical: 6),
                  child: GestureDetector(
                    onTap: (){
                      orderScrap(1);
                      saveConfig();
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text('내림차순'),
                        CheckCircle(
                          width: 20,
                          height: 20,
                          isChecked: selectedOrderScrap[1],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
