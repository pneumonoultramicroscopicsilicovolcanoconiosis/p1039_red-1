import 'package:flutter/material.dart';
import 'package:redproject/Components/GradientButton.dart';
import 'package:redproject/Constant.dart';
import 'package:redproject/ResultManager.dart';
import 'package:redproject/MessageSnack.dart';
import 'package:http/http.dart' as http;
import 'package:crypto/crypto.dart';
import 'dart:convert';
import 'dart:developer' as developer;

class RegisterScreen extends StatefulWidget {
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  final emailController = TextEditingController();
  final nickController = TextEditingController();
  final pwController = TextEditingController();
  final pw2Controller = TextEditingController();

  String labelEmail = '이메일';
  String labelPass1 = '비밀번호';
  String labelPass2 = '비밀번호 확인';

  bool isEmail = false;
  bool isPassword1 = false;
  bool isPassword2 = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        resizeToAvoidBottomPadding: false,
        body: Column(children: <Widget>[
          titleBar(),
          Container(
            margin: EdgeInsets.all(24),
            child: Column(
              children: <Widget>[
                TextField(
                  controller: emailController,
                  onChanged: (text) => {
                    _checkingEmail(text)
                  },
                  decoration: InputDecoration(
                    labelText: labelEmail,
                    labelStyle: TextStyle(fontSize: 14),
                    contentPadding: EdgeInsets.fromLTRB(8, 4, 8, 4),
                    border: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey)
                    )
                  ),
                ),
                Container(
                  height: 8,
                ),
                TextFormField(
                  controller: nickController,
                  decoration: InputDecoration(
                    labelText: '별명',
                      labelStyle: TextStyle(fontSize: 14),
                    contentPadding: EdgeInsets.fromLTRB(8, 4, 8, 4),
                    border: UnderlineInputBorder(
                    )
                  ),
                ),
                Container(
                  height: 8,
                ),
                TextField(
                  controller: pwController,
                  onChanged: (text) => {
                    _checkingPassword1(text)
                  },
                  obscureText: true,
                  decoration: InputDecoration(
                    labelText: labelPass1,
                    labelStyle: TextStyle(fontSize: 14),
                    contentPadding: EdgeInsets.fromLTRB(8, 4, 8, 4),
                    border: UnderlineInputBorder(

                    )
                  ),
                ),
                Container(
                  height: 8,
                ),
                TextField(
                  controller: pw2Controller,
                  onChanged: (text) => {
                    _checkingPassword2(text)
                  },
                  obscureText: true,
                  decoration: InputDecoration(
                    labelText: labelPass2,
                      labelStyle: TextStyle(fontSize: 14),
                    contentPadding: EdgeInsets.fromLTRB(8, 4, 8, 4),
                    border: UnderlineInputBorder(
                    )
                  ),
                ),
                Container(
                  height: 8,
                ),
                GradientButton(
                  child: Text(
                    '완료',
                    style: TextStyle(color: Colors.white),
                  ),
                  gradient: LinearGradient(
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                    colors: [Color(COLOR_YELLOW), Color(COLOR_PINK)]),
                  onPressed: () => {upload()},
                ),
              ],
            ),
          )
        ],)
        );
  }

  Widget titleBar() {
    return Container(
      margin: EdgeInsets.fromLTRB(0, MediaQuery.of(context).padding.top, 0, 0),
      height: 50,
      child: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment.centerLeft,
            child: GestureDetector(
              onTap: (){
                Navigator.of(context).pop();
              },
              child: Container(
                width: 18,
                height: 18,
                margin: EdgeInsets.only(left: 12),
                child: Image.asset('assets/icon_back.png'),
              ),
            ),
          ),
          Align(
              alignment: Alignment.center,
              child:
              Text('회원 가입')
          ),
          Align(
            alignment: Alignment.centerRight,
            child: Padding(
              padding: EdgeInsets.fromLTRB(0, 8, 12, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[

                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  _checkingEmail(String text) {
    Pattern pattern = r'^[0-9a-zA-Z][0-9a-zA-Z\_\-\.\+]+[0-9a-zA-Z]@[0-9a-zA-Z][0-9a-zA-Z\_\-]*[0-9a-zA-Z](\.[a-zA-Z]{2,6}){1,2}$';
    RegExp regExp = RegExp(pattern, caseSensitive: false, multiLine: false);

    setState(() {
      if(regExp.hasMatch(text.trim())) {
        labelEmail = '이메일: 정상';
        isEmail = true;
      } else {
        labelEmail = '이메일 형식에 맞춰주세요.';
        isEmail = false;
      }
    });
  }

  _checkingPassword1(String text) {
    Pattern pattern = r'^(?=.*?[a-z][A-Z])(?=.*?[0-9]).{8,}$';
    RegExp regExp = RegExp(pattern, caseSensitive: false, multiLine: false);

    setState(() {
      if(regExp.hasMatch(text)) {
        labelPass1 = '패스워드: 정상';
        isPassword1 = true;
      } else {
        labelPass1 = '패스워드: 영문, 숫자 포함 8글자 이상';
        isPassword1 = false;
      }
    });

  }

  _checkingPassword2(String text) {

    setState(() {
      if(pwController.text == text) {
        labelPass2 = '패스워드 일치';
        isPassword2 = true;
      } else {
        labelPass2 = '비밀번호 불일치';
        isPassword2 = false;
      }
    });

  }

  upload() async {
    var url = 'https://api.takebook.org/Account/CreateUsers';

    String id = emailController.text.trim();
    String nick = nickController.text;
    String pw1 = pwController.text;
    String pw2 = pw2Controller.text;

    if (id == null || nick == null || pw1 == null || pw2 == null ||
    id == "" || nick == "" || pw1 == "" || pw2 == "") {
      final snackBar = SnackBar(content: Text('누락된 값이 있습니다.'));
      _scaffoldKey.currentState.showSnackBar(snackBar);
      return;
    }

    developer.log('$isEmail, $isPassword1, $isPassword2', name: 'Regular Exp');

    if(!isEmail || !isPassword1 || !isPassword2) {
      MessageSnack.show(_scaffoldKey, '형식에 맞지 않는 값이 있습니다.');
      return;
    }

    var bytes = utf8.encode(pw1);
    var digest = sha256.convert(bytes);

    var response = await http.post(url, body: {
      'user_id': '$id',
      "user_password": '$digest',
      "user_name": '$nick'
    });

    Map<String, dynamic> map = json.decode(response.body);
    String resultCode = map['Result_Code'];

    if(resultCode == 'RS000') {
      Navigator.pop(context);
    } else if(resultCode == 'RS001') {
      final snackBar = SnackBar(content: Text('이미 사용 중인 아이디입니다.'));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }

    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');
  }
}
