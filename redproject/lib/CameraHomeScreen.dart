import 'dart:async';
import 'dart:io';

import 'package:camera/camera.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flutter/material.dart';
import 'package:redproject/PreviewScreen.dart';
import 'package:redproject/ScrapPreviewScreen.dart';
import 'package:image_cropper/image_cropper.dart';

class CameraHomeScreen extends StatefulWidget {

  int mode = 0;

  CameraHomeScreen(this.mode);

  @override
  State<StatefulWidget> createState() {
    return _CameraHomeScreenState();
  }
}

class _CameraHomeScreenState extends State<CameraHomeScreen> {
  String imagePath;
  bool _toggleCamera = false;
  CameraController controller;
  List<CameraDescription> cameras;
  List<String> labels = ['책 촬영', '스크랩 촬영'];

  @override
  void initState() {
    try {
      getCameras();
    } catch (e) {
      print(e.toString());
    }
    super.initState();
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }

  Future getCameras() async {
    cameras = await availableCameras();

    setState(() {
      controller = CameraController(cameras[0], ResolutionPreset.medium);
      onCameraSelected(cameras[0]);
    });

  }

  @override
  Widget build(BuildContext context) {

    Widget initWidget = Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      color: Colors.black,
      child: Text('카메라 로딩 중...'),
    );

    if(cameras.isEmpty || cameras == null) {
      return Container(
        alignment: Alignment.center,
        padding: EdgeInsets.all(16.0),
        child: Text(
          'No Camera Found',
          style: TextStyle(
            fontSize: 16.0,
            color: Colors.white,
          ),
        ),
      );
    }

    if (!controller.value.isInitialized) {

      return Container();
    }

    return Scaffold(
      body: Stack(
        children: <Widget>[
          AspectRatio(
            aspectRatio: controller.value.aspectRatio,
            child: Container(
              child: Stack(
                children: <Widget>[
                  Container(
                    color: Colors.white12,
                    child: CameraPreview(controller),
                    padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                  ),
                ],
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: cameraControllerView(),
          ),
        ],
      ),
    );
  }

  Widget cameraButtonView() {
    return Container(
      height: 150,
      child: Column(
        children: <Widget>[
          Text(labels[widget.mode], style: TextStyle(color: Colors.white))
        ],
      ),
    );
  }

  Widget cameraControllerView() {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Stack(
        children: <Widget>[
          Container(
            width: double.infinity,
            height: 150,
            padding: EdgeInsets.all(20.0),
            child: Stack(
              children: <Widget>[
                Align(
                  alignment: Alignment.center,
                  child: Material(
                    color: Colors.transparent,
                    child: InkWell(
                      borderRadius: BorderRadius.all(Radius.circular(50.0)),
                      onTap: () {
                        _captureImage();
                      },
                      child: Column(
                        children: <Widget>[
                          Align(
                            alignment: Alignment.topCenter,
                            child: Container(
                              padding: EdgeInsets.only(bottom: 12),
                              child: Text(labels[widget.mode], style: TextStyle(color: Colors.white)),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.all(4.0),
                            child: Image.asset(
                              'assets/ic_shutter_1.png',
                              width: 72.0,
                              height: 72.0,
                            ),
                          ),
                        ],
                      )
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Material(
                    color: Colors.transparent,
                    child: InkWell(
                      borderRadius: BorderRadius.all(Radius.circular(50.0)),
                      onTap: getImage,
                      child: Container(
                        color: Colors.transparent,
                        padding: EdgeInsets.all(4.0),
                        child: Image.asset(
                          'assets/icon_gallery_white.png',
                          width: 24,
                          height: 24,
                        ),
                      ),
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.centerRight,
                  child: Material(
                    color: Colors.transparent,
                    child: InkWell(
                      borderRadius: BorderRadius.all(Radius.circular(50.0)),
                      onTap: getImage,
                      child: GestureDetector(
                        onTap: (){
                          setState(() {
                            if(widget.mode == 0) {
                              widget.mode = 1;
                            } else {
                              widget.mode = 0;
                            }
                          });
                        },
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(12),
                            color: Colors.black54,
                          ),
                          padding: EdgeInsets.all(12),
                          child: Text('모드 변경', style: TextStyle(color: Colors.white))
                        ),
                      )
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void onCameraSelected(CameraDescription cameraDescription) async {
    if (controller != null) await controller.dispose();
    controller = CameraController(cameraDescription, ResolutionPreset.high);

    controller.addListener(() {
      if (mounted) setState(() {});
      if (controller.value.hasError) {
        showMessage('Camera Error: ${controller.value.errorDescription}');
      }
    });

    try {
      await controller.initialize();
    } on CameraException catch (e) {
      print('CameraException');
      showException(e);
    }

    if (mounted) setState(() {});
  }

  String timestamp() => new DateTime.now().millisecondsSinceEpoch.toString();

  void _captureImage() {
    takePicture().then((String filePath) {
      if (mounted) {
        setState(() {
          imagePath = filePath;
        });
        if (filePath != null) {
          showMessage('Picture saved to $filePath');
          setCameraResult();
        }
      }
    });
  }

  void setCameraResult() async {
    //Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => PreviewScreen(imagePath)));

    if(widget.mode == 0) {
      File croppedFile = await ImageCropper.cropImage(
          sourcePath: imagePath,
          aspectRatioPresets: [
            CropAspectRatioPreset.square,
            CropAspectRatioPreset.ratio3x2,
            CropAspectRatioPreset.original,
            CropAspectRatioPreset.ratio4x3,
            CropAspectRatioPreset.ratio16x9
          ],
          androidUiSettings: AndroidUiSettings(
              toolbarTitle: 'Cropper',
              toolbarColor: Colors.deepOrange,
              toolbarWidgetColor: Colors.white,
              initAspectRatio: CropAspectRatioPreset.original,
              lockAspectRatio: false),
          iosUiSettings: IOSUiSettings(
              aspectRatioLockEnabled: false,
              aspectRatioLockDimensionSwapEnabled: true,
              doneButtonTitle: '완료',
              cancelButtonTitle: '취소'
          )
      );

      if (croppedFile != null) {
        Navigator.pushReplacement(context, MaterialPageRoute(
            builder: (context) => PreviewScreen(croppedFile)));
      }
    } else {
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => ScrapPreviewScreen(imagePath: imagePath,)));
    }

  }

  Future<String> takePicture() async {
    if (!controller.value.isInitialized) {
      showMessage('Error: select a camera first.');
      return null;
    }
    final Directory extDir = await getApplicationDocumentsDirectory();
    final String dirPath = '${extDir.path}/FlutterDevs/Camera/Images';
    await new Directory(dirPath).create(recursive: true);
    final String filePath = '$dirPath/${timestamp()}.jpg';

    if (controller.value.isTakingPicture) {
      // A capture is already pending, do nothing.
      return null;
    }

    try {
      await controller.takePicture(filePath);
    } on CameraException catch (e) {
      showException(e);
      return null;
    }
    return filePath;
  }

  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);

    imagePath = image.path;
    setCameraResult();
  }

  void showException(CameraException e) {
    logError(e.code, e.description);
    showMessage('Error: ${e.code} ${e.description}');
  }

  void showMessage(String message) {
    print(message);
  }

  void logError(String code, String message) =>
      print('Error: $code Message: $message');
}
