import 'package:flutter/material.dart';

class CenterSliceLab extends StatefulWidget {
  @override
  _CenterSliceLabState createState() => _CenterSliceLabState();
}

class _CenterSliceLabState extends State<CenterSliceLab> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('CenterSlice'),
      ),
      body: Center(
        child: Image.asset(
          'assets/img_slicetest.png',
          width: 100,
          height: 10,
          fit: BoxFit.fill,
          centerSlice: Rect.fromLTRB(5, 5, 5, 5),
        ),
      ),
    );
  }
}
