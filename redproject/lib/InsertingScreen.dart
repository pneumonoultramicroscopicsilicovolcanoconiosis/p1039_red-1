import 'package:flutter/material.dart';
import 'package:redproject/DTO/BookCover.dart';
import 'package:redproject/TokenManager.dart';
import 'package:redproject/ResultManager.dart';
import 'package:redproject/MessageSnack.dart';
import 'package:http/http.dart' as http;
import 'dart:developer' as developer;
import 'dart:convert';
import 'dart:io';

class InsertingScreen extends StatefulWidget {
  @override
  _InsertingScreenState createState() => _InsertingScreenState();
}

class _InsertingScreenState extends State<InsertingScreen> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  // 0: 등록중
  // 1: 등록실패
  var bookState = ['등록중', '등록실패'];

  List<BookList> list = List();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: Column(
        children: <Widget>[
          titleBar(),
          Expanded(
            child: ListView.builder(
              scrollDirection: Axis.vertical,
              itemCount: list.length,
              itemBuilder: (BuildContext context, int index) => Container(
                child: GestureDetector(
                  onTap: () => {
                    removeBookList(list[index].image_id, index)
                  },
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        height: 150,
                        padding: EdgeInsets.fromLTRB(8, 8, 8, 8),
                        child: Row(
                          children: <Widget>[
                            Container(
                              width: 100,
                              child: Image.network(list[index].image_url, fit: BoxFit.fitWidth),
                            ),
                            Expanded(
                              child: Stack(
                                children: <Widget>[
                                  Align(
                                    alignment: Alignment.center,
                                    child: Text(bookState[list[index].state]),
                                  ),
                                  Align(
                                    alignment: Alignment.bottomRight,
                                    child: Text(list[index].registration_date),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                          height: 8,
                          color: Colors.black12
                      )
                    ],
                  ),
                )
              ),
            ),
          )
        ],
      ),
    );
  }

  @override
  void initState() {
    getBookList();
  }

  Future getBookList() async {
    String token = await TokenManager().getToken();

    var url = 'https://api.takebook.org/Account/ImageList';

    var response = await http.get(url, headers: {'Authorization': '$token'});

    print('Home Response status: ${response.statusCode}');
    print('Home Response body: ${response.body}');

    developer.log(response.body, name: 'apple');

    ResultManager resultManager = ResultManager(response);
    if(!resultManager.isSuccess()) {
      // 실패

    }

    Map<String, dynamic> map = resultManager.getResult();
    var item = map['item'];

    setState(() {
      list = item.map<BookList>((json) => BookList.fromJson(json)).toList();
    });

  }

  Future removeBookList(String image_id, int index) async {
    var url = 'https://api.takebook.org/Account/ImageList';

    TokenManager tokenManager = TokenManager();
    String token = await tokenManager.getToken();

    HttpClient httpClient = HttpClient();
    HttpClientRequest request = await httpClient.deleteUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    request.headers.add('Authorization', '$token');
    request.add(utf8.encode(json.encode({'image_id' : '$image_id'})));

    developer.log(image_id, name: 'HttpClient');
    developer.log(request.toString(), name: 'HttpClient');

    HttpClientResponse response = await request.close();
    String statusCode = response.statusCode.toString();
    String result = await response.transform(utf8.decoder).join();

    setState(() {
      list.removeAt(index);
    });

    httpClient.close();

    developer.log(result, name: 'HttpClient');
  }

  Widget titleBar() {
    return Container(
      color: Colors.white,
      margin: EdgeInsets.fromLTRB(0, MediaQuery
          .of(context)
          .padding
          .top, 0, 0),
      height: 50,
      child: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment.centerLeft,
            child: GestureDetector(
              onTap: () {
                Navigator.of(context).pop();
              },
              child: Container(
                width: 18,
                height: 18,
                color: Colors.white,
                margin: EdgeInsets.only(left: 12),
                child: Image.asset('assets/icon_back.png'),
              ),
            ),
          ),
          Align(
              alignment: Alignment.center,
              child:
              Text('등록 실패 리스트')
          ),
        ],
      ),
    );
  }
}

class BookList {
  String image_id;
  String registration_date;
  String image_url;
  int state;

  BookList({
    this.image_id,
    this.registration_date,
    this.image_url,
    this.state
  });

  BookList.fromJson(Map<String, dynamic> json) {
    this.image_id = json['image_id'];
    this.registration_date = json['registration_date'];
    this.image_url = json['image_url'];
    this.state = json['state'];
  }
}