# 책을 찍다

## 클라이언트

---

> 플랫폼 / 언어: Flutter / Dart

## Android와 iOS UI 스크린샷

| 구분 | Android | iOS |
| -------- | ------- | -------- |
| 스플래시 화면 | <img src="images/Android/splash.png" width="250px"></img> | <img src="images/iOS/splash.png" width="250px"></img> |
| 로그인 | <img src="images/Android/login.png" width="250px"></img> | <img src="images/iOS/login.png" width="250px"></img> |
| 메인 - 도서 | <img src="images/Android/home.png" width="250px"></img> | <img src="images/iOS/home.png" width="250px"></img> |
| 메인 - 스크랩 폴더 | <img src="images/Android/scrapfolder.png" width="250px"></img> | <img src="images/iOS/scrapfolder.png" width="250px"></img> |
| 스크랩 리스트 | <img src="images/Android/scraplist.png" width="250px"></img> | <img src="images/iOS/scraplist.png" width="250px"></img> |
| 스크랩 상세 | <img src="images/Android/realscrapedit.png" width="250px"></img> | <img src="images/iOS/realscrapedit.png" width="250px"></img> |
| 스크랩 편집 | <img src="images/Android/scrapedit.png" width="250px"></img> | <img src="images/iOS/scrapedit.png" width="250px"></img> |
| 책 상세 | <img src="images/Android/bookinfo.png" width="250px"></img> | <img src="images/iOS/bookinfo.png" width="250px"></img> |
| 댓글과 평점 | <img src="images/Android/comment.png" width="250px"></img> | <img src="images/iOS/comment.png" width="250px"></img> |
| 북친 | <img src="images/Android/friends.png" width="250px"></img> | <img src="images/iOS/friends.png" width="250px"></img> |
| 검색 | <img src="images/Android/search.png" width="250px"></img> | <img src="images/iOS/search.png" width="250px"></img> |



## 오픈소스 라이센스

> #### Flutter

> #### OpenCV
>
> https://opencv.org/
>
> License(BSD)
>
> ```
> License Agreement
> For Open Source Computer Vision Library
> (3-clause BSD License)
> 
> Copyright (C) 2000-2019, Intel Corporation, all rights reserved.
> Copyright (C) 2009-2011, Willow Garage Inc., all rights reserved.
> Copyright (C) 2009-2016, NVIDIA Corporation, all rights reserved.
> Copyright (C) 2010-2013, Advanced Micro Devices, Inc., all rights reserved.
> Copyright (C) 2015-2016, OpenCV Foundation, all rights reserved.
> Copyright (C) 2015-2016, Itseez Inc., all rights reserved.
> Third party copyrights are property of their respective owners.
> 
> Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
> 
> Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
> Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
> Neither the names of the copyright holders nor the names of the contributors may be used to endorse or promote products derived from this software without specific prior written permission.
> This software is provided by the copyright holders and contributors “as is” and any express or implied warranties, including, but not limited to, the implied warranties of merchantability and fitness for a particular purpose are disclaimed. In no event shall copyright holders or contributors be liable for any direct, indirect, incidental, special, exemplary, or consequential damages (including, but not limited to, procurement of substitute goods or services; loss of use, data, or profits; or business interruption) however caused and on any theory of liability, whether in contract, strict liability, or tort (including negligence or otherwise) arising in any way out of the use of this software, even if advised of the possibility of such damage.
> ```

> #### image_cropper(1.1.0)
> 
> https://pub.dev/packages/image_cropper
> 
> License(BSD)
>
> ```
> Copyright 2013, the Dart project authors. All rights reserved.
> Redistribution and use in source and binary forms, with or without
> modification, are permitted provided that the following conditions are
> met:
> 
>     * Redistributions of source code must retain the above copyright
>       notice, this list of conditions and the following disclaimer.
>     * Redistributions in binary form must reproduce the above
>       copyright notice, this list of conditions and the following
>       disclaimer in the documentation and/or other materials provided
>       with the distribution.
>     * Neither the name of Google Inc. nor the names of its
>       contributors may be used to endorse or promote products derived
>       from this software without specific prior written permission.
> 
> THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
> "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
> LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
> A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
> OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
> SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
> LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
> DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
> THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
> (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
> OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
> ```

> #### SimpleCropView
> https://github.com/igreenwood/SimpleCropView
> 
> License(MIT)
> ```
> The MIT License (MIT)
> 
> Copyright (c) 2015 Issei Aoki
> 
> Permission is hereby granted, free of charge, to any person obtaining a copy
> of this software and associated documentation files (the "Software"), to deal
> in the Software without restriction, including without limitation the rights
> to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
> copies of the Software, and to permit persons to whom the Software is
> furnished to do so, subject to the following conditions:
> 
> The above copyright notice and this permission notice shall be included in all
> copies or substantial portions of the Software.
> 
> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
> IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
> FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
> AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
> LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
> OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
> SOFTWARE.
> ```

